﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public Camera cameraObj;
	public float minHeight = 5f;
	public float maxHeight = 50f;
	public float zoomSpeed = 1f;
	GameObject player;

	// Use this for initialization
	void Start () {
		cameraObj = GetComponentInChildren<Camera> ();
		player = GameObject.FindWithTag ("Player");
	}

	Vector3 currentVelocity;
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.SmoothDamp (transform.position, player.transform.position, ref currentVelocity, 0.5f);

		float delta = -Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;
		if (delta != 0f) {
			float size = cameraObj.orthographicSize;
			size += delta;
			cameraObj.orthographicSize = size;
		}
	}
}
