﻿using System.Collections.Generic;
using UnityEngine;

public class PrefabBinder: MonoBehaviour
{
    static readonly Dictionary<string, PrefabBinder> Instances = new Dictionary<string, PrefabBinder>();
    PrefabBinder _instance;
    Transform[] _transforms;

    void Awake()
    {

        int childCount = transform.childCount;
        _transforms = new Transform[childCount];
        for (int i = 0; i < childCount; i++)
        {
            Transform tr = transform.GetChild(i);
            _transforms[tr.GetSiblingIndex()] = tr;
        }
        Instances.Add(name, this);
    }

    public Transform BindChild(Transform transform)
    {
        if (_instance == null)
        {
            Instances.TryGetValue(name, out _instance);
            if (_instance == null)
            {
                Debug.LogError("There is no instance of " + name + " in scene");
                return null;
            }
        }
        return _instance._transforms[transform.GetSiblingIndex()];
    }

    public static Transform Bind(Transform transform)
    {
        Transform parent = transform.parent;
        if (parent == null)
        {
            Debug.LogError("Transform has no parent");
            return null;
        }
        PrefabBinder prefabBinder = parent.GetComponent<PrefabBinder>();
        if (prefabBinder == null)
        {
            Debug.LogError("Parent has no PrefabBinder script");
            return null;
        }
        return prefabBinder.BindChild(transform);
    }
}