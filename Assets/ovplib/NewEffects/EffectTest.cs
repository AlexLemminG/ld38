﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectTest : MonoBehaviour
{
    public Effect Effect;

    public bool Check;
    public bool Once;

    void Update()
    {
        if (!Check || Effect == null) return;
        if (Once) Check = false;
        Effect.Emit(transform.position, transform.forward);
    }
}
