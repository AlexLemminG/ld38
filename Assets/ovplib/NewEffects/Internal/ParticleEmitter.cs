﻿using UnityEngine;

[System.Serializable]
public class ParticleEmitter
{
    public Emitter ParticleSystem;
    public ShapeType Shape = ShapeType.Cone;
    public int CountMin = 1;
    public int CountRange;
    public float Speed = 1f;
    public float ConeSize = 0.5f;

    Emitter _actualPs;
#if UNITY_EDITOR
    Emitter _lastEmitter;

#endif

    public void Emit(Vector3 position, Vector3 normal)
    {
#if UNITY_EDITOR
        if (_lastEmitter != ParticleSystem)
        {
            _lastEmitter = ParticleSystem;
            _actualPs = null;
        }
#endif
        if (_actualPs == null)
        {
            Transform particleSystemTransform = PrefabBinder.Bind(ParticleSystem.transform);
            if (particleSystemTransform == null) return;
            Component component = particleSystemTransform.GetComponent(ParticleSystem.GetType());
            _actualPs = (Emitter)component;
            if (_actualPs == null) return;
        }


        int count = CountRange < 1 ? CountMin : FastRndom.Int(CountMin, CountRange);

        switch (Shape)
        {
            case ShapeType.Cone:
            {
                normal *= Speed;
                for (int i = 0; i < count; i++)
                {
                    //speed, cone size
                    Vector3 rnd = normal + FastRndom.VectorNormalized() * ConeSize;
                    _actualPs.Emit(position, rnd);
                }
            }
                break;
            case ShapeType.Point:
            {
                //speed
                normal *= Speed;
                for (int i = 0; i < count; i++)
                    _actualPs.Emit(position, normal);
            }
                break;
            case ShapeType.Circle:
            {
                for (int i = 0; i < count; i++)
                {
                    //circle size
                    Vector3 rnd = FastRndom.VectorCircle();
                    _actualPs.Emit(position, rnd * Speed);
                }
            }
                break;
        }
    }


    public enum ShapeType
    {
        Cone,
        Point,
        Circle,
        //PerfectCone60
    }
}