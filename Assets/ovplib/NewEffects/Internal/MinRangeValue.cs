using System;

[Serializable]
public class MinRangeValue
{
    public float Min;
    public float Range;
    bool _random;

    public void Initialize()
    {
        _random = Range > 0.00001f;
    }
    public float GetValue()
    {
        //if (_random)
            return Min + FastRndom.Float() * Range;
        return Min;
    }
}