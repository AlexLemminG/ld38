using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof (MinRangeValue))]
public class MinRangeValueDrawer: PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        EditorGUIUtility.labelWidth = 46;

        position.width = position.width * 0.5f;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("Min"));
        position.x += position.width;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("Range"));

        EditorGUI.EndProperty();
    }
}