﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof (ParticleEmitter))]
public class ParticleEmitterDrawer: PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 3;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        EditorGUIUtility.labelWidth = 80;

        position.height *= 0.33333f;

        float width = position.width;
        float x = position.x;
        position.width = width * 0.666f;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("ParticleSystem"), GUIContent.none);
        position.x += position.width;
        position.width = width * 0.333f;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("Shape"), GUIContent.none);

        position.y += position.height;
        position.x = x;
        position.width = width;
        
        position.width = width * 0.5f;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("CountMin"));
        position.x += position.width;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("CountRange"));
        
        position.y += position.height;
        position.x = x;
        position.width = width;

        ParticleEmitter.ShapeType shape = (ParticleEmitter.ShapeType)property.FindPropertyRelative("Shape").intValue;
        switch (shape)
        {
            case ParticleEmitter.ShapeType.Cone:
            {
                position.width = width * 0.5f;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("Speed"), new GUIContent("Speed"));
                position.x += position.width;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("ConeSize"), new GUIContent("Cone Size"));

            }
                break;
            case ParticleEmitter.ShapeType.Point:
            {
                position.width = width;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("Speed"), new GUIContent("Speed"));
            }
                break;
            case ParticleEmitter.ShapeType.Circle:
            {
                position.width = width;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("Speed"), new GUIContent("Circle Size"));
            }
                break;
        }


        EditorGUI.EndProperty();
    }
}