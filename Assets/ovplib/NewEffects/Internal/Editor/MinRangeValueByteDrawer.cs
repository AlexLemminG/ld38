using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(MinRangeValueByte))]
public class MinRangeValueByteDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        EditorGUIUtility.labelWidth = 46;

        SerializedProperty minProperty = property.FindPropertyRelative("Min");
        SerializedProperty rangeProperty = property.FindPropertyRelative("Range");
        float min = minProperty.intValue;
        float max = min + rangeProperty.intValue;

        EditorGUI.MinMaxSlider(position, ref min, ref max, 0, 255);

        minProperty.intValue = (int)min;
        rangeProperty.intValue = (int)(max - min);

        EditorGUI.EndProperty();
    }
}