﻿using System;
using UnityEngine;

public static class FastRndom
{
    static int _rnd1 = 242313459;
    static uint _rnd2 = 242313459;
    static int _rndI;
    const int Count = 256;
    static float[] _floats;
    static Vector3[] _vectors;
    static Vector3[] _hemisphereVectors;
    static Vector3[] _circleVectors;
    static float[] _fRotations;

    static FastRndom()
    {
        _floats = RandomsF(Count);
        _fRotations = RandomsF(Count, 360f);
        _vectors = RandomsV(Count, () => UnityEngine.Random.onUnitSphere);
        _hemisphereVectors = RandomsV(Count, () =>
                                             {
                                                 Vector3 vec = UnityEngine.Random.insideUnitSphere;
                                                 if (vec.y < 0) vec.y = -vec.y;
                                                 vec.y += 0.3f;
                                                 return vec.normalized;
                                             });
        _circleVectors = RandomsV(Count, () =>
                                         {
                                             Vector2 vec = new Vector2(0.5f - UnityEngine.Random.value, 0.5f - UnityEngine.Random.value).normalized;
                                             return new Vector3(vec.x, 0f, vec.y);
                                         });

    }

    public static int Int()
    {
        _rnd1 ^= _rnd1 << 7;
        _rnd1 ^= _rnd1 >> 3;
        return _rnd1;
    }
    public static int Int(int max)
    {
        if (max == 0) return 0;
        _rnd1 ^= _rnd1 << 7;
        _rnd1 ^= _rnd1 >> 3;
        return (_rnd1 < 0 ? -_rnd1 : _rnd1) % max;
    }
    public static int Int(int start, int diapazone)
    {
        _rnd1 ^= _rnd1 << 7;
        _rnd1 ^= _rnd1 >> 3;
        return start + (_rnd1 < 0 ? -_rnd1 : _rnd1) % diapazone;
    }
    public static uint Uint()
    {
        _rnd2 ^= _rnd2 << 7;
        _rnd2 ^= _rnd2 >> 3;
        return _rnd2;
    }
    public static float Float()
    {
        if (--_rndI < 0) _rndI = Count - 1;
        return _floats[_rndI];
    }

    public static float FloatRotation()
    {
        if (--_rndI < 0) _rndI = Count - 1;
        return _fRotations[_rndI];
    }

    public static Vector3 VectorNormalized()
    {
        if (--_rndI < 0) _rndI = Count - 1;
        return _vectors[_rndI];
    }
    public static Vector3 VectorHemisphere()
    {
        if (--_rndI < 0) _rndI = Count - 1;
        return _hemisphereVectors[_rndI];
    }
    public static Vector3 VectorCircle()
    {
        if (--_rndI < 0) _rndI = Count - 1;
        return _circleVectors[_rndI];
    }

    static float[] RandomsF(int length, float mult = 1f)
    {
        float[] array = new float[length];
        if (mult == 1f)
        {
            for (int i = 0; i < length; i++)
                array[i] = UnityEngine.Random.value;
        }
        else
        {
            for (int i = 0; i < length; i++)
                array[i] = UnityEngine.Random.value * mult;
        }
        return array;
    }
    static Vector3[] RandomsV(int length, Func<Vector3> func )
    {
        Vector3[] array = new Vector3[length];
        for (int i = 0; i < length; i++)
            array[i] = func();
        return array;
    }



    internal class Unrepeated
    {
        int _lastRandom;
        int _maxRandom;
        public Unrepeated(int maxRandom)
        {
            _maxRandom = maxRandom;
        }
        public int Get()
        {
            _rnd1 ^= _rnd1 << 7;
            _rnd1 ^= _rnd1 >> 3;
            int rndVal = (_rnd1 < 0 ? -_rnd1 : _rnd1) % _maxRandom;
            if (rndVal == _lastRandom) rndVal += 1;
            _lastRandom = rndVal;
            return rndVal % _maxRandom;
        }
    }
}
