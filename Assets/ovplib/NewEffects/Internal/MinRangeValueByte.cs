using System;

[Serializable]
public class MinRangeValueByte
{
    public byte Min;
    public byte Range;
    bool _random;

    public void Initialize()
    {
        _random = Range > 0.00001f;
    }
    public byte GetValue()
    {
        if (_random)
        {
            int val = FastRndom.Int(Min, Range);
            if (val > 255) val = 255;
            return (byte)val;
        }
        return Min;
    }
}