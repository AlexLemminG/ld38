﻿using UnityEngine;

public class ParticleSystemAdapter: Emitter
{
    public ParticleSystem ParticleSystem;

    public MinRangeValue Lifetime;
    public MinRangeValue Speed;
    public MinRangeValue Size;

    [UberDraw("Random 20;Value 120")]
    public RotationRandom Rotation;
    public Color32 Color = new Color32(255, 255, 255, 255);

    ParticleSystem.EmitParams _emitParams;

    void Awake()
    {
        Initialize();
    }

    void OnValidate()
    {
        Initialize();
    }
    
    void Initialize()
    {
        _emitParams = new ParticleSystem.EmitParams
        {
            randomSeed = FastRndom.Uint(),
            startColor = Color,
            startLifetime = Lifetime.Min,
            startSize = Size.Min,
            rotation = Rotation.Value
        };

    }

    public override void Emit(Vector3 pos, Vector3 vel)
    {
        float speed = Speed.GetValue();
        _emitParams.startLifetime = Lifetime.GetValue();
        _emitParams.startSize = Size.GetValue();
        if (Rotation.Random) _emitParams.rotation = FastRndom.FloatRotation();

        _emitParams.velocity = vel * speed;
        _emitParams.randomSeed = FastRndom.Uint();
        _emitParams.position = pos;
        ParticleSystem.Emit(_emitParams, 1);
    }


    [System.Serializable]
    public class RotationRandom
    {
        public bool Random;
        public float Value;
    }
}