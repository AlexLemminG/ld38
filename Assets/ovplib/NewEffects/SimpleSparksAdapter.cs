using UnityEngine;

public class SimpleSparksAdapter : Emitter
{
    public SimpleSparksRenderer SimpleSparks;

    public MinRangeValue Lifetime;
    public MinRangeValue Speed;
    public MinRangeValue Gravity;
    public MinRangeValue Drag;

    public MinRangeValueByte Emission;
    public MinRangeValueByte Size;
    public MinRangeValueByte TurbulenceAmplitude;
    public MinRangeValueByte TurbulenceFrequency;

    public float TimeShift;


    void Awake()
    {
        if (SimpleSparks == null) SimpleSparks = GetComponent<SimpleSparksRenderer>();
    }

    void OnValidate()
    {
        Lifetime.Initialize();
        Speed.Initialize();
        Gravity.Initialize();
        Drag.Initialize();

        Emission.Initialize();
        Size.Initialize();
        TurbulenceAmplitude.Initialize();
        TurbulenceFrequency.Initialize();
    }


    public override void Emit(Vector3 pos, Vector3 vel)
    {

        float lifetime = Lifetime.GetValue();
        float speed = Speed.GetValue();
        float gravity = Gravity.GetValue();
        float drag = Drag.GetValue();

        byte emission = Emission.GetValue();
        byte size = Size.GetValue();
        byte turbulenceAmp = TurbulenceAmplitude.GetValue();
        byte turbulenceFreq = TurbulenceFrequency.GetValue();

//        Debug.Log(speed);
        SimpleSparks.EmitSeg(pos, vel * speed, Time.time + TimeShift, gravity, drag, lifetime, emission, size, turbulenceAmp, turbulenceFreq);
    }
}