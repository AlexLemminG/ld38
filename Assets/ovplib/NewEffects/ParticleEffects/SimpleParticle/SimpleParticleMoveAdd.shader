﻿Shader "Custom/SimpleParticleMoveAdd" 
{
	Properties 
	{		
		[Queue]
		[HDR]_TintColor ("Tint Color (HDR)", Color) = (0.5,0.5,0.5,0.5)
		_MainTex ("Texture", 2D) = "white" { }
		_Size ("Size", Float) = 1
		[Toggle(TURBULENCE)] _Turbulence("Turbulence", Int) = 0
		[Space(8)]
		_TurbulenceFrequency0("Turbulence Frequency 0", Float) = 0
		_TurbulenceAmplitude0("Turbulence Amplitude 0", Float) = 0
		[Space(8)]
		_TurbulenceFrequency1("Turbulence Frequency 1", Float) = 0
		_TurbulenceAmplitude1("Turbulence Amplitude 1", Float) = 0
		[Space(16)]
		_FadeOut ("Fade Out Turbulence (1/sec)", Float) = 16


	}
	SubShader 
	{
		Tags { "Queue" = "Transparent" } 
        Blend One One
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Mode Off }

		
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma shader_feature TURBULENCE
			
			sampler2D _MainTex;
			float _MainTime;
			float _LastMainTime;
			half4 _TintColor; 
			half _TurbulenceFrequency0;
			half _TurbulenceAmplitude0;
			half _TurbulenceFrequency1;
			half _TurbulenceAmplitude1;
			half _Size;
			half _FadeOut;

			
			struct vertexIn 
			{
				float4 pos : POSITION;//    	position
				fixed2 uv0 : TEXCOORD0;//      	uv
				fixed2 uv1 : TEXCOORD1;//      	screenShift
				fixed2 uv2 : TEXCOORD2;//      	x:frameTimePos y:rnd 
				half4 tangent : TANGENT;//    	x:time y:lifeTime z:gravity w:drug
				half3 normal : NORMAL;//      	speed
				fixed4 color : COLOR0;//		r:emission g:size b:turbulence a:frequency
			};
			
			struct v2f 
			{
				float4 pos : SV_POSITION;
				fixed2 uv : TEXCOORD0;
				fixed light : COLOR0;
			};



			inline float3 CalcPos(half4 vals, half3 dir, half Time)
			{
				//half3 pos = dir * (1.0 - pow(vals.w, Time)) / (1.0 - vals.w);
				half3 pos = dir * (pow(vals.w, Time) - 1.0) / log(vals.w);
				pos.y -= vals.z * Time * Time;
				return pos;
			}

	     	inline half3 TriangleWave(half3 x) 
	        {  
	  			return abs(frac( x + 0.5 ) * 2.0 - 1.0);  
			}  
	 		inline half3 SmoothTriangleWave(half3 x) 
			{  
				x = TriangleWave(x);
				return x * x *(3.0 - 2.0 * x) - 0.5;
			} 
			inline half3 Turbulence(half3 pos, half3 normal, float Time, fixed vertRnd, half Frec) 
			{  
				half3 rnd = 0.2 + frac((pos.yzx + normal.zxy) * half3(769, 773, 787));
				return SmoothTriangleWave(vertRnd + rnd * (17.0 + Time) * Frec);
			} 



			v2f vert (vertexIn v)
			{
				v2f o;			

				const half minPeriod = 1.0/30;
				half period = max(_MainTime - _LastMainTime, minPeriod);

				half Time = _MainTime - (period - period * v.uv2.x) - v.tangent.x;

				Time = max(Time, 0);

				half3 curPos = CalcPos(v.tangent, v.normal, Time);
				half3 dir = curPos - CalcPos(v.tangent, v.normal, Time - 0.04);

				float3 pos = v.pos.xyz + curPos;

				#if TURBULENCE
				half fade = min(Time * _FadeOut, 1) * v.color.b;
				pos += Turbulence(v.pos, v.normal, Time,     v.uv2.y, _TurbulenceFrequency0 * v.color.a) * _TurbulenceAmplitude0 * fade;
				pos += Turbulence(v.normal, v.pos, Time, 1 - v.uv2.y, _TurbulenceFrequency1 * v.color.a) * _TurbulenceAmplitude1 * fade;
				#endif

				o.pos = mul(UNITY_MATRIX_VP, float4(pos, 1));
				fixed2 dir2 = normalize(mul((fixed3x3)UNITY_MATRIX_VP, dir).xy);

				fixed4 shift = v.uv1.xxyy * fixed4(dir2.xy, -dir2.y, dir2.x) * _Size * v.color.g;
				o.pos.xy += shift.xy + shift.zw;

				fixed a = 1 - (Time / v.tangent.y);
				o.light = a * v.color.r;
				o.pos *= (a > 0); 

				o.uv = v.uv0;	
				return o;
			}

			fixed4 frag (v2f i) : COLOR
			{
				return tex2D(_MainTex, i.uv) * i.light * _TintColor;
			}
			ENDCG
		}
	}
} 