﻿Shader "Custom/SimpleParticleMoveAlphaLight" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" { }
	}
	SubShader 
	{
		Tags { "Queue" = "Transparent" "LightMode" = "Vertex"} 
        Blend SrcAlpha OneMinusSrcAlpha
        //Blend One One
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Mode Off }

		
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			sampler2D _MainTex;
			float _MainTime;

			
			struct vertexIn 
			{
				fixed4 pos : POSITION;//    position
				fixed2 uv0 : TEXCOORD0;//      uv
				fixed2 uv1 : TEXCOORD1;//      screenShift
				float4 tangent : TANGENT;//    x:time y:lifeTime z:weight w:drug
				float3 normal : NORMAL;//      speed
			};
			
			struct v2f 
			{
				float4 pos : SV_POSITION;
				fixed2 uv : TEXCOORD0;
				fixed4 color : COLOR0;
			};



			v2f vert (vertexIn v)
			{
				v2f o;
				
				float Time = _MainTime - v.tangent.x;
				float moveTime = min(0.5 / v.tangent.w, Time);

				float3 pos = v.pos.xyz + v.normal * (moveTime - v.tangent.w * moveTime * moveTime);
				pos.y -= v.tangent.z * Time * Time;

				o.pos = mul(UNITY_MATRIX_VP, float4(pos, 1));
				o.pos.xy += v.uv1;

				o.uv = v.uv0;

				float3 lightColor = UNITY_LIGHTMODEL_AMBIENT.xyz;
				for (int i = 0; i < 4; i++) 
				{
					float3 toLight = unity_LightPosition[i].xyz - mul(UNITY_MATRIX_V, fixed4(pos, 1)).xyz * unity_LightPosition[i].w;
					float lengthSq = dot(toLight, toLight);
					float atten = 1.0 / (1.0 + lengthSq * unity_LightAtten[i].z);
					lightColor += unity_LightColor[i].rgb * atten;				
				}

				fixed a = 1 - Time / v.tangent.y;
				o.color = fixed4(lightColor,a);
	
				return o;
			}

			fixed4 frag (v2f i) : COLOR
			{
				fixed4 tex = tex2D(_MainTex, i.uv);
				return tex * i.color;
			}
			ENDCG
		}
	}
} 