﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class SimpleSparksRenderer : MonoBehaviour
{
    public int Segments;
    public int Count = 250;
    Vector3[] _vertices;
    Vector3[] _normals;
    Vector4[] _tangents;
    Color32[] _colors;
    Mesh _mesh;
    Bounds _bounds;
    Material _material;
    bool _needToUpdate;
    int _lastPos = 0;
    float _lastFrameTime;



    void OnValidate()
    {
        Segments = Math.Max(Segments, 2);
        Count = Math.Max(Count, 1);
        const int limit = (1 << 16) - 1;
        int particleLength = (Segments + 1) << 1;

        int max = limit / particleLength;
        Count = Math.Min(Count, max);
    }

    public void Awake()
    {
        int particleLength = (Segments + 1) << 1;
        int v4Count = Count * particleLength;
        int v6Count = Count * Segments * 6;

        _vertices = new Vector3[v4Count];
        int[] triangles = new int[v6Count];
        Vector2[] uv0 = new Vector2[v4Count];
        Vector2[] uv1 = new Vector2[v4Count];
        Vector2[] uv2 = new Vector2[v4Count];
        _normals = new Vector3[v4Count];
        _tangents = new Vector4[v4Count];
        _colors = new Color32[v4Count];

        for (int i = 0; i < Count; i++)
            Fill(i, uv0, uv1, triangles);

        {
            float[] tmpTimePos = new float[particleLength];
            for (int i = 0; i < Segments + 1; i++)
            {
                float timePos = Mathf.Clamp(i - 1, 0, Segments - 2) / (float)(Segments - 2);
                tmpTimePos[i << 1] = tmpTimePos[(i << 1) + 1] = timePos;
            }
            for (int s = 0, i = 0; s < Count; s++)
            {
                float rnd = Random.value;
                for (int v = 0; v < particleLength; v++, i++)
                    uv2[i] = new Vector2(tmpTimePos[v], rnd);
            }
        }

        _bounds = new Bounds(Vector3.zero, Vector3.zero);
        _mesh = new Mesh {vertices = _vertices, triangles = triangles, uv = uv0, uv2 = uv1, uv3 = uv2, normals = _normals, tangents = _tangents, colors32 = _colors};


        GetComponent<MeshFilter>().mesh = _mesh;
        _material = GetComponent<Renderer>().sharedMaterial;
    }

    public void Fill(int pos, Vector2[] uv0, Vector2[] uv1, int[] triangles)
    {
        int vertPairs = Segments + 1;
        int vertCount = vertPairs << 1;
        int v = pos * vertCount;

        //central
        {
            int length = vertPairs - 1;
            for (int vp = 1; vp < length; vp++)
            {
                int v0 = v + (vp << 1);
                int v1 = v0 + 1;

                uv0[v0] = new Vector2(0.5f, 0);
                uv0[v1] = new Vector2(0.5f, 1);

                uv1[v0] = new Vector2(0, -1);
                uv1[v1] = new Vector2(0, 1);
            }
        }

        //first
        {
            int v0 = v;
            int v1 = v + 1;

            uv0[v0] = new Vector2(0, 0);
            uv0[v1] = new Vector2(0, 1);


            uv1[v0] = new Vector2(-1, -1);
            uv1[v1] = new Vector2(-1, 1);
        }

        //last
        {
            int v0 = v + vertCount - 2;
            int v1 = v0 + 1;

            uv0[v0] = new Vector2(1, 0);
            uv0[v1] = new Vector2(1, 1);


            uv1[v0] = new Vector2(1, -1);
            uv1[v1] = new Vector2(1, 1);
        }

        //triangles
        {
            int tr = pos * Segments * 6;
            for (int i = 0; i < Segments; i++)
            {
                int iv3 = v + (i << 1);
                int iv0 = iv3++;
                int iv1 = iv3++;
                int iv2 = iv3++;

                triangles[tr++] = iv0;
                triangles[tr++] = iv2;
                triangles[tr++] = iv3;
                triangles[tr++] = iv3;
                triangles[tr++] = iv1;
                triangles[tr++] = iv0;
            }
        }
    }


    //  0 - 2 -       - 2n
    //  | \ | \  ...  \ |
    //  1 - 3 -       - 2n+1
    //  vert     - position
    //  uv0      - uv
    //  uv1      - screenShift
    //  uv2      - x:frameTimePos y:rnd
    //  normal   - speed
    //  tangent  - x:time y:lifeTime z:gravity w:drug
    //  color    - r:emission g:size b:turbulence a:frequency
    public void EmitSeg(Vector3 position, Vector3 velocity, float time, float gravity, float drag, float lifeTime, byte emission = 255, byte size = 255, byte turbulence = 255, byte frequency = 255)
    {
#if UNITY_EDITOR
        if (_vertices == null) Awake();
#endif
        int vertPairs = Segments + 1;
        int vertCount = vertPairs << 1;

        int v = _lastPos * vertCount;


        int end = v + vertCount;
        for (int i = 0; v < end; i++, v++)
        {
            _vertices[v] = position;
            _normals[v] = velocity;
            _tangents[v] = new Vector4(time, lifeTime, gravity, drag);
            _colors[v] = new Color32(emission, size, turbulence, frequency);
        }

        _lastPos++;
        if (_lastPos >= Count) _lastPos = 0;

        float expandValue = (velocity.x + velocity.y + velocity.z) * lifeTime + gravity * lifeTime * lifeTime;
        ExpandBoundsFast(ref _bounds, position, expandValue);

        _needToUpdate = true;
    }


    public static void ExpandBoundsFast(ref Bounds bounds, Vector3 position, float size)
    {
        position.x = position.x > 0 ? position.x : -position.x;
        position.y = position.y > 0 ? position.y : -position.y;
        position.z = position.z > 0 ? position.z : -position.z;

        position.x += size;
        position.y += size;
        position.z += size;

        Vector3 extents = bounds.extents;
        extents.x = extents.x > position.x ? extents.x : position.x;
        extents.y = extents.y > position.y ? extents.y : position.y;
        extents.z = extents.z > position.z ? extents.z : position.z;
        bounds.extents = extents;
    }

    void LateUpdate()
    { 
        if (!_needToUpdate) return;
        _mesh.vertices = _vertices;
        _mesh.normals = _normals;
        _mesh.tangents = _tangents;
        _mesh.colors32 = _colors;
        _mesh.bounds = _bounds;
        _needToUpdate = false;
    }

    void Update()
    {
        _material.SetFloat("_MainTime", Time.time);
        _material.SetFloat("_LastMainTime", _lastFrameTime);
        _lastFrameTime = Time.time;
    }
}