﻿using UnityEngine;

public class SimpleSparksTest : MonoBehaviour
{
    public SimpleSparksRenderer Renderer;
    public float Speed;
    public float Gravity;
    [Range(0, 0.9999999f)]
    public float Drag;
    public float Radius;
    public float LifeTime;
    [Range(0, 255)]
    public byte Emission;

    [Range(0, 255)]
    public byte Size;

    [Range(0, 255)]
    public byte TurbulenceAmp;

    [Range(0, 255)]
    public byte TurbulenceFreq;

    public int Count;
    public int CountRnd;


    public bool Check;
    public bool Once;



    public void Awake()
    {
        //Application.targetFrameRate = 30;
        //QualitySettings.vSyncCount = 2;
        // QualitySettings.maxQueuedFrames = 0;
    }


    void Update()
    {
        if (!Check || Renderer == null) return;
        if (Once) Check = false;

        int count = Count + Random.Range(0, CountRnd);

        float drag = Drag * Drag;
        drag *= drag;
        drag *= drag;

        for (int i = 0; i < count; i++)
            Renderer.EmitSeg(transform.position, (transform.forward + Random.insideUnitSphere * Radius) * Speed, Time.time, Gravity, drag, LifeTime, Emission, Size, TurbulenceAmp, TurbulenceFreq);
    }
}