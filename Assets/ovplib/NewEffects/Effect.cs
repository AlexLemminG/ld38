﻿using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "ScriptableObjects/Effect")]
public class Effect : ScriptableObject
{
    public ParticleEmitter[] ParticleEmitters;
    [Space]
    public AudioClip AudioClip;
    public float AudioVolume = 1f;



    //Sound
    //Camera
    //Light

    //Common Hit Behaviour

    //Blast wave
    //LensFlares
    //Decals


    public void Emit(Vector3 position, Vector3 normal)
    {
        for (int i = 0; i < ParticleEmitters.Length; i++)
            ParticleEmitters[i].Emit(position, normal);

        if (AudioClip != null)
            AudioSource.PlayClipAtPoint(AudioClip, position, AudioVolume);
    }

}

