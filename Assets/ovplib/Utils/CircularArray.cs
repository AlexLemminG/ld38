﻿using System;
using System.Collections;
using System.Collections.Generic;

class CircularArray<T> : IEnumerable<T>
{
    T[] _data;
    int _zeroIndex;
    public int Length { get; private set; }

    public T this[int i]
    {
        get
        {
            if (i < 0 || i >= Length) throw new IndexOutOfRangeException();
            return _data[GetIndex(i)];
        }
        set
        {
            if (i < 0 || i >= Length) throw new IndexOutOfRangeException();
            _data[GetIndex(i)] = value;
        }
    }


    public CircularArray(int length)
    {
        Length = length;
        _data = new T[length];
    }

    public void Add(T value)
    {
        _zeroIndex++;
        _zeroIndex %= Length;
        _data[_zeroIndex] = value;
    }

    int GetIndex(int i)
    {
        i += _zeroIndex;
        return i % Length;
    }
    int GetReverseIndex(int i)
    {
        i = (_zeroIndex - i) % Length;
        if (i < 0) i += _data.Length;
        return i;
    }

    public void Clear()
    {
        Array.Clear(_data, 0, _data.Length);
        _zeroIndex = 0;
    }


    public T GetFromOldest(int i)
    {
        return _data[GetIndex(i)];
    }
    public void SetFromOldest(int i, T value)
    {
        _data[GetIndex(i)] = value;
    }
    public T GetFromNewest(int i)
    {
        return _data[GetReverseIndex(i)];
    }
    public void SetFromNewest(int i, T value)
    {
        _data[GetReverseIndex(i)] = value;
    }


    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < Length; i++)
            yield return this[i];
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}