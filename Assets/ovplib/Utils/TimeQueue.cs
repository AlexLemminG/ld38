using UnityEngine;
using System.Collections.Generic;
using System;

public class TimeQueue
{
    SortedDictionary<float, Action> _vals = new SortedDictionary<float, Action>();
    float _nextTime;
    Action _nextAction;
    bool _off = true;


    public void Update()
    {
        Update(Time.time);
    }
    public void Update(float time)
    {
        if (_off) return;
        if (time < _nextTime) return;
        _nextAction();
        Next(time);
    }

    public void Add(float keyTime, Action action)
    {
        Add(Time.time, keyTime, action);
    }
    public void Add(float time, float keyTime, Action action)
    {
        if (time >= keyTime)
        {
            action();
            return;
        }
        if (_off)
        {
            AddToQueue(keyTime, action);
            _off = false;
            Next(time);
        }
        else
        {
            if (_nextTime < keyTime) AddToQueue(keyTime, action);
            else
            {
                AddToQueue(_nextTime, _nextAction);
                _nextTime = keyTime;
                _nextAction = action;
            }
        }
    }
    void AddToQueue(float keyTime, Action action)
    {
        Action oldAction;
        if (_vals.TryGetValue(keyTime, out oldAction))
        {
            _vals[keyTime] = delegate
                {
                    oldAction();
                    action();
                };
        }
        else
            _vals.Add(keyTime, action);
    }
    void Next(float time)
    {
        start:
        if (_vals.Count == 0)
        {
            _off = true;
            return;
        }

        foreach (KeyValuePair<float, Action> item in _vals)
        {
            _nextTime = item.Key;
            _nextAction = item.Value;
            _vals.Remove(_nextTime);
            break;
        }
        if (time > _nextTime)
        {
            _nextAction();
            goto start;
        }
    }
}
