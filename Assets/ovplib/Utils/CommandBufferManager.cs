using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CommandBufferManager
{
    public readonly Dictionary<Camera, CommandBuffer> Cameras = new Dictionary<Camera, CommandBuffer>();
    public readonly string BufferName;
    public readonly CameraEvent CameraEvent;

    public CommandBufferManager(string bufferName, CameraEvent cameraEvent)
    {
        BufferName = bufferName;
        CameraEvent = cameraEvent;
    }

    /// <summary> Put it Into OnRenderObject Event </summary>
    public CommandBuffer GetBuffer()
    {
        Camera currentCamera = Camera.current;
        if (!currentCamera) return null;

        CommandBuffer buffer;
        if (!Cameras.TryGetValue(currentCamera, out buffer))
        {
            buffer = FindOrCreate(currentCamera, CameraEvent, BufferName);
            Cameras.Add(currentCamera, buffer);
        }
        return buffer;
    }

    /// <summary> Put it Into OnRenderObject Event </summary>
    /// <returns> new buffer created </returns>
    public bool IsNewBufferCreated(out CommandBuffer buffer)
    {
        buffer = null;
        Camera currentCamera = Camera.current;
        if (!currentCamera) return false;

        if (Cameras.TryGetValue(currentCamera, out buffer)) return false;

        buffer = FindOrCreate(currentCamera, CameraEvent, BufferName);

        Cameras.Add(currentCamera, buffer);
        return true;
    }

    static CommandBuffer FindCommandBuffer(Camera camera, CameraEvent cameraEvent, string name)
    {
        CommandBuffer[] buffers = camera.GetCommandBuffers(cameraEvent);
        foreach (CommandBuffer buffer in buffers)
            if (buffer.name == name) return buffer;
        return null;
    }

    public static CommandBuffer FindOrCreate(Camera camera, CameraEvent cameraEvent, string name)
    {
        CommandBuffer buffer = FindCommandBuffer(camera, cameraEvent, name);
        if (buffer == null)
        {
            buffer = new CommandBuffer {name = name};
            camera.AddCommandBuffer(cameraEvent, buffer);
        }
        return buffer;
    }

    public void DestroyBuffers()
    {
        foreach (KeyValuePair<Camera, CommandBuffer> pair in Cameras)
            if (pair.Key) pair.Key.RemoveCommandBuffer(CameraEvent, pair.Value);

        Cameras.Clear();
    }
}

public class CommandBufferManager<T>
{
    public readonly Dictionary<Camera, Item> Cameras = new Dictionary<Camera, Item>();
    public readonly string BufferName;
    public readonly CameraEvent CameraEvent;


    public CommandBufferManager(string bufferName, CameraEvent cameraEvent)
    {
        BufferName = bufferName;
        CameraEvent = cameraEvent;
    }

    /// <summary> Put it Into OnRenderObject Event </summary>
    public Item GetData()
    {
        Camera currentCamera = Camera.current;
        if (!currentCamera) return null;

        Item data;
        if (!Cameras.TryGetValue(currentCamera, out data))
        {
            data = new Item();
            data.Buffer = FindOrCreate(currentCamera, CameraEvent, BufferName);
            Cameras.Add(currentCamera, data);
        }
        return data;
    }

    /// <summary> Put it Into OnRenderObject Event </summary>
    /// <returns> new buffer created </returns>
    public bool IsNewBufferCreated(out Item data)
    {
        data = null;
        Camera currentCamera = Camera.current;
        if (!currentCamera) return false;

        if (Cameras.TryGetValue(currentCamera, out data)) return false;

        data = new Item();
        data.Buffer = FindOrCreate(currentCamera, CameraEvent, BufferName);

        Cameras.Add(currentCamera, data);
        return true;
    }

    static CommandBuffer FindCommandBuffer(Camera camera, CameraEvent cameraEvent, string name)
    {
        CommandBuffer[] buffers = camera.GetCommandBuffers(cameraEvent);
        foreach (CommandBuffer buffer in buffers)
            if (buffer.name == name) return buffer;
        return null;
    }

    public static CommandBuffer FindOrCreate(Camera camera, CameraEvent cameraEvent, string name)
    {
        CommandBuffer buffer = FindCommandBuffer(camera, cameraEvent, name);
        if (buffer == null)
        {
            buffer = new CommandBuffer {name = name};
            camera.AddCommandBuffer(cameraEvent, buffer);
        }
        return buffer;
    }

    public void DestroyBuffers()
    {
        foreach (KeyValuePair<Camera, Item> pair in Cameras)
            if (pair.Key) pair.Key.RemoveCommandBuffer(CameraEvent, pair.Value.Buffer);

        Cameras.Clear();
    }

    public class Item
    {
        public CommandBuffer Buffer;
        public T Data;
    }
}