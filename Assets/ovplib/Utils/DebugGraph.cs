using UnityEngine;

public class DebugGraph
{
    public float this[int index]
    {
        get { return _vals[index].val; }
        set
        {
            _needToUpdate = true;
            _vals[index].val = value;
        }
    }

    public Color BackGround = Color.white;
    public Color CenterLine = Color.black;
    public float Scale;
    public Rect Rect;
    public bool DrawCenterLine = true;

    Texture2D _tex;
    int _t;
    int _h;
    int _ch;
    int _w;
    Value[] _vals;

    bool _needToUpdate;


    public DebugGraph(Rect rect, float scale, params Color[] colors)
    {
        Rect = rect;
        _h = (int)rect.height;
        _w = (int)rect.width;
        _ch = (int)(rect.height / 2f);
        Scale = scale;
        //_tex = new Texture2D((int)rect.width, (int)rect.height);

        if (colors.Length == 0) return;
        _vals = new Value[colors.Length];
        for (int i = 0; i < _vals.Length; i++)
        {
            _vals[i] = new Value(colors[i]);
        }
    }

    public void OnGUI()
    {
        if (_needToUpdate)
        {
            Update();
            _needToUpdate = false;
        }
        GUI.DrawTexture(Rect, _tex);
    }

    int Val(float value)
    {
        int v = (int)(value * Scale) + _ch;
        if (v >= _h) v = _h - 1;
        if (v < 0) v = 0;
        return v;
    }

    void Update()
    {
        Color[] cols = new Color[_h];

        for (int i = 0; i < _h; i++) cols[i] = BackGround;
        if (DrawCenterLine) cols[_ch] = CenterLine;

        for (int i = 0; i < _vals.Length; i++) _vals[i].Drawe(cols, this);

        if(_tex == null) _tex = new Texture2D(_w, _h);

        _tex.SetPixels(_t, 0, 1, _h, cols);
        _tex.Apply();

        _t++;
        if (_t >= _w) _t = 0;
    }
            
    class Value
    {
        public Color color;
        public float val;

        int lastVal;

        public Value(Color color)
        {
            this.color = color;
        }

        public void Drawe(Color[] colors, DebugGraph G)
        {
            int ival = G.Val(val);
            int start;
            int end;
            if (ival > lastVal)
            {
                start = lastVal;
                end = ival;
            }
            else
            {
                start = ival;
                end = lastVal;
            }

            for (int i = start; i <= end; i++)
            {
                colors[i] = color;
            }
            lastVal = ival;
        }
    }
}