using UnityEngine;

public class KineticValue3
{
    public Vector3 Value { get; private set; }

    public Vector3 Velocity
    {
        get
        {
            if (_velocityCalculated) return _velocity;
            _velocity = (Value - _lastValue) / _dt;
            _lastValue = Value;
            _velocityCalculated = true;
            return _velocity;
        }
    }

    public Vector3 Acceleration
    {
        get
        {
            if (_accelerationCalculated) return _acceleration;
            _acceleration = (Velocity - _lastVelocity) / _dt;
            _lastVelocity = Velocity;
            _accelerationCalculated = true;
            return _acceleration;
        }
    }

    Vector3 _lastValue;
    Vector3 _lastVelocity;

    Vector3 _velocity;
    Vector3 _acceleration;

    bool _velocityCalculated;
    bool _accelerationCalculated;
    bool _initialized;

    float _dt;

    public void Update(Vector3 value, float dt)
    {
        if (!_initialized)
        {
            _initialized = true;
            _lastValue = value;
        }

        Value = value;
        _dt = dt;
        _velocityCalculated = false;
        _accelerationCalculated = false;
    }
}