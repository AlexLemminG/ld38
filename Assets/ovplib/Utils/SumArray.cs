﻿using System;

public class SumArray
{
    float[] _vals;
    public int Length { get; private set; }
    public int CurrentLength { get; private set; }
    int _lastIndex = -1;
    public float Sum { get; private set; }
    public float Average { get { return Sum * _invLength; } }
    float _invLength;
    bool _full = false;

    public SumArray(int length)
    {
        if (length < 1) throw new Exception("Length must be greater than zero. Length:" + length);
        _vals = new float[length];
        _invLength = 1f / length;
        Length = length;
    }

    public void Clear()
    {
        for (int i = 0; i < _vals.Length; i++) _vals[i] = 0f;
        _lastIndex = -1;
        CurrentLength = 0;
        Sum = 0f;
        _full = false;
    }

    public void AddValue(float val)
    {
        if (!_full)
        {
            _lastIndex++;
            CurrentLength = _lastIndex;
            if (_lastIndex == Length)
            {
                _lastIndex = 0;
                _full = true;
            }
        }
        else
        {
            _lastIndex++;
            if (_lastIndex == Length) _lastIndex = 0;
        }
        Sum -= _vals[_lastIndex];
        _vals[_lastIndex] = val;
        Sum += val;
    }
}