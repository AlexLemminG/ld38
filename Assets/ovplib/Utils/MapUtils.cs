﻿using System;
using UnityEngine;

public static class MapUtils
{
    public delegate void BlendFunc(ref double arr, double val);
    public delegate void ProcessFunc(ref double arr);
    static MapUtils()
    {
        InitGradients();
    }

    #region Processing
    public static void Copy(double[,] inArray, double[,] outArray, int side)
    {
        for (int y = 0; y < side; y++)
            for (int x = 0; x < side; x++)
                outArray[x, y] = inArray[x, y];
    }
    public static void Process(double[,] outArray, ProcessFunc processFunc, int size)
    {
        for (int y = 0; y < size; y++)
            for (int x = 0; x < size; x++)
                processFunc(ref outArray[x, y]);
    }

    public static void Process(double[,] outArray, Func<double, double, double> processFunc, int size, bool inPixels = false)
    {
        double step = 1d/size;
        double xd=0, yd=0;
        if (inPixels)
        {
            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                    outArray[x, y] = processFunc(x, y);
        }
        else
        {
            for (int y = 0; y < size; y++, yd += step, xd = 0)
                for (int x = 0; x < size; x++, xd += step)
                    outArray[x, y] = processFunc(xd, yd);
        }
    }

    public static void GetGraph(int width, int height, Color32[] colors, Color32 color, Color32 background, int lineWidth, Func<float, float> processFunc)
    {
        int lastValue = (int) (processFunc(0)*height);
        float xStep = 1f/width;
        float fx = 0;
        for (int x = 0; x < width; x++, fx += xStep)
        {
            int value = (int) (processFunc(fx)*height);

            int min = Math.Min(value, lastValue);
            int max = Math.Max(value, lastValue) + lineWidth;
            min = Mathf.Clamp(min, 0, height);
            max = Mathf.Clamp(max, 0, height);

            lastValue = value;

            for (int y = 0; y < min; y++)
                colors[y*width + x] = background;

            for (int y = min; y < max; y++)
                colors[y*width + x] = color;

            for (int y = max; y < height; y++)
                colors[y*width + x] = background;
        }
    }

    #endregion

    #region Noise
    public static void Perlin2DFill(double[,] outArray, int shiftX, int shiftY, int pow2Size, int pow2Scale)
    {
        int size = 1 << pow2Size;
        int baseSize = (size >> pow2Scale) + 1;
        int grI = pow2Size - pow2Scale;
        double[][] gradients = _cashed2DGr[grI];
        if (gradients == null)
            _cashed2DGr[grI] = gradients = new double[baseSize * baseSize][];


        const int mask = GTableSize - 1;
        for (int y = 0, yshift = 0; y < baseSize; y++, yshift += baseSize)
        {
            for (int x = 0; x < baseSize; x++)
            {
                int i = x + yshift;
                int r = _rnd[((x + shiftX) + _rnd[(y + shiftY) & mask]) & mask];
                int gi = r & mask;
                gradients[i] = _gradientTable2D[gi];
            }
        }

        double[,][] lerps = _cashed2DLerps[pow2Scale];
        if (lerps == null)
            _cashed2DLerps[pow2Scale] = lerps = GetLerps2D(pow2Scale);

        int baseSize2 = baseSize - 1;
        int cellSize = 1 << pow2Scale;
        for (int y = 0; y < baseSize2; y++)
            for (int x = 0; x < baseSize2; x++)
            {
                int i0 = x + y * baseSize;

                double[] i00 = gradients[i0];
                double[] i10 = gradients[i0 + 1];
                i0 += baseSize;
                double[] i01 = gradients[i0];
                double[] i11 = gradients[i0 + 1];


                int xShift = x << pow2Scale;
                int yShift = y << pow2Scale;
                for (int xi = 0; xi < cellSize; xi++)
                    for (int yi = 0; yi < cellSize; yi++)
                    {
                        double[] lt = lerps[xi, yi];
                        outArray[xi + xShift, yi + yShift] = i00[0] * lt[0] +
                                                             i00[1] * lt[1] +
                                                             i10[0] * lt[2] +
                                                             i10[1] * lt[3] +
                                                             i01[0] * lt[4] +
                                                             i01[1] * lt[5] +
                                                             i11[0] * lt[6] +
                                                             i11[1] * lt[7];
                    }
            }
    }
    public static double Perlin2D(double x, double y)
    {
        const int mask = GTableSize - 1;
        int ix = (int) x;
        int iy = (int) y;

        double fx = x - ix;
        double fy = y - iy;

        int ry = _rnd[iy & mask];
        double[] i00 = _gradientTable2D[_rnd[(ix + ry) & mask] & mask];
        double[] i10 = _gradientTable2D[_rnd[(ix + 1 + ry) & mask] & mask];
        ry = _rnd[(iy + 1) & mask];
        double[] i01 = _gradientTable2D[_rnd[(ix + ry) & mask] & mask];
        double[] i11 = _gradientTable2D[_rnd[(ix + 1 + ry) & mask] & mask];

        double fxn = fx - 1;
        double fyn = fy - 1;

        double f00 = i00[0] * fx + i00[1] * fy;
        double f10 = i10[0] * fxn + i10[1] * fy;
        double f01 = i01[0] * fx + i01[1] * fyn;
        double f11 = i11[0] * fxn + i11[1] * fyn;

        fx = fx * fx * (3 - 2 * fx);
        fy = fy * fy * (3 - 2 * fy);

        double l0 = f00 + fy * (f01 - f00);
        double l1 = f10 + fy * (f11 - f10);
        return l0 + fx * (l1 - l0);
    }
    public static void Perlin2DBlend(double[,] outArray, BlendFunc blendFunc, int shiftX, int shiftY, int pow2Size, int pow2Scale)
    {
        int size = 1 << pow2Size;
        int baseSize = (size >> pow2Scale) + 1;
        int grI = pow2Size - pow2Scale;
        double[][] gradients = _cashed2DGr[grI];
        if (gradients == null)
            _cashed2DGr[grI] = gradients = new double[baseSize * baseSize][];


        const int mask = GTableSize - 1;
        for (int y = 0, yshift = 0; y < baseSize; y++, yshift += baseSize)
        {
            for (int x = 0; x < baseSize; x++)
            {
                int i = x + yshift;
                int r = _rnd[((x + shiftX) + _rnd[(y + shiftY) & mask]) & mask];
                int gi = r & mask;
                gradients[i] = _gradientTable2D[gi];
            }
        }

        double[,][] lerps = _cashed2DLerps[pow2Scale];
        if (lerps == null)
            _cashed2DLerps[pow2Scale] = lerps = GetLerps2D(pow2Scale);

        int baseSize2 = baseSize - 1;
        int cellSize = 1 << pow2Scale;
        for (int y = 0; y < baseSize2; y++)
            for (int x = 0; x < baseSize2; x++)
            {
                int i0 = x + y * baseSize;

                double[] i00 = gradients[i0];
                double[] i10 = gradients[i0 + 1];
                i0 += baseSize;
                double[] i01 = gradients[i0];
                double[] i11 = gradients[i0 + 1];


                int xShift = x << pow2Scale;
                int yShift = y << pow2Scale;
                for (int xi = 0; xi < cellSize; xi++)
                    for (int yi = 0; yi < cellSize; yi++)
                    {
                        double[] lt = lerps[xi, yi];

                        double val = i00[0] * lt[0] +
                                     i00[1] * lt[1] +
                                     i10[0] * lt[2] +
                                     i10[1] * lt[3] +
                                     i01[0] * lt[4] +
                                     i01[1] * lt[5] +
                                     i11[0] * lt[6] +
                                     i11[1] * lt[7];

                        blendFunc(ref outArray[xi + xShift, yi + yShift], val);
                    }
            }
    }

    public static double[,,] GeneratePerlin3D(int shiftX, int shiftY, int shiftZ, int pow2Size, int pow2Scale)
    {
        int size = 1 << pow2Size;
        int baseSize = (size >> pow2Scale) + 1;
        int sqrBaseSize = baseSize * baseSize;

        int grI = pow2Size - pow2Scale;
        double[][] gradients = _cashed3DGr[grI];
        if (gradients == null)
            _cashed3DGr[grI] = gradients = new double[baseSize * baseSize * baseSize][];

        const int mask = GTableSize - 1;
        for (int z = 0, zshift = 0; z < baseSize; z++, zshift += sqrBaseSize)
            for (int y = 0, yshift = 0; y < baseSize; y++, yshift += baseSize)
                for (int x = 0; x < baseSize; x++)
                {
                    int i = x + yshift + zshift;
                    int r = _rnd[(x + shiftX + _rnd[(y + shiftY + _rnd[(z + shiftZ) & mask]) & mask]) & mask];
                    int gi = r & mask;
                    gradients[i] = _gradientTable3D[gi];
                }

        double[,,][] lerps = _cashed3DLerps[pow2Scale];
        if (lerps == null)
            _cashed3DLerps[pow2Scale] = lerps = GetLerps3D(pow2Scale);

        double[,,] outArray = new double[size,size,size];

        int baseSize2 = baseSize - 1;
        int cellSize = 1 << pow2Scale;

        for (int z = 0; z < baseSize2; z++)
            for (int y = 0; y < baseSize2; y++)
                for (int x = 0; x < baseSize2; x++)
                {
                    int i0 = x + y * baseSize + z * sqrBaseSize;

                    double[] i000 = gradients[i0];
                    double[] i001 = gradients[i0 + 1];

                    double[] i010 = gradients[i0 + baseSize];
                    double[] i011 = gradients[i0 + baseSize + 1];
                    i0 += sqrBaseSize;
                    double[] i100 = gradients[i0];
                    double[] i101 = gradients[i0 + 1];
                    i0 += baseSize;
                    double[] i110 = gradients[i0];
                    double[] i111 = gradients[i0 + 1];


                    int xShift = x << pow2Scale;
                    int yShift = y << pow2Scale;
                    int zShift = z << pow2Scale;

                    for (int zi = 0; zi < cellSize; zi++)
                        for (int yi = 0; yi < cellSize; yi++)
                            for (int xi = 0; xi < cellSize; xi++)
                            {
                                double[] lt = lerps[xi, yi, zi];
                                outArray[xi + xShift, yi + yShift, zi + zShift] = lt[0] * i000[0] +
                                                                                  lt[1] * i000[1] +
                                                                                  lt[2] * i000[2] +
                                                                                  lt[3] * i001[0] +
                                                                                  lt[4] * i001[1] +
                                                                                  lt[5] * i001[2] +
                                                                                  lt[6] * i010[0] +
                                                                                  lt[7] * i010[1] +
                                                                                  lt[8] * i010[2] +
                                                                                  lt[9] * i011[0] +
                                                                                  lt[10] * i011[1] +
                                                                                  lt[11] * i011[2] +
                                                                                  lt[12] * i100[0] +
                                                                                  lt[13] * i100[1] +
                                                                                  lt[14] * i100[2] +
                                                                                  lt[15] * i101[0] +
                                                                                  lt[16] * i101[1] +
                                                                                  lt[17] * i101[2] +
                                                                                  lt[18] * i110[0] +
                                                                                  lt[19] * i110[1] +
                                                                                  lt[20] * i110[2] +
                                                                                  lt[21] * i111[0] +
                                                                                  lt[22] * i111[1] +
                                                                                  lt[23] * i111[2];
                            }
                }
        return outArray;
    }

    #region Cash
    const int CashSize = 32;
    static double[][][] _cashed2DGr = new double[CashSize][][];
    static double[][][] _cashed3DGr = new double[CashSize][][];
    static double[][,][] _cashed2DLerps = new double[CashSize][,][];
    static double[][,,][] _cashed3DLerps = new double[CashSize][,,][];
    #endregion

    #region Tables
    const int Pow2GTableSize = 11;
    const int GTableSize = 1 << Pow2GTableSize;
    static int[] _rnd = new int[GTableSize];
    static double[][] _gradientTable2D = new double[GTableSize][];
    static double[][] _gradientTable3D = new double[GTableSize][];

    static void InitGradients()
    {
        int seed = 1387649;
        for (int i = 0; i < _gradientTable2D.Length; i++)
        {
            double x = Noise1D(seed--);
            double y = Math.Sqrt(1.0 - x * x);
            if (Noise1D(seed--) > 0) y = -y;
            _gradientTable2D[i] = new[] {x, y};
        }


        seed = 1387649;
        for (int i = 0; i < _gradientTable3D.Length; i++)
        {
            double x = Noise1D(seed--);
            double y = Noise1D(seed--);
            double z = Noise1D(seed--);
            double invL = 1.0 / Math.Sqrt(x * x + y * y + z * z);
            _gradientTable3D[i] = new[] {x * invL, y * invL, z * invL};
        }

        seed = 1387649;
        for (int i = 0; i < GTableSize; i++)
            _rnd[i] = seed = Noise2Di(seed, i ^ 987654321);

    }
    #endregion

    static double[,][] GetLerps2D(int pow2Size)
    {
        int size = 1 << pow2Size;
        double[,][] table = new double[size,size][];
        double scale = 1.0 / size;

        for (int y = 0; y < size; y++)
            for (int x = 0; x < size; x++)
            {
                double xd = x * scale;
                double yd = y * scale;

                double xf = xd * xd * (3 - 2 * xd);
                double yf = yd * yd * (3 - 2 * yd);

                double a00 = (1.0 - xf - yf + yf * xf);
                double a10 = (xf - yf * xf);
                double a01 = (yf - yf * xf);
                double a11 = yf * xf;

                double i00X = -xd * a00;
                double i00Y = -yd * a00;
                double i10X = (1.0 - xd) * a10;
                double i10Y = -yd * a10;
                double i01X = -xd * a01;
                double i01Y = (1.0 - yd) * a01;
                double i11X = (1.0 - xd) * a11;
                double i11Y = (1.0 - yd) * a11;

                double[] vals = table[x, y] = new double[8];



                vals[0] = i00X;
                vals[1] = i00Y;
                vals[2] = i10X;
                vals[3] = i10Y;
                vals[4] = i01X;
                vals[5] = i01Y;
                vals[6] = i11X;
                vals[7] = i11Y;
            }
        return table;
    }
    static double[,,][] GetLerps3D(int pow2Size)
    {
        int size = 1 << pow2Size;
        double[,,][] table = new double[size,size,size][];
        double scale = 1.0 / size;
        for (int z = 0; z < size; z++)
            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                {
                    double fx = x * scale;
                    double fy = y * scale;
                    double fz = z * scale;

                    //lerps
                    double z1 = fz * fz * (3 - 2 * fz);
                    double y1 = fy * fy * (3 - 2 * fy);
                    double x1 = fx * fx * (3 - 2 * fx);

                    double z0 = (1 - z1);
                    double y0 = (1 - y1);
                    double x0 = (1 - x1);

                    double l000 = z0 * y0 * x0;
                    double l001 = z0 * y0 * x1;
                    double l010 = z0 * y1 * x0;
                    double l011 = z0 * y1 * x1;
                    double l100 = z1 * y0 * x0;
                    double l101 = z1 * y0 * x1;
                    double l110 = z1 * y1 * x0;
                    double l111 = z1 * y1 * x1;

                    //dots
                    double dx0 = -fx;
                    double dx1 = 1.0 - fx;
                    double dy0 = -fy;
                    double dy1 = 1.0 - fy;
                    double dz0 = -fz;
                    double dz1 = 1.0 - fz;

                    double v000X = dx0 * l000;
                    double v000Y = dy0 * l000;
                    double v000Z = dz0 * l000;
                    double v001X = dx1 * l001;
                    double v001Y = dy0 * l001;
                    double v001Z = dz0 * l001;
                    double v010X = dx0 * l010;
                    double v010Y = dy1 * l010;
                    double v010Z = dz0 * l010;
                    double v011X = dx1 * l011;
                    double v011Y = dy1 * l011;
                    double v011Z = dz0 * l011;
                    double v100X = dx0 * l100;
                    double v100Y = dy0 * l100;
                    double v100Z = dz1 * l100;
                    double v101X = dx1 * l101;
                    double v101Y = dy0 * l101;
                    double v101Z = dz1 * l101;
                    double v110X = dx0 * l110;
                    double v110Y = dy1 * l110;
                    double v110Z = dz1 * l110;
                    double v111X = dx1 * l111;
                    double v111Y = dy1 * l111;
                    double v111Z = dz1 * l111;

                    double[] vals = table[x, y, z] = new double[24];
                    vals[0] = v000X;
                    vals[1] = v000Y;
                    vals[2] = v000Z;
                    vals[3] = v001X;
                    vals[4] = v001Y;
                    vals[5] = v001Z;
                    vals[6] = v010X;
                    vals[7] = v010Y;
                    vals[8] = v010Z;
                    vals[9] = v011X;
                    vals[10] = v011Y;
                    vals[11] = v011Z;
                    vals[12] = v100X;
                    vals[13] = v100Y;
                    vals[14] = v100Z;
                    vals[15] = v101X;
                    vals[16] = v101Y;
                    vals[17] = v101Z;
                    vals[18] = v110X;
                    vals[19] = v110Y;
                    vals[20] = v110Z;
                    vals[21] = v111X;
                    vals[22] = v111Y;
                    vals[23] = v111Z;
                }
        return table;
    }

    static double Noise1D(int x)
    {
        //diapazone[-1: 1]
        x = (x << 13) ^ x;
        return (1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
    }
    static double Noise2D(int x, int y)
    {
        //diapazone[-1: 1]
        x = x + y * 57;
        x = (x << 13) ^ x;
        return (1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
    }
    static int Noise2Di(int x, int y)
    {
        int a = x ^ (y << 15);
        a = (a ^ 61) ^ (a >> 16);
        a = a + (a << 3);
        a = a ^ (a >> 4);
        a = a * 0x27d4eb2d;
        a = a ^ (a >> 15);
        return a;
        /*
        int h = x ^ (y << 15);
        h ^= h << 13;
        h ^= (h >> 20) ^ (h >> 12);
        return h ^ (h >> 7) ^ (h >> 4);
        //*/
    }
    static double Noise3D(int x, int y, int z)
    {
        //diapazone[-1: 1]
        x = x + y * 57;
        x = x + z * 79;
        x = (x << 13) ^ x;
        return (1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
    }
    #endregion
}