﻿using System.Collections.Generic;
using UnityEngine;

public static class TransformTools
{
    #region Render
    /// <summary>
    /// Difference between fast and slow variants 3% - 20%, but fast variant not support scale
    /// </summary>
    public static void Render(Transform T, Vector3 position, Quaternion rotation, int layer, Camera camera, bool fast = true)
    {
        Renderer[] renderers = T.GetComponentsInChildren<Renderer>();
        HashSet<Transform> transforms = new HashSet<Transform>(new[] { T });

        foreach (Renderer r in renderers)
        {
            Transform t = r.transform;
            while (transforms.Add(t)) t = t.parent;
            //for (; transforms.Add(t); t = t.parent) ;
        }
        if (fast) RenderReqursiveFast(T, transforms, position, rotation, layer, camera);
        else RenderReqursiveWithMatrix(T, transforms, Matrix4x4.TRS(position, rotation, Vector3.one), layer, camera);
    }

    static void RenderReqursiveWithMatrix(Transform t, HashSet<Transform> transforms, Matrix4x4 matrix, int layer, Camera camera)
    {
        if (!transforms.Contains(t)) return;
        matrix = matrix * Matrix4x4.TRS(t.localPosition, t.localRotation, t.localScale);

        Renderer r = t.GetComponent<Renderer>();
        if (r)
        {
            SkinnedMeshRenderer mr = r as SkinnedMeshRenderer;
            Mesh mesh;
            Material[] materials;
            if (mr)
            {
                mesh = mr.sharedMesh;
                materials = mr.sharedMaterials;
            }
            else
            {
                MeshFilter mf = t.GetComponent<MeshFilter>();
                mesh = mf.sharedMesh;
                materials = r.sharedMaterials;
            }

            int smc = mesh.subMeshCount;
            for (int i = 0; i < smc; i++)
            {
                Graphics.DrawMesh(mesh, matrix, materials[i], layer, camera, i);
            }
        }
        foreach (Transform child in t)
        {
            RenderReqursiveWithMatrix(child, transforms, matrix, layer, camera);
        }
    }
    static void RenderReqursiveFast(Transform t, HashSet<Transform> transforms, Vector3 position, Quaternion rotation, int layer, Camera camera)
    {
        if (!transforms.Contains(t)) return;

        position += rotation * t.localPosition;
        rotation = rotation * t.localRotation;

        Renderer r = t.GetComponent<Renderer>();
        if (r)
        {
            SkinnedMeshRenderer mr = r as SkinnedMeshRenderer;
            Mesh mesh;
            Material[] materials;
            if (mr)
            {
                mesh = mr.sharedMesh;
                materials = mr.sharedMaterials;
            }
            else
            {
                MeshFilter mf = t.GetComponent<MeshFilter>();
                mesh = mf.sharedMesh;
                materials = r.sharedMaterials;
            }

            int smc = mesh.subMeshCount;
            for (int i = 0; i < smc; i++)
            {
                Graphics.DrawMesh(mesh, position, rotation, materials[i], layer, camera, i);
            }
        }
        foreach (Transform child in t)
        {
            RenderReqursiveFast(child, transforms, position, rotation, layer, camera);
        }
    }
    #endregion



    public static Vector3 GetPosition(Matrix4x4 m)
    {
        return m.MultiplyPoint(Vector3.zero);
    }
    public static Quaternion GetRotation(Matrix4x4 m)
    {
        return Quaternion.LookRotation(m.MultiplyVector(Vector3.forward), m.MultiplyVector(Vector3.up));
    }
   
    /// <summary>
    /// Fastest method
    /// </summary>
    public static void GetPosRotRelativeToParent(Transform parent, Transform child, out Vector3 position, out Quaternion rotation)// todo 
    {
        Matrix4x4 m = GetMatrixRelativeToParent(parent, child);
        rotation = GetRotation(m);
        position = GetPosition(m);
    }
    /// <summary>
    /// Slower than GetPosRotRelativeToParent
    /// </summary>
    public static Vector3 GetPositionRelativeToParent(Transform parent, Transform child)
    {
        Vector3 position;
        Quaternion rotation;
        GetPosRotRelativeToParent(parent, child, out position, out rotation);
        return position;
    }
    /// <summary>
    /// Slower than GetPosRotRelativeToParent
    /// </summary>
    public static Quaternion GetRotationRelativeToParent(Transform parent, Transform child)
    {
        Vector3 position;
        Quaternion rotation;
        GetPosRotRelativeToParent(parent, child, out position, out rotation);
        return rotation;
    }

    public static Matrix4x4 GetMatrixRelativeToTransform(Transform parent, Transform my, Transform another)
    {
        Matrix4x4 a = GetMatrixRelativeToParent(parent, another);
        Matrix4x4 b = GetMatrixRelativeToParent(parent, my);
        return Matrix4x4.Inverse(b) * a;
    }

    public static Matrix4x4 GetMatrixRelativeToParent(Transform parent, Transform child)
    {
        Matrix4x4 tempMatrix = Matrix4x4.TRS(child.localPosition, child.localRotation, child.localScale);
        for (int i = 0; i < 100; i++)
        {
            child = child.parent;
            if (child == parent) return tempMatrix;
            tempMatrix = Matrix4x4.TRS(child.localPosition, child.localRotation, child.localScale) * tempMatrix;
        }
        return Matrix4x4.zero;
    }
    public static Transform GetMutualTransform(Transform a, Transform b)
    {
        if (a == b) return a;
        HashSet<Transform> parents = new HashSet<Transform>(new[] { a, b });
        for (int i = 0; i < 100; i++)
        {
            if (a != null)
            {
                if (!parents.Add(a.parent)) return a.parent;
                a = a.parent;
            }

            if (b != null)
            {
                if (!parents.Add(b.parent)) return b.parent;
                b = b.parent;
            }
            else if (a == null) return null;
        }
        return null;
    }




}