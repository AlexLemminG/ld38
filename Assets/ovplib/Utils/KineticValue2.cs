using UnityEngine;

public class KineticValue2
{
    public Vector2 Value { get; private set; }

    public Vector2 Velocity
    {
        get
        {
            if (_velocityCalculated) return _velocity;
            _velocity = (Value - _lastValue) / _dt;
            _lastValue = Value;
            _velocityCalculated = true;
            return _velocity;
        }
    }

    public Vector2 Acceleration
    {
        get
        {
            if (_accelerationCalculated) return _acceleration;
            _acceleration = (Velocity - _lastVelocity) / _dt;
            _lastVelocity = Velocity;
            _accelerationCalculated = true;
            return _acceleration;
        }
    }

    Vector2 _lastValue;
    Vector2 _lastVelocity;

    Vector2 _velocity;
    Vector2 _acceleration;

    bool _velocityCalculated;
    bool _accelerationCalculated;
    bool _initialized;

    float _dt;

    public void Update(Vector2 value, float dt)
    {
        if (!_initialized)
        {
            _initialized = true;
            _lastValue = value;
        }

        Value = value;
        _dt = dt;
        _velocityCalculated = false;
        _accelerationCalculated = false;
    }
}