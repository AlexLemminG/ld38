﻿using System;
using UnityEngine;

public class SumArrayV3
{
    Vector3[] _vals;
    public int Length { get; private set; }
    public int CurrentLength { get; private set; }
    int _lastIndex = -1;
    public Vector3 Sum { get; private set; }
    public Vector3 Average { get { return Sum * _invLength; } }
    float _invLength;
    bool _full = false;

    public SumArrayV3(int length)
    {
        if (length < 1) throw new Exception("Length must be greater than zero. Length:" + length);
        _vals = new Vector3[length];
        _invLength = 1f / (float)length;
        Length = length;
    }

    public void Clear()
    {
        Array.Clear(_vals, 0, _vals.Length);
        _lastIndex = -1;
        CurrentLength = 0;
        Sum = Vector3.zero;
        _full = false;
    }

    public void AddValue(Vector3 val)
    {
        if (!_full)
        {
            _lastIndex++;
            CurrentLength = _lastIndex;
            if (_lastIndex == Length)
            {
                _lastIndex = 0;
                _full = true;
            }
        }
        else
        {
            _lastIndex++;
            if (_lastIndex == Length) _lastIndex = 0;
        }
        Sum -= _vals[_lastIndex];
        _vals[_lastIndex] = val;
        Sum += val;
    }
}