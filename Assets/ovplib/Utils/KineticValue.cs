public class KineticValue
{
    public float Value { get; private set; }

    public float Velocity
    {
        get
        {
            if (_velocityCalculated) return _velocity;
            _velocity = (Value - _lastValue)/_dt;
            _lastValue = Value;
            _velocityCalculated = true;
            return _velocity;
        }
    }

    public float Acceleration
    {
        get
        {
            if (_accelerationCalculated) return _acceleration;
            _acceleration = (Velocity - _lastVelocity)/_dt;
            _lastVelocity = Velocity;
            _accelerationCalculated = true;
            return _acceleration;
        }
    }

    float _lastValue;
    float _lastVelocity;

    float _velocity;
    float _acceleration;

    bool _velocityCalculated;
    bool _accelerationCalculated;
    bool _initialized;

    float _dt;

    public void Update(float value, float dt)
    {
        if (!_initialized)
        {
            _initialized = true;
            _lastValue = value;
        }
        Value = value;
        _dt = dt;
        _velocityCalculated = false;
        _accelerationCalculated = false;
    }
}