﻿using System;

public class EvalCurveF
{
    float[] _points;
    float[] _slopes;
    float _invStep;
    int _length;

    public EvalCurveF(Func<float, float> func, int length, float step, bool lastSlopeZero = false)
    {
        _points = new float[length];
        _slopes = new float[length];
        _length = length;
        _invStep = 1.0f / step;
        float p0 = func(0f);
        for (int i = 0; i < length; i++)
        {
            float p1 = func((i + 1) * step);
            _slopes[i] = (p1 - p0) * _invStep;
            _points[i] = p0 - (i * step) * _slopes[i];
            p0 = p1;
        }
        if (lastSlopeZero)
        {
            int i = length - 1;
            _slopes[i] = 0f;
            _points[i] = func(i * step);
        }
    }

    public float Evaluate(float val)
    {
        int i = (int) (val * _invStep);
        if (i >= _length) i = _length - 1;
        return _points[i] + _slopes[i] * val;
    }
}