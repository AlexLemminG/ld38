[System.Serializable]
public class PIDController
{
    public float Kp;
    public float Ki;
    public float Kd;


    float _integral;
    float _lastError;


    public float Update(float error, float dt)
    {
        _integral = _integral + (error*dt);

        float derivative = (error - _lastError)/dt;
        _lastError = error;

        return (Kp*error) + (Ki*_integral) + (Kd*derivative);
    }
}