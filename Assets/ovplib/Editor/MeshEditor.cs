﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections;

[CanEditMultipleObjects]
[CustomEditor(typeof(Mesh))]
public class MeshEditor : Editor
{
    PreviewRenderUtility _previewUtility;
    Vector2 _previewDir = new Vector2(120f, -20f);
    Material _mMaterial;
    Material _mWireMaterial;

    public override void OnInspectorGUI()
    {
        Mesh mesh = target as Mesh;
        if (mesh == null) return;

        if (GUILayout.Button("Save")) Save(mesh);

    }
    public override bool HasPreviewGUI()
    {
        return target != null;
    }
    public override void OnPreviewSettings()
    {
        if (!ShaderUtil.hardwareSupportsRectRenderTexture) return;
        GUI.enabled = true;
        Init();
    }

    public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
    {
        if (!ShaderUtil.hardwareSupportsRectRenderTexture) return null;
        Init();
        _previewUtility.BeginStaticPreview(new Rect(0f, 0f, (float) width, (float) height));
        DoRenderPreview();
        return _previewUtility.EndStaticPreview();
    }

    public override string GetInfoString()
    {
        string str;
        Mesh mesh = target as Mesh;
        string str1 = string.Concat(new object[] {mesh.vertexCount, " verts, "});
        int num = mesh.subMeshCount;
        if (num > 1)
        {
            str = str1;
            str1 = string.Concat(new object[] {str, ", ", num, " submeshes"});
        }
        int num1 = mesh.blendShapeCount;
        if (num1 > 1)
        {
            str = str1;
            str1 = string.Concat(new object[] {str, ", ", num1, " blendShapes"});
        }
        // str1 = string.Concat(str1, "\n", InternalMeshUtil.GetVertexFormat(mesh));
        return str1;
    }
    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        if (!ShaderUtil.hardwareSupportsRectRenderTexture)
        {
            if (Event.current.type == EventType.Repaint)
            {
                EditorGUI.DropShadowLabel(new Rect(r.x, r.y, r.width, 40f), "Mesh preview requires\nrender texture support");
            }
            return;
        }
        Init();
        _previewDir = Drag2D(this._previewDir, r);
        if (Event.current.type != EventType.Repaint)
        {
            return;
        }
        _previewUtility.BeginPreview(r, background);
        DoRenderPreview();
        GUI.DrawTexture(r, this._previewUtility.EndPreview(), ScaleMode.StretchToFill, false);
    }

    public static Vector2 Drag2D(Vector2 scrollPosition, Rect position)
    {
        int controlID = GUIUtility.GetControlID("Slider".GetHashCode(), FocusType.Passive);
        Event @event = Event.current;
        switch (@event.GetTypeForControl(controlID))
        {
            case EventType.MouseDown:
                {
                    if (position.Contains(@event.mousePosition) && position.width > 50f)
                    {
                        GUIUtility.hotControl = controlID;
                        @event.Use();
                        EditorGUIUtility.SetWantsMouseJumping(1);
                    }
                    return scrollPosition;
                }
            case EventType.MouseUp:
                {
                    if (GUIUtility.hotControl == controlID)
                    {
                        GUIUtility.hotControl = 0;
                    }
                    EditorGUIUtility.SetWantsMouseJumping(0);
                    return scrollPosition;
                }
            case EventType.MouseMove:
                {
                    return scrollPosition;
                }
            case EventType.MouseDrag:
                {
                    if (GUIUtility.hotControl == controlID)
                    {
                        scrollPosition = scrollPosition - (((@event.delta * (float) ((!@event.shift ? 1 : 3))) / Mathf.Min(position.width, position.height)) * 140f);
                        scrollPosition.y = Mathf.Clamp(scrollPosition.y, -90f, 90f);
                        @event.Use();
                        GUI.changed = true;
                    }
                    return scrollPosition;
                }
            default:
                {
                    return scrollPosition;
                }
        }
    }
    void Init()
    {
        if (_previewUtility != null) return;

        _previewUtility = new PreviewRenderUtility();
        _previewUtility.m_CameraFieldOfView = 30f;
        _mMaterial = new Material(Shader.Find("Standard"));
        _mMaterial.color = new Color(0.7f, 0.7f, 0.7f, 1f);

        _mWireMaterial = new Material(Shader.Find("Hidden/Internal-Colored"));
        _mWireMaterial.hideFlags = HideFlags.HideAndDontSave;
        _mWireMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        _mWireMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        _mWireMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        _mWireMaterial.SetInt("_ZWrite", 0);
        _mWireMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.LessEqual);
        _mWireMaterial.SetFloat("_ZBias", -1);
    }
    void DoRenderPreview()
    {
        Mesh mesh = target as Mesh;
        Bounds bound = mesh.bounds;
        float single = bound.extents.magnitude;
        if (float.IsInfinity(single)) return;
        float single1 = single * 4f;
        _previewUtility.m_Camera.transform.position = -Vector3.forward * single1;
        _previewUtility.m_Camera.transform.rotation = Quaternion.identity;
        _previewUtility.m_Camera.nearClipPlane = single1 - single * 1.1f;
        _previewUtility.m_Camera.farClipPlane = single1 + single * 1.1f;
        _previewUtility.m_Light[0].intensity = 0.7f;
        _previewUtility.m_Light[0].transform.rotation = Quaternion.Euler(40f, 40f, 0f);
        _previewUtility.m_Light[1].intensity = 0.7f;
        Color color = new Color(0.1f, 0.1f, 0.1f, 0f);
        InternalEditorUtility.SetCustomLighting(_previewUtility.m_Light, color);
        Quaternion quaternion = Quaternion.Euler(_previewDir.y, 0f, 0f) * Quaternion.Euler(0f, this._previewDir.x, 0f);
        Vector3 vector3 = quaternion * -bound.center;
        bool flag = RenderSettings.fog;
        Unsupported.SetRenderSettingsUseFogNoDirty(false);
        int num = mesh.subMeshCount;
        _previewUtility.m_Camera.clearFlags = CameraClearFlags.Nothing;
        for (int i = 0; i < num; i++)
            _previewUtility.DrawMesh(mesh, vector3, quaternion, _mMaterial, i);
        
        _previewUtility.m_Camera.Render();
        _previewUtility.m_Camera.clearFlags = CameraClearFlags.Nothing;


        GL.wireframe = true;
        for (int j = 0; j < num; j++)
            _previewUtility.DrawMesh(mesh, vector3, quaternion, _mWireMaterial, j);
        
        _previewUtility.m_Camera.Render();
        Unsupported.SetRenderSettingsUseFogNoDirty(flag);
        GL.wireframe = false;
        InternalEditorUtility.RemoveCustomLighting();
    }
    
    static void Save(Mesh mesh)
    {
        string fileName = EditorUtility.SaveFilePanelInProject("Save mesh", "mesh", "asset", "Save mesh");
        if (fileName == "") return;
        AssetDatabase.CreateAsset(mesh, fileName);
    }
}