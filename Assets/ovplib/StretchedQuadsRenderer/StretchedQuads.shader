Shader "Hiden/StretchedQuads"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Width ("_Width", Float) = 0.1

	}
	SubShader
	{
		Tags { "Queue" = "Transparent" } 
        Blend One One
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Mode Off }

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct data
			{
				float4 vertex : POSITION;
				float3 vals : TEXCOORD0;// xy: shifts(-1;1), z: line start/end(-1;1)
				float3 direction : TEXCOORD1;
				float3 vals2 : TEXCOORD2;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float _Width;
			
			v2f vert (data v)
			{
				v2f o;

				float2 screenFix = float2(_ScreenParams.y / _ScreenParams.x, 1);


				float3 perpendicular = normalize(cross((v.vertex.xyz - _WorldSpaceCameraPos),  v.direction));
				float3 normDirection = normalize(v.direction);

				float2 size = v.vals.xy * _Width * v.vals2.x;

				v.vertex.xyz += size.x * perpendicular;
				v.vertex.xyz += size.y * normDirection;
				v.vertex.xyz += v.vals.z * v.direction;

				o.vertex = mul(UNITY_MATRIX_VP, v.vertex);

				o.color = v.color;
				o.uv = v.vals.xy * 0.5 + 0.5;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return tex2D(_MainTex, i.uv) * i.color;
			}
			ENDCG
		}
	}
}
