using UnityEngine;

public class SimpleStretchedQuadsRenderer
{
    public Material Material;
    Vector3[][] _texCoord;

    public SimpleStretchedQuadsRenderer()
    {
        //Material = new Material(Shader.Find("Hiden/StretchedQuads"));

        _texCoord = new[]
        {
            new[]
            {
                new Vector3(0, 0, -1f),
                new Vector3(1, 0, +1f),
                new Vector3(1, 1, +1f),
                new Vector3(0, 1, -1f)
            }
        };
    }

    public SimpleStretchedQuadsRenderer(Texture texture, Rect[] tiles)
    {
        _texCoord = new Vector3[tiles.Length][];
        for (int i = 0; i < tiles.Length; i++)
        {
            Rect tile = tiles[i];
            Vector3[] array = new Vector3[4];
            array[0] = new Vector3(tile.xMin, tile.yMin, -1f);
            array[1] = new Vector3(tile.xMax, tile.yMin, +1f);
            array[2] = new Vector3(tile.xMax, tile.yMax, +1f);
            array[3] = new Vector3(tile.xMin, tile.yMax, -1f);
            _texCoord[i] = array;
        }
    }
    /// <summary>OnRenderObject()</summary>
    public void Begin()
    {
        Material.SetPass(0);
        GL.Begin(GL.QUADS);
    }

    /// <summary>OnRenderObject()</summary>
    public void DrawLine(Vector3 position0, Vector3 position1, int tile, Color color)
    {
        GL.Color(color);
        Vector3[] texCoord = _texCoord[tile];
        Vector3 direction = position1 - position0;
        GL.MultiTexCoord(1, direction);
        GL.MultiTexCoord(0, texCoord[0]);
        GL.Vertex(position0);
        GL.MultiTexCoord(0, texCoord[1]);
        GL.Vertex(position0);

        GL.MultiTexCoord(0, texCoord[2]);
        GL.Vertex(position1);
        GL.MultiTexCoord(0, texCoord[3]);
        GL.Vertex(position1);
    }

    /// <summary>OnRenderObject()</summary>
    public void DrawLine2(Vector3 position0, Vector3 direction, int tile, Color color)
    {
        GL.Color(color);
        Vector3[] texCoord = _texCoord[tile];
        Vector3 position1 = position0 + direction;
        GL.MultiTexCoord(1, direction);
        GL.MultiTexCoord(0, texCoord[0]);
        GL.Vertex(position0);
        GL.MultiTexCoord(0, texCoord[1]);
        GL.Vertex(position0);

        GL.MultiTexCoord(0, texCoord[2]);
        GL.Vertex(position1);
        GL.MultiTexCoord(0, texCoord[3]);
        GL.Vertex(position1);
    }
    /// <summary>OnRenderObject()</summary>
    public void End()
    {
        GL.End();
    }
}