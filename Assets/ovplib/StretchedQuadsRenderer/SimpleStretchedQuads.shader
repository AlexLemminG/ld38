﻿Shader "Hiden/SimpleStretchedQuads"
{
	SubShader
	{
		Tags { "Queue" = "Transparent" } 
        Blend One One
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Mode Off }

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct data
			{
				float4 vertex : POSITION;
				float3 vals : TEXCOORD0;// xy: uv, z: shift
				float3 direction : TEXCOORD1;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float _Width;
			
			v2f vert (data v)
			{
				v2f o;
				v.vertex.xyz += v.vals.z * normalize(cross((v.vertex.xyz - _WorldSpaceCameraPos),  v.direction)) * _Width;
				o.vertex = mul(UNITY_MATRIX_VP, v.vertex);

				o.color = v.color;
				o.uv = v.vals.xy;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//return 1;
				return tex2D(_MainTex, i.uv) * i.color;
			}
			ENDCG
		}
	}
}
