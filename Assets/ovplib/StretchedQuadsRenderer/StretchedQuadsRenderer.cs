﻿using System.Collections.Generic;
using UnityEngine;

public class StretchedQuadsRenderer : MonoBehaviour
{
    public static StretchedQuadsRenderer Instance;
    public Material Material;


    readonly List<Line> _lines = new List<Line>();
    int _position;
    int _linesCount;


    void Awake()
    {
        Instance = this;
        for (int i = 0; i < 64; i++) _lines.Add(new Line());
        _linesCount = _lines.Count;
    }

    // 0 - 1
    // | \ |
    // 2 - 3
    // | \ |
    // 4 - 5
    // | \ |
    // 6 - 7
    /// <summary>OnRenderObject()</summary>
    public void DrawLine(Vector3 position, Vector3 direction, Color color, float size)
    {
        GL.Color(color);
        GL.MultiTexCoord(1, direction);
        GL.MultiTexCoord3(2, size, 0, 0);
        GL.MultiTexCoord(0, new Vector3(-1, 1f, 1));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(1, 1f, 1));
        GL.Vertex(position);

        GL.MultiTexCoord(0, new Vector3(1, 0f, 1));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(-1, 0f, 1));
        GL.Vertex(position);


        GL.MultiTexCoord(0, new Vector3(1, 0f, 1));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(-1, 0f, 1));
        GL.Vertex(position);

        GL.MultiTexCoord(0, new Vector3(-1, 0f, 0));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(1, 0f, 0));
        GL.Vertex(position);


        GL.MultiTexCoord(0, new Vector3(-1, 0f, 0));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(1, 0f, 0));
        GL.Vertex(position);

        GL.MultiTexCoord(0, new Vector3(1, -1f, 0));
        GL.Vertex(position);
        GL.MultiTexCoord(0, new Vector3(-1, -1f, 0));
        GL.Vertex(position);
    }

    void OnRenderObject()
    {
        Material.SetPass(0);
        GL.Begin(GL.QUADS);
        for (int i = 0; i < _position; i++)
        {
            Line line = _lines[i];
            DrawLine(line.Position, line.Direction, line.Color, line.Size);
        }
        _position = 0;
        GL.End();
    }

    public void Draw(Vector3 position, Vector3 direction, Color color, float size)
    {
        if (_position >= _linesCount)
        {
            for (int i = 0; i < 64; i++)
                _lines.Add(new Line());
            _linesCount = _lines.Count;
        }
        Line line = _lines[_position];
        _position++;
        line.Position = position;
        line.Direction = direction;
        line.Color = color;
        line.Size = size;
    }

    public class Line
    {
        public Vector3 Position;
        public Vector3 Direction;
        public Color Color;
        public float Size;
    }
}