using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Shape))]
public class ShapeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (GUI.Button(position, label))
        {
            Shape shape = (Shape) GetObject(property);
            ShapeEditorWindow.Show(shape);
        }
    }

    static object GetObject(SerializedProperty property)
    {
        object obj = property.serializedObject.targetObject;
        return obj.GetType().GetField(property.name).GetValue(obj);
    }
}