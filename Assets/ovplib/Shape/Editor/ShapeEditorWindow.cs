using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class ShapeEditorWindow : EditorWindow
{
    public Shape Shape;
    static Material _material;
    public static Material GetMaterial()
    {
        if (!_material)
        {
            _material = new Material(Shader.Find("Hidden/Internal-Colored"));
            _material.hideFlags = HideFlags.HideAndDontSave;
            _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            _material.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            _material.SetInt("_ZWrite", 0);
            _material.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
        }
        return _material;
    }


    Vector2[] _points;
    Rect _rect;
    private GUIStyle _label;



    public static void Show(Shape shape)
    {
        ShapeEditorWindow window = (ShapeEditorWindow)EditorWindow.GetWindow(typeof(ShapeEditorWindow));
        window.Shape = shape;
        window.Show();
        //window.ShowAuxWindow();
    }

    private void OnDestroy()
    {

    }

    private void OnDisable()
    {

    }

    private void OnEnable()
    {

    }

    private int _selected = -1;
    private Vector2 _mouseOffset;
    private void OnGUI()
    {
        Rect posRect = position;
        posRect.position = Vector2.zero;

        if (_rect != posRect || _points == null)
        {
            _rect = posRect;
            _points = Transform(Shape.Points, _rect);
        }
        Draw(_rect.size, new Vector2(5, 5), _points, Shape.GetTriangles());
       


        if (Event.current.type == EventType.MouseDrag && _selected!= -1)
        {
            _points[_selected] = Event.current.mousePosition + _mouseOffset;
            if (Event.current.control)
                _points[_selected] = Snap(_points[_selected], _rect.size*0.01f);
            Repaint();
            Shape.Points = InverseTransform(_points, _rect);
        }

        if (Event.current.type == EventType.mouseUp && Event.current.button == 0 && _selected != -1)
        {
            _selected = -1;
        }


        if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && Event.current.clickCount == 2 && _selected == -1)
        {
            AddPoint(ref _points, 15f);
            Repaint();
            Shape.Points = InverseTransform(_points, _rect);
        }
    


        for (int i = 0; i < _points.Length; i++)
        {
            const float size = 25;
            const float halfSize = size*0.5f;
            Rect rect = new Rect(_points[i].x - halfSize, _points[i].y - halfSize, size, size);
            EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0 &&
                rect.Contains(Event.current.mousePosition))
            {
                _selected = i;
                _mouseOffset = _points[i] - Event.current.mousePosition;
                Event.current.Use();
            }
            else if (Event.current.type == EventType.ContextClick && rect.Contains(Event.current.mousePosition))
            {
                GenericMenu genericMenu = new GenericMenu();
                genericMenu.AddItem(new GUIContent("Remove"), false, Remove, i);
                genericMenu.ShowAsContext();
                Event.current.Use();
            }
        }

        if (Event.current.control)
            for (int i = 0; i < _points.Length; i++)
            {
                if (_label == null)
                {
                    _label = new GUIStyle();
                    _label.normal.textColor = Color.white;
                    _label.fontStyle = FontStyle.Bold;
                }
                Rect rect = new Rect(_points[i], new Vector2(80, 20));
                GUI.Label(rect, Shape.Points[i].ToString("0.00"), _label);
                //GUI.Label(rect, "[" + i + "]" + Shape.Points[i].ToString("0.000"), _label);
            }

    }

    void Remove(object data)
    {
        ArrayUtility.RemoveAt(ref _points, (int) data);
        Shape.Points = InverseTransform(_points, _rect);
    }


    static void Draw(Vector2 size, Vector2 dotSize, Vector2[] points, int[] triangles)
    {
        if (Event.current.type != EventType.Repaint) return;

        GetMaterial().SetPass(0);
        DrawBackground(new Color(.15f, .15f, .15f, 1), size);
        DrawGrid(new Color(.3f, .3f, .3f, 1), size);

        DrawTriangles(points, triangles);   
        
        DrawLines(Color.red, points);

        DrawDots(Color.red, points, dotSize);
    }

    static void DrawBackground(Color color, Vector2 size)
    {
        GL.Begin(GL.QUADS);
        GL.Color(color);
        GL.Vertex3(0, 0, 0);
        GL.Vertex3(0, size.y, 0);
        GL.Vertex3(size.x, size.y, 0);
        GL.Vertex3(size.x, 0, 0);
        GL.End();
    }
    static void DrawDots(Color color, Vector2[] points, Vector2 dotSize)
    {
        GL.Begin(GL.QUADS);
        GL.Color(color);
        for (int i = 0; i < points.Length; i++)
        {
            Vector3 p = points[i] - dotSize;
            GL.Vertex(p);
            p.y = points[i].y + dotSize.y;
            GL.Vertex(p);
            p.x = points[i].x + dotSize.x;
            GL.Vertex(p);
            p.y = points[i].y - dotSize.y;
            GL.Vertex(p);
        }
        GL.End();
    }

    static void DrawLines(Color color, Vector2[] points)
    {
        GL.Begin(GL.LINES);
        GL.Color(color);
        for (int i = 1; i < points.Length; i++)
        {
            GL.Vertex(points[i - 1]);
            GL.Vertex(points[i]);
        }
        GL.Vertex(points[0]);
        GL.Vertex(points[points.Length - 1]);
        GL.End();
    }




    static void DrawGrid(Color color, Vector2 size)
    {
        const int count = 10;
        Vector2 step = size/count;
        GL.Begin(GL.LINES);
        GL.Color(color);
        for (int i = 0; i < count; i++)
        {
            GL.Vertex3(0, i*step.y, 0);
            GL.Vertex3(size.x, i*step.y, 0);
            GL.Vertex3(i*step.x, 0, 0);
            GL.Vertex3(i*step.x, size.y, 0);
        }
        GL.End();
    }


    static void DrawTriangles(Vector2[] points, int[] triangles)
    {
        if (triangles == null) return;
        if (Event.current.type != EventType.Repaint) return;

        GL.Begin(GL.TRIANGLES);
        GL.Color(new Color(0, 1, 0, 0.1f));

        for (int i = 0; i < triangles.Length; i += 3)
        {
            Vector2 p0 = points[triangles[i]];
            Vector2 p1 = points[triangles[i + 1]];
            Vector2 p2 = points[triangles[i + 2]];
            GL.Vertex(p0);
            GL.Vertex(p1);
            GL.Vertex(p2);
        }
        GL.End();


        GL.Begin(GL.LINES);
        GL.Color(Color.green);

        for (int i = 0; i < triangles.Length; i+=3)
        {
            Vector2 p0 = points[triangles[i]];
            Vector2 p1 = points[triangles[i+1]];
            Vector2 p2 = points[triangles[i+2]];
            GL.Vertex(p0);
            GL.Vertex(p1);
            GL.Vertex(p1);
            GL.Vertex(p2);
            GL.Vertex(p2);
            GL.Vertex(p0);
        }
        GL.End();
    }


    public static void AddPoint(ref Vector2[] vertices , float minDistance)
    {
        float line = HandleUtility.DistanceToLine(vertices[0], vertices[vertices.Length-1]);
        int num = 0;
        for (int i = 1; i < vertices.Length; i++)
        {
            float single = HandleUtility.DistanceToLine(vertices[i - 1], vertices[i]);
            if (single < line)
            {
                line = single;
                num = i;
            }
        }

        if (line < minDistance)
            ArrayUtility.Insert(ref vertices, num, Event.current.mousePosition);
    }




    static Vector2[] Transform(Vector2[] vecs, Rect rect)
    {
        vecs = (Vector2[])vecs.Clone();
        for (int i = 0; i < vecs.Length; i++)
            vecs[i] = Vector2.Scale(vecs[i], rect.size) + rect.position;
        return vecs;
    }

    static Vector2 Transform(Vector2 vec, Rect rect)
    {
        return Vector2.Scale(vec, rect.size) + rect.position;
    }

    static Vector2 InverseTransform(Vector2 vec, Rect rect)
    {
        Vector2 invSize = new Vector2(1f / rect.size.x, 1f / rect.size.y);
        return Vector2.Scale(vec - rect.position, invSize);
    }

    static Vector2[] InverseTransform(Vector2[] vecs, Rect rect)
    {
        vecs = (Vector2[])vecs.Clone();
        Vector2 invSize = new Vector2(1f/rect.size.x, 1f/rect.size.y);
        for (int i = 0; i < vecs.Length; i++)
            vecs[i] = Vector2.Scale(vecs[i] - rect.position, invSize);
        return vecs;
    }

    private static Vector2 Snap(Vector2 vec, Vector2 val)
    {
        vec.x = Mathf.Round(vec.x/val.x)*val.x;
        vec.y = Mathf.Round(vec.y/val.y)*val.y;
        return vec;
    }
}