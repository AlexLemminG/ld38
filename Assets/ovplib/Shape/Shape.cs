using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR

#endif

[Serializable]
public class Shape
{
    public Vector2[] Points = new[] {new Vector2(0, 0), new Vector2(0.5f, 1), new Vector2(1, 0)};

    public int[] GetTriangles()
    {
        if (Points == null || Points.Length < 3) return null;
        if (IntersectAnyLines(Points)) return null;


        bool rightInside = RightInside(Points);


        LinkedList<int> list = new LinkedList<int>();
        for (int i = 0; i < Points.Length; i++)
            list.AddLast(i);

        List<int> triangles = new List<int>();

        int q = 0;
        for (LinkedListNode<int> node0 = list.First; list.Count > 3;)
        {
            if (q++ > 1000) break;

            LinkedListNode<int> node1 = node0.Next ?? list.First;
            LinkedListNode<int> node2 = node1.Next ?? list.First;

            Vector2 p0 = Points[node0.Value];
            Vector2 p1 = Points[node1.Value];
            Vector2 p2 = Points[node2.Value];

            Vector2 l0 = p1 - p0;
            Vector2 l2 = p2 - p0;

            Vector2 l2Left = rightInside ? new Vector2(-l2.y, l2.x) : new Vector2(l2.y, -l2.x);
            bool inside = Vector2.Dot(l2Left, l0) > 0;
            if (inside)
            {
                node0 = node1;
                continue;
            }

            if (AnyInsideTriangle(node0.Value, node1.Value, node2.Value, Points, rightInside))
            {
                node0 = node1;
                continue;
            }


            triangles.Add(node0.Value);
            triangles.Add(node1.Value);
            triangles.Add(node2.Value);
            list.Remove(node1);
            node0 = node2;
        }
        triangles.Add(list.First.Value);
        triangles.Add(list.First.Next.Value);
        triangles.Add(list.First.Next.Next.Value);
        return triangles.ToArray();
    }

    private static bool AnyInsideTriangle(int n0, int n1, int n2, Vector2[] points, bool rightInside)
    {

        Vector2 p0 = points[n0];
        Vector2 p1 = points[n1];
        Vector2 p2 = points[n2];

        Vector2 l0 = p1 - p0;
        Vector2 l1 = p2 - p1;
        Vector2 l2 = p0 - p2;

        Vector2 l0Left, l1Left, l2Left;
        if (rightInside)
        {
            l0Left = new Vector2(-l0.y, l0.x);
            l1Left = new Vector2(-l1.y, l1.x);
            l2Left = new Vector2(-l2.y, l2.x);
        }
        else
        {
            l0Left = new Vector2(l0.y, -l0.x);
            l1Left = new Vector2(l1.y, -l1.x);
            l2Left = new Vector2(l2.y, -l2.x);
        }

        for (int i = 0; i < points.Length; i++)
        {
            if (i == n1) continue;
            //Vector2 point = points[i] - p0;
            if ((Vector2.Dot(l0Left, points[i] - p0) > 0) &&
                (Vector2.Dot(l1Left, points[i] - p1) > 0) &&
                (Vector2.Dot(l2Left, points[i] - p2) > 0))
                return true;
        }
        return false;
    }

    private static bool IntersectLines(Vector2 l0A, Vector2 l0B, Vector2 l1A, Vector2 l1B)
    {
        Vector2 l1D = l1B - l1A;
        Vector2 l0D = l0B - l0A;
        float v1 = l1D.x*(l0A.y - l1A.y) - l1D.y*(l0A.x - l1A.x);
        float v2 = l1D.x*(l0B.y - l1A.y) - l1D.y*(l0B.x - l1A.x);
        float v3 = l0D.x*(l1A.y - l0A.y) - l0D.y*(l1A.x - l0A.x);
        float v4 = l0D.x*(l1B.y - l0A.y) - l0D.y*(l1B.x - l0A.x);
        return (v1*v2 < 0) && (v3*v4 < 0);
    }

    private static bool IntersectAnyLines(Vector2[] points)
    {
        for (int i = 0; i < points.Length; i++)
            for (int j = i + 1; j < points.Length; j++)
            {
                int i2 = (i + 1)%points.Length;
                int j2 = (j + 1)%points.Length;

                if (i == j2 || i2 == j) continue;

                Vector2 l0A = points[i];
                Vector2 l0B = points[i2];
                Vector2 l1A = points[j];
                Vector2 l1B = points[j2];
                if (IntersectLines(l0A, l0B, l1A, l1B)) return true;
            }
        return false;
    }

    private static bool RightInside(Vector2[] points)
    {
        float ff = 0;
        for (int i = 0; i < points.Length; i++)
        {
            int i1 = (i + 1)%points.Length;
            int i2 = (i + 2)%points.Length;

            Vector2 l2 = points[i] - points[i1];
            l2 = new Vector2(l2.y, -l2.x);
            ff += Vector2.Dot((points[i1] - points[i2]).normalized, l2.normalized);
        }

        return ff < 0;
    }

}  



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      