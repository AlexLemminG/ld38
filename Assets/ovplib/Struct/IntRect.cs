using System;
[Serializable]
public struct IntRect
{
    int m_XMin;
    int m_YMin;
    int m_Width;
    int m_Height;

    public IntVec2 center
    {
        get { return new IntVec2(this.x + this.m_Width / 2, this.y + this.m_Height / 2); }
        set
        {
            this.m_XMin = value.x - this.m_Width / 2;
            this.m_YMin = value.y - this.m_Height / 2;
        }
    }

    public int height
    {
        get { return this.m_Height; }
        set { this.m_Height = value; }
    }

    public int width
    {
        get { return this.m_Width; }
        set { this.m_Width = value; }
    }

    public int x
    {
        get { return this.m_XMin; }
        set { this.m_XMin = value; }
    }

    public int xMax
    {
        get { return this.m_Width + this.m_XMin; }
        set { this.m_Width = value - this.m_XMin; }
    }

    public int xMin
    {
        get { return this.m_XMin; }
        set
        {
            int single = this.xMax;
            this.m_XMin = value;
            this.m_Width = single - this.m_XMin;
        }
    }

    public int y
    {
        get { return this.m_YMin; }
        set { this.m_YMin = value; }
    }

    public int yMax
    {
        get { return this.m_Height + this.m_YMin; }
        set { this.m_Height = value - this.m_YMin; }
    }

    public int yMin
    {
        get { return this.m_YMin; }
        set
        {
            int single = this.yMax;
            this.m_YMin = value;
            this.m_Height = single - this.m_YMin;
        }
    }

    public IntRect(int left, int top, int width, int height)
    {
        this.m_XMin = left;
        this.m_YMin = top;
        this.m_Width = width;
        this.m_Height = height;
    }

    public IntRect(IntRect source)
    {
        this.m_XMin = source.m_XMin;
        this.m_YMin = source.m_YMin;
        this.m_Width = source.m_Width;
        this.m_Height = source.m_Height;
    }

    public bool Contains(int x, int y)
    {
        return x >= this.xMin && x < this.xMax && y >= this.yMin && y < this.yMax;
    }
    public bool Contains(IntVec2 point)
    {
        return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
    }
    public bool Intersects(IntRect rect)
    {
        return !(rect.xMin > xMax || rect.xMax < xMin || rect.yMin > yMax || rect.yMax < yMin);
    }


    public override bool Equals(object other)
    {
        if (!(other is IntRect))
        {
            return false;
        }
        IntRect rect = (IntRect) other;
        return (this.x.Equals(rect.x) && this.y.Equals(rect.y) && this.width.Equals(rect.width) && this.height.Equals(rect.height));
    }

    public override int GetHashCode()
    {
        int hashCode = this.x.GetHashCode();
        float single = this.width;
        float single1 = this.y;
        float single2 = this.height;
        return hashCode ^ single.GetHashCode() << 2 ^ single1.GetHashCode() >> 2 ^ single2.GetHashCode() >> 1;
    }

    public static IntRect MinMaxRect(int left, int top, int right, int bottom)
    {
        return new IntRect(left, top, right - left, bottom - top);
    }

    public static bool operator ==(IntRect lhs, IntRect rhs)
    {
        return (lhs.x != rhs.x || lhs.y != rhs.y || lhs.width != rhs.width ? false : lhs.height == rhs.height);
    }

    public static bool operator !=(IntRect lhs, IntRect rhs)
    {
        return (lhs.x != rhs.x || lhs.y != rhs.y || lhs.width != rhs.width ? true : lhs.height != rhs.height);
    }

    public void Set(int left, int top, int width, int height)
    {
        this.m_XMin = left;
        this.m_YMin = top;
        this.m_Width = width;
        this.m_Height = height;
    }

    public override string ToString()
    {
        return string.Format("(x:{0:F2}, y:{1:F2}, width:{2:F2}, height:{3:F2})", new object[] {this.x, this.y, this.width, this.height});
    }

    public string ToString(string format)
    {
        return string.Format("(x:{0}, y:{1}, width:{2}, height:{3})", new object[] {this.x.ToString(format), this.y.ToString(format), this.width.ToString(format), this.height.ToString(format)});
    }
}