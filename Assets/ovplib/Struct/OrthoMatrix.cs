// ReSharper disable InconsistentNaming

using UnityEngine;

public struct OrthoMatrix
{
    public double2 scale;
    public double2 position;

    public OrthoMatrix inverse
    {
        get
        {
            double2 newScale = 1d / scale;
            return new OrthoMatrix(newScale, -position * newScale);
        }
    }

    public OrthoMatrix(double2 scale, double2 position)
    {
        this.scale = scale;
        this.position = position;
    }
    public OrthoMatrix(double scaleX, double scaleY, double positionX, double positionY): 
        this(new double2(scaleX, scaleY), new double2(positionX, positionY)) {}

    public static OrthoMatrix operator *(OrthoMatrix a, OrthoMatrix b)
    {
        return new OrthoMatrix(a.scale * b.scale, (a.position * b.scale) + b.position);
    }

    public static double2 operator *(double2 a, OrthoMatrix b)
    {
        return a * b.scale + b.position;
    }

    public static OrthoMatrix Window01ToWorld(double2 size, double2 position)
    {
        return new OrthoMatrix(size, position);
    }
    public static OrthoMatrix Window11ToWorld(double2 size, double2 position)
    {
        size *= 0.5d;
        return new OrthoMatrix(size, position + size);
    }
    public Matrix4x4 ToMatrix4X4()
    {
        return new Matrix4x4 { m00 = (float)scale.x, m11 = (float)scale.y, m33 = 1f, m03 = (float)position.x, m13 = (float)position.y };
    }
    public static implicit operator Matrix4x4(OrthoMatrix om)
    {
        return om.ToMatrix4X4();
    }


    public override string ToString()
    {
        return string.Format("s:{0}, p:{1}", scale, position);
    }

    public string ToString(string format)
    {
        return string.Format("s:{0}, p:{1}", scale.ToString(format), position.ToString(format));
    }
}