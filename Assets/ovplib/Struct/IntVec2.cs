using System;
using UnityEngine;
[Serializable]
public struct IntVec2
{
    public int x;
    public int y;

    public int this[int index]
    {
        get
        {
            int num = index;
            if (num == 0)
            {
                return this.x;
            }
            if (num != 1)
            {
                throw new IndexOutOfRangeException("Invalid IntVec2 index!");
            }
            return this.y;
        }
        set
        {
            int num = index;
            if (num == 0)
            {
                this.x = value;
            }
            else
            {
                if (num != 1)
                {
                    throw new IndexOutOfRangeException("Invalid IntVec2 index!");
                }
                this.y = value;
            }
        }
    }
    public float Length
    {
        get { return Mathf.Sqrt(this.x * this.x + this.y * this.y); }
    }
    public IntVec2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public static float Distance(IntVec2 a, IntVec2 b)
    {
        return (a - b).Length;
    }
    public override bool Equals(object other)
    {
        if (!(other is IntVec2)) return false;
        IntVec2 vector2 = (IntVec2) other;
        return x.Equals(vector2.x) && y.Equals(vector2.y);
    }
    public override int GetHashCode()
    {
        return this.x.GetHashCode() ^ this.y.GetHashCode() << 2;
    }
    public static IntVec2 Max(IntVec2 lhs, IntVec2 rhs)
    {
        return new IntVec2(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y));
    }
    public static IntVec2 Min(IntVec2 lhs, IntVec2 rhs)
    {
        return new IntVec2(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y));
    }
    public static IntVec2 operator +(IntVec2 a, IntVec2 b)
    {
        return new IntVec2(a.x + b.x, a.y + b.y);
    }
    public static IntVec2 operator /(IntVec2 a, float d)
    {
        return new IntVec2((int) (a.x / d), (int) (a.y / d));
    }
    public static IntVec2 operator /(IntVec2 a, int d)
    {
        return new IntVec2(a.x / d, a.y / d);
    }
    public static bool operator ==(IntVec2 lhs, IntVec2 rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }
    public static bool operator !=(IntVec2 lhs, IntVec2 rhs)
    {
        return lhs.x != rhs.x || lhs.y != rhs.y;
    }
    public static implicit operator IntVec2(Vector2 v)
    {
        return new IntVec2((int) v.x, (int) v.y);
    }
    public static IntVec2 operator *(IntVec2 a, float d)
    {
        return new IntVec2((int) (a.x * d), (int) (a.y * d));
    }
    public static IntVec2 operator *(IntVec2 a, int d)
    {
        return new IntVec2(a.x * d, a.y * d);
    }
    public static IntVec2 operator *(float d, IntVec2 a)
    {
        return a * d;
    }
    public static IntVec2 operator *(int d, IntVec2 a)
    {
        return a * d;
    }
    public static IntVec2 operator *(IntVec2 a, IntVec2 b)
    {
        return new IntVec2(a.x * b.x, a.y * b.y);
    }
    public static IntVec2 operator -(IntVec2 a, IntVec2 b)
    {
        return new IntVec2(a.x - b.x, a.y - b.y);
    }
    public static IntVec2 operator -(IntVec2 a)
    {
        return new IntVec2(-a.x, -a.y);
    }
    public void Set(int new_x, int new_y)
    {
        this.x = new_x;
        this.y = new_y;
    }
    public override string ToString()
    {
        return string.Format("({0:F1}, {1:F1})", new object[] {this.x, this.y});
    }
    public string ToString(string format)
    {
        return string.Format("({0}, {1})", new object[] {this.x.ToString(format), this.y.ToString(format)});
    }
}
