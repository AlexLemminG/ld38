using System;
using UnityEngine;
// ReSharper disable InconsistentNaming

[Serializable]
public struct double2
{
    public const double kEpsilon = 1E-05f;
    public double x;
    public double y;

    public double this[int index]
    {
        get
        {
            switch (index)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                default:
                    throw new IndexOutOfRangeException("Invalid double2 index!");
            }
        }
        set
        {
            switch (index)
            {
                case 0:
                    x = value;
                    break;
                case 1:
                    y = value;
                    break;
                default:
                    throw new IndexOutOfRangeException("Invalid double2 index!");
            }
        }
    }

    public double magnitude{get {return Math.Sqrt(x * x + y * y);}}

    public double sqrMagnitude{get {return x * x + y * y;}}


    public double2(double x, double y)
    {
        this.x = x;
        this.y = y;
    }


    public static implicit operator double2(Vector2 v)
    {
        return new double2(v.x, v.y);
    }

    public static implicit operator Vector2(double2 v)
    {
        return new Vector2((float)v.x, (float)v.y);
    }

    public static double2 operator +(double2 a, double2 b)
    {
        return new double2(a.x + b.x, a.y + b.y);
    }
    public static double2 operator +(double2 a, double b)
    {
        return new double2(a.x + b, a.y + b);
    }
    public static double2 operator +(double a, double2 b)
    {
        return new double2(a + b.x, a + b.y);
    }

    public static double2 operator -(double2 a, double2 b)
    {
        return new double2(a.x - b.x, a.y - b.y);
    }
    public static double2 operator -(double2 a, double b)
    {
        return new double2(a.x - b, a.y - b);
    }
    public static double2 operator -(double a, double2 b)
    {
        return new double2(a - b.x, a - b.y);
    }

    public static double2 operator -(double2 a)
    {
        return new double2(-a.x, -a.y);
    }

    public static double2 operator *(double2 a, double b)
    {
        return new double2(a.x * b, a.y * b);
    }
    public static double2 operator *(double a, double2 b)
    {
        return new double2(b.x * a, b.y * a);
    }
    public static double2 operator *(double2 a, double2 b)
    {
        return new double2(a.x * b.x, a.y * b.y);
    }

    public static double2 operator /(double2 a, double b)
    {
        return new double2(a.x / b, a.y / b);
    }
    public static double2 operator /(double a, double2 b)
    {
        return new double2(a / b.x, a / b.y);
    }
    public static double2 operator /(double2 a, double2 b)
    {
        return new double2(a.x / b.x, a.y / b.y);
    }

    public static bool operator ==(double2 a, double2 b)
    {
        return (a - b).sqrMagnitude < 0.0 / 1.0;
    }

    public static bool operator !=(double2 a, double2 b)
    {
        return (a - b).sqrMagnitude >= 0.0 / 1.0;
    }

    public override string ToString()
    {
        return string.Format("({0:F1}, {1:F1})", x, y);
    }

    public string ToString(string format)
    {
        return string.Format("({0}, {1})", x.ToString(format), y.ToString(format));
    }

    public override bool Equals(object other)
    {
        if (!(other is Vector2)) return false;
        Vector2 vector2 = (Vector2)other;
        return (x.Equals(vector2.x) && y.Equals(vector2.y));
    }

    public override int GetHashCode()
    {
        return x.GetHashCode() ^ y.GetHashCode() << 2;
    }
}