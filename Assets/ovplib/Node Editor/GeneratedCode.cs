public partial class NodeData{
public const int NodeTypesCount = 23;
public AnimationCurveNode[] AnimationCurveNodes;
public BinaryOperatorNode[] BinaryOperatorNodes;
public BoolNode[] BoolNodes;
public BoolOperatorNode[] BoolOperatorNodes;
public ColorNode[] ColorNodes;
public ConditionNode[] ConditionNodes;
public DeltaNode[] DeltaNodes;
public DozerNode[] DozerNodes;
public FloatNode[] FloatNodes;
public GradientNode[] GradientNodes;
public InputNode[] InputNodes;
public LerpNode[] LerpNodes;
public OutputNode[] OutputNodes;
public RandomNode[] RandomNodes;
public SpringNode[] SpringNodes;
public TimerNode[] TimerNodes;
public UnaryOperatorNode[] UnaryOperatorNodes;
public VecCompNode[] VecCompNodes;
public VecFloatOpNode[] VecFloatOpNodes;
public VecModifyNode[] VecModifyNodes;
public Vector2Node[] Vector2Nodes;
public Vector3Node[] Vector3Nodes;
public Vector4Node[] Vector4Nodes;
public static NodeType[] NodesTypes = {
new NodeType {Type = typeof (AnimationCurveNode), TypeID = 0, Path = "Other/AnimationCurve"},
new NodeType {Type = typeof (BinaryOperatorNode), TypeID = 1, Path = "Operations/BinaryOperator"},
new NodeType {Type = typeof (BoolNode), TypeID = 2, Path = "Values/Bool"},
new NodeType {Type = typeof (BoolOperatorNode), TypeID = 3, Path = "Operations/BoolOperator"},
new NodeType {Type = typeof (ColorNode), TypeID = 4, Path = "Values/Color"},
new NodeType {Type = typeof (ConditionNode), TypeID = 5, Path = "Operations/Condition"},
new NodeType {Type = typeof (DeltaNode), TypeID = 6, Path = "Other/Delta"},
new NodeType {Type = typeof (DozerNode), TypeID = 7, Path = "Other/Dozer"},
new NodeType {Type = typeof (FloatNode), TypeID = 8, Path = "Values/Float"},
new NodeType {Type = typeof (GradientNode), TypeID = 9, Path = "Other/Gradient"},
new NodeType {Type = typeof (InputNode), TypeID = 10, Path = "Other/Input"},
new NodeType {Type = typeof (LerpNode), TypeID = 11, Path = "Operations/Lerp"},
new NodeType {Type = typeof (OutputNode), TypeID = 12, Path = "Other/Output"},
new NodeType {Type = typeof (RandomNode), TypeID = 13, Path = "Values/Random"},
new NodeType {Type = typeof (SpringNode), TypeID = 14, Path = "Other/Spring"},
new NodeType {Type = typeof (TimerNode), TypeID = 15, Path = "Other/Timer"},
new NodeType {Type = typeof (UnaryOperatorNode), TypeID = 16, Path = "Operations/UnaryOperator"},
new NodeType {Type = typeof (VecCompNode), TypeID = 17, Path = "Values/VecComp"},
new NodeType {Type = typeof (VecFloatOpNode), TypeID = 18, Path = "Operations/VecFloatOp"},
new NodeType {Type = typeof (VecModifyNode), TypeID = 19, Path = "Values/VecModify"},
new NodeType {Type = typeof (Vector2Node), TypeID = 20, Path = "Values/Vector2"},
new NodeType {Type = typeof (Vector3Node), TypeID = 21, Path = "Values/Vector3"},
new NodeType {Type = typeof (Vector4Node), TypeID = 22, Path = "Values/Vector4"},
};
public Node[] GetArray(int type){switch (type){
case 0: return AnimationCurveNodes;
case 1: return BinaryOperatorNodes;
case 2: return BoolNodes;
case 3: return BoolOperatorNodes;
case 4: return ColorNodes;
case 5: return ConditionNodes;
case 6: return DeltaNodes;
case 7: return DozerNodes;
case 8: return FloatNodes;
case 9: return GradientNodes;
case 10: return InputNodes;
case 11: return LerpNodes;
case 12: return OutputNodes;
case 13: return RandomNodes;
case 14: return SpringNodes;
case 15: return TimerNodes;
case 16: return UnaryOperatorNodes;
case 17: return VecCompNodes;
case 18: return VecFloatOpNodes;
case 19: return VecModifyNodes;
case 20: return Vector2Nodes;
case 21: return Vector3Nodes;
case 22: return Vector4Nodes;
default:return null;}}
public void SetArray(int type, Node[] nodes){switch (type){
case 0:AnimationCurveNodes = (AnimationCurveNode[])nodes;break;
case 1:BinaryOperatorNodes = (BinaryOperatorNode[])nodes;break;
case 2:BoolNodes = (BoolNode[])nodes;break;
case 3:BoolOperatorNodes = (BoolOperatorNode[])nodes;break;
case 4:ColorNodes = (ColorNode[])nodes;break;
case 5:ConditionNodes = (ConditionNode[])nodes;break;
case 6:DeltaNodes = (DeltaNode[])nodes;break;
case 7:DozerNodes = (DozerNode[])nodes;break;
case 8:FloatNodes = (FloatNode[])nodes;break;
case 9:GradientNodes = (GradientNode[])nodes;break;
case 10:InputNodes = (InputNode[])nodes;break;
case 11:LerpNodes = (LerpNode[])nodes;break;
case 12:OutputNodes = (OutputNode[])nodes;break;
case 13:RandomNodes = (RandomNode[])nodes;break;
case 14:SpringNodes = (SpringNode[])nodes;break;
case 15:TimerNodes = (TimerNode[])nodes;break;
case 16:UnaryOperatorNodes = (UnaryOperatorNode[])nodes;break;
case 17:VecCompNodes = (VecCompNode[])nodes;break;
case 18:VecFloatOpNodes = (VecFloatOpNode[])nodes;break;
case 19:VecModifyNodes = (VecModifyNode[])nodes;break;
case 20:Vector2Nodes = (Vector2Node[])nodes;break;
case 21:Vector3Nodes = (Vector3Node[])nodes;break;
case 22:Vector4Nodes = (Vector4Node[])nodes;break;
}}
public void AddElement(int type, Node node){switch (type){
case 0: AddElement(ref AnimationCurveNodes , (AnimationCurveNode)node);break;
case 1: AddElement(ref BinaryOperatorNodes , (BinaryOperatorNode)node);break;
case 2: AddElement(ref BoolNodes , (BoolNode)node);break;
case 3: AddElement(ref BoolOperatorNodes , (BoolOperatorNode)node);break;
case 4: AddElement(ref ColorNodes , (ColorNode)node);break;
case 5: AddElement(ref ConditionNodes , (ConditionNode)node);break;
case 6: AddElement(ref DeltaNodes , (DeltaNode)node);break;
case 7: AddElement(ref DozerNodes , (DozerNode)node);break;
case 8: AddElement(ref FloatNodes , (FloatNode)node);break;
case 9: AddElement(ref GradientNodes , (GradientNode)node);break;
case 10: AddElement(ref InputNodes , (InputNode)node);break;
case 11: AddElement(ref LerpNodes , (LerpNode)node);break;
case 12: AddElement(ref OutputNodes , (OutputNode)node);break;
case 13: AddElement(ref RandomNodes , (RandomNode)node);break;
case 14: AddElement(ref SpringNodes , (SpringNode)node);break;
case 15: AddElement(ref TimerNodes , (TimerNode)node);break;
case 16: AddElement(ref UnaryOperatorNodes , (UnaryOperatorNode)node);break;
case 17: AddElement(ref VecCompNodes , (VecCompNode)node);break;
case 18: AddElement(ref VecFloatOpNodes , (VecFloatOpNode)node);break;
case 19: AddElement(ref VecModifyNodes , (VecModifyNode)node);break;
case 20: AddElement(ref Vector2Nodes , (Vector2Node)node);break;
case 21: AddElement(ref Vector3Nodes , (Vector3Node)node);break;
case 22: AddElement(ref Vector4Nodes , (Vector4Node)node);break;
}}
public void DecreaseArray(int type){switch (type){
case 0: DecreaseArray(ref AnimationCurveNodes );break;
case 1: DecreaseArray(ref BinaryOperatorNodes );break;
case 2: DecreaseArray(ref BoolNodes );break;
case 3: DecreaseArray(ref BoolOperatorNodes );break;
case 4: DecreaseArray(ref ColorNodes );break;
case 5: DecreaseArray(ref ConditionNodes );break;
case 6: DecreaseArray(ref DeltaNodes );break;
case 7: DecreaseArray(ref DozerNodes );break;
case 8: DecreaseArray(ref FloatNodes );break;
case 9: DecreaseArray(ref GradientNodes );break;
case 10: DecreaseArray(ref InputNodes );break;
case 11: DecreaseArray(ref LerpNodes );break;
case 12: DecreaseArray(ref OutputNodes );break;
case 13: DecreaseArray(ref RandomNodes );break;
case 14: DecreaseArray(ref SpringNodes );break;
case 15: DecreaseArray(ref TimerNodes );break;
case 16: DecreaseArray(ref UnaryOperatorNodes );break;
case 17: DecreaseArray(ref VecCompNodes );break;
case 18: DecreaseArray(ref VecFloatOpNodes );break;
case 19: DecreaseArray(ref VecModifyNodes );break;
case 20: DecreaseArray(ref Vector2Nodes );break;
case 21: DecreaseArray(ref Vector3Nodes );break;
case 22: DecreaseArray(ref Vector4Nodes );break;
}}
public void FixNullArrays(){
if(AnimationCurveNodes == null) AnimationCurveNodes = new AnimationCurveNode[0];
if(BinaryOperatorNodes == null) BinaryOperatorNodes = new BinaryOperatorNode[0];
if(BoolNodes == null) BoolNodes = new BoolNode[0];
if(BoolOperatorNodes == null) BoolOperatorNodes = new BoolOperatorNode[0];
if(ColorNodes == null) ColorNodes = new ColorNode[0];
if(ConditionNodes == null) ConditionNodes = new ConditionNode[0];
if(DeltaNodes == null) DeltaNodes = new DeltaNode[0];
if(DozerNodes == null) DozerNodes = new DozerNode[0];
if(FloatNodes == null) FloatNodes = new FloatNode[0];
if(GradientNodes == null) GradientNodes = new GradientNode[0];
if(InputNodes == null) InputNodes = new InputNode[0];
if(LerpNodes == null) LerpNodes = new LerpNode[0];
if(OutputNodes == null) OutputNodes = new OutputNode[0];
if(RandomNodes == null) RandomNodes = new RandomNode[0];
if(SpringNodes == null) SpringNodes = new SpringNode[0];
if(TimerNodes == null) TimerNodes = new TimerNode[0];
if(UnaryOperatorNodes == null) UnaryOperatorNodes = new UnaryOperatorNode[0];
if(VecCompNodes == null) VecCompNodes = new VecCompNode[0];
if(VecFloatOpNodes == null) VecFloatOpNodes = new VecFloatOpNode[0];
if(VecModifyNodes == null) VecModifyNodes = new VecModifyNode[0];
if(Vector2Nodes == null) Vector2Nodes = new Vector2Node[0];
if(Vector3Nodes == null) Vector3Nodes = new Vector3Node[0];
if(Vector4Nodes == null) Vector4Nodes = new Vector4Node[0];
}
}