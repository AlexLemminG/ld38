﻿using System;
using UnityEngine;



public partial class NodeData
{
    [Serializable]
    public class FloatNode : Node
    {
        public float Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Float";
        }
        public FloatNode()
        {
            Name = "Float";
            InitLinks(0);
            Type = ValueType.Float;
            DrawProperties = new[] {"Value"};
            NodeWidth = 6;
            CalcNodeHeight();
            Controllable = IOMode == IOMode.None;
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Float, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].FloatOut();
            else
                processor.FloatOut = () => Value;

        }
    }
   
    [Serializable]
    public class Vector2Node : Node
    {
        public Vector2 Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Vector2";
        }
        public Vector2Node()
        {
            Name = "Vector2";
            InitLinks(0);
            Type = ValueType.Vector2;
            DrawProperties = new[] { "Value" };
            NodeWidth = 7;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Vector2, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].Vector2Out();
            else
                processor.Vector2Out = () => Value;
        }
    }


    [Serializable]
    public class Vector3Node : Node
    {
        public Vector3 Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Vector3";
        }
        public Vector3Node()
        {
            Name = "Vector3";
            InitLinks(0);
            Type = ValueType.Vector3;
            DrawProperties = new[] { "Value" };
            NodeWidth = 11;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Vector3, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].Vector3Out();
            else
                processor.Vector3Out = () => Value;
        }
    }


    [Serializable]
    public class Vector4Node : Node
    {
        public Vector4 Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Vector4";
        }
        public Vector4Node()
        {
            Name = "Vector4";
            InitLinks(0);
            Type = ValueType.Vector4;
            DrawProperties = new[] { "Value" };
            NodeWidth = 14;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Vector4, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].Vector4Out();
            else
                processor.Vector4Out = () => Value;
        }
    }

    [Serializable]
    public class ColorNode : Node
    {
        public Color Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Color";
        }
        public ColorNode()
        {
            Name = "Color";
            InitLinks(0);
            Type = ValueType.Color;
            DrawProperties = new[] { "Value" };
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Color, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].ColorOut();
            else
                processor.ColorOut = () => Value;
        }
    }

    [Serializable]
    public class BoolNode : Node
    {
        public bool Value;
        public IOMode IOMode = IOMode.None;
        public string ValueName;

        public override string GetPath()
        {
            return "Values/Bool";
        }
        public BoolNode()
        {
            Name = "Bool";
            InitLinks(0);
            Type = ValueType.Bool;
            DrawProperties = new[] { "Value" };
            NodeWidth = 6;
            CalcNodeHeight();
            Controllable = IOMode == IOMode.None;
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            if (IOMode != IOMode.None) Name = ValueName;
            IOUpdate();
            Controllable = IOMode == IOMode.None;
            NodeHeight = DefaultHeight;
            CalcNodeHeight();
            return base.ValidateOut(proposedType);
        }

        void IOUpdate()
        {
            if (IOMode == IOMode.Output)
            {
                InitLinks(1);
                Inputs[0].Initialize(ValueType.Bool, "In");
                Type = ValueType.None;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            IOUpdate();
            if (IOMode == IOMode.Output)
                processor.VoidOut = () => Value = processor.Inputs[0].BoolOut();
            else
                processor.BoolOut = () => Value;

        }
    }
   


    [Serializable]
    public class VecModifyNode : Node
    {
        static readonly Vector2 _zero2 = new Vector2();
        static Vector3 _zero3 = new Vector3();
        static Vector4 _zero4 = new Vector4();
        static Color _zeroC = new Color();

        public override string GetPath()
        {
            return "Values/VecModify";
        }
        public VecModifyNode()
        {
            Name = "VecModify";
            InitLinks(5);
            Inputs[0].Initialize(ValueType.AutoVector, "Vec", false);
            Inputs[1].Initialize(ValueType.Float, "x", false);
            Inputs[2].Initialize(ValueType.Float, "y", false);
            Inputs[3].Initialize(ValueType.Float, "z", false);
            Inputs[4].Initialize(ValueType.Float, "w", false);

            Type = ValueType.AutoVector;
            NodeWidth = 4;
            CalcNodeHeight();
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.Vector2Out = () =>
            {
                NodeProcessor inv = processor.Inputs[0];
                Vector2 v = inv != null ? inv.Vector2Out() : _zero2;
                NodeProcessor inx = processor.Inputs[1];
                NodeProcessor iny = processor.Inputs[2];
                if (inx != null) v.x = inx.FloatOut();
                if (iny != null) v.y = iny.FloatOut();
                return v;
            };
            processor.Vector3Out = () =>
            {
                NodeProcessor inv = processor.Inputs[0];
                Vector3 v = inv != null ? inv.Vector3Out() : _zero3;
                NodeProcessor inx = processor.Inputs[1];
                NodeProcessor iny = processor.Inputs[2];
                NodeProcessor inz = processor.Inputs[3];
                if (inx != null) v.x = inx.FloatOut();
                if (iny != null) v.y = iny.FloatOut();
                if (inz != null) v.z = inz.FloatOut();
                return v;
            };
            processor.Vector4Out = () =>
            {
                NodeProcessor inv = processor.Inputs[0];
                Vector4 v = inv != null ? inv.Vector4Out() : _zero4;
                NodeProcessor inx = processor.Inputs[1];
                NodeProcessor iny = processor.Inputs[2];
                NodeProcessor inz = processor.Inputs[3];
                NodeProcessor inw = processor.Inputs[4];
                if (inx != null) v.x = inx.FloatOut();
                if (iny != null) v.y = iny.FloatOut();
                if (inz != null) v.z = inz.FloatOut();
                if (inw != null) v.w = inw.FloatOut();
                return v;
            };
            processor.ColorOut = () =>
            {
                NodeProcessor inv = processor.Inputs[0];
                Color v = inv != null ? inv.ColorOut() : _zeroC;
                NodeProcessor inx = processor.Inputs[1];
                NodeProcessor iny = processor.Inputs[2];
                NodeProcessor inz = processor.Inputs[3];
                NodeProcessor inw = processor.Inputs[4];
                if (inx != null) v.r = inx.FloatOut();
                if (iny != null) v.g = iny.FloatOut();
                if (inz != null) v.b = inz.FloatOut();
                if (inw != null) v.a = inw.FloatOut();
                return v;
            };
        }
    }
    
    [Serializable]
    public class VecCompNode: Node
    {
        public VectorType VType;
        public ComponentType Component;

        public override string GetPath()
        {
            return "Values/VecComp";
        }
        public VecCompNode()
        {
            Name = "VecComp";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.AutoVector, "Vec", true);
            DrawProperties = new[] { "VType", "Component" };

            Type = ValueType.AutoVector;
            NodeWidth = 5;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            int component = (int)Component;
            Type = ValueType.Float;
            if (VType == VectorType.Vector2)
            {
                Inputs[0].Type = ValueType.Vector2;
                if (component > 1) return false;
            }
            else if (VType == VectorType.Vector3)
            {
                Inputs[0].Type = ValueType.Vector3;
                if (component > 2) return false;
            }
            else if (VType == VectorType.Vector4)
                Inputs[0].Type = ValueType.Vector4;
            else if (VType == VectorType.Color)
                Inputs[0].Type = ValueType.Color;

            
            return base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            int component = (int)Component;
            if (VType == VectorType.Vector2)
                processor.FloatOut = () => processor.Inputs[0].Vector2Out()[component];
            else if (VType == VectorType.Vector3)
                processor.FloatOut = () => processor.Inputs[0].Vector3Out()[component];
            else if (VType == VectorType.Vector4)
                processor.FloatOut = () => processor.Inputs[0].Vector4Out()[component];
            else if (VType == VectorType.Color)
                processor.FloatOut = () => processor.Inputs[0].ColorOut()[component];
        }

        public enum ComponentType
        {
            x=0,
            y=1,
            z=2,
            w=3,
        }

        public enum VectorType
        {
            Vector2,
            Vector3,
            Vector4,
            Color,
        }
    }


    [Serializable]
    public class RandomNode : Node
    {
        public RandomType RType;

        public override string GetPath()
        {
            return "Values/Random";
        }
        public RandomNode()
        {
            Name = "Random";
            InitLinks(0);
            DrawProperties = new[] { "RType", };

            Type = ValueType.AutoVector;
            NodeWidth = 5;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            Type = ValueType.Float;
            switch (RType)
            {
                case RandomType.InsideUnitCircle:
                case RandomType.OnUnitCircle:
                    Type = ValueType.Vector2;
                    break;
                case RandomType.InsideUnitSphere:
                case RandomType.OnUnitSphere:
                    Type = ValueType.Vector3;
                    break;
                case RandomType.Value01:
                case RandomType.Valuem11:
                    Type = ValueType.Float;
                    break;
            }

            return base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            if (RType == RandomType.InsideUnitCircle)
                processor.Vector2Out = () => UnityEngine.Random.insideUnitCircle;
            else if (RType == RandomType.OnUnitCircle)
                processor.Vector2Out = () => UnityEngine.Random.insideUnitCircle.normalized;
            if (RType == RandomType.InsideUnitSphere)
                processor.Vector3Out = () => UnityEngine.Random.insideUnitSphere;
            if (RType == RandomType.OnUnitSphere)
                processor.Vector3Out = () => UnityEngine.Random.onUnitSphere;
            if (RType == RandomType.Value01)
                processor.FloatOut = () => UnityEngine.Random.value;
            if (RType == RandomType.Valuem11)
                processor.FloatOut = () => UnityEngine.Random.Range(-1f, 1f);
        }

        public enum RandomType
        {
            InsideUnitCircle,
            OnUnitCircle,
            InsideUnitSphere,
            OnUnitSphere,
            Value01,
            Valuem11
        }
    }
    
    public enum IOMode
    {
        None = 0,
        Input,
        Output,
        InOut
    }
}
