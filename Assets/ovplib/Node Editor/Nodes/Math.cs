﻿using System;
using UnityEngine;

public partial class NodeData
{

    [Serializable]
    public class LerpNode : Node
    {
        public override string GetPath()
        {
            return "Operations/Lerp";
        }
        public LerpNode()
        {
            Name = "Lerp";
            InitLinks(3);
            Inputs[0].Initialize(ValueType.AutoValue, "A");
            Inputs[1].Initialize(ValueType.AutoValue, "B");
            Inputs[2].Initialize(ValueType.Float, "t");
            Type = ValueType.AutoValue;
            NodeWidth = 3;
            CalcNodeHeight();
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.FloatOut = () => Mathf.Lerp(processor.Inputs[0].FloatOut(), processor.Inputs[1].FloatOut(), processor.Inputs[2].FloatOut());
            processor.Vector2Out = () => Vector2.LerpUnclamped(processor.Inputs[0].Vector2Out(), processor.Inputs[1].Vector2Out(), processor.Inputs[2].FloatOut());
            processor.Vector3Out = () => Vector3.LerpUnclamped(processor.Inputs[0].Vector3Out(), processor.Inputs[1].Vector3Out(), processor.Inputs[2].FloatOut());
            processor.Vector4Out = () => Vector4.LerpUnclamped(processor.Inputs[0].Vector4Out(), processor.Inputs[1].Vector4Out(), processor.Inputs[2].FloatOut());
            processor.ColorOut = () => Color.LerpUnclamped(processor.Inputs[0].ColorOut(), processor.Inputs[1].ColorOut(), processor.Inputs[2].FloatOut());
            processor.IntOut = () => (int)Mathf.Lerp(processor.Inputs[0].IntOut(), processor.Inputs[1].IntOut(), processor.Inputs[2].FloatOut());
        }
    }

    [Serializable]
    public class BinaryOperatorNode: Node
    {
        public OperationType _Type;
        public override string GetPath()
        {
            return "Operations/BinaryOperator";
   
        }
        public BinaryOperatorNode()
        {
            Name = "BinaryOperator";
            InitLinks(2);
            Inputs[0].Initialize(ValueType.AutoValue, "A");
            Inputs[1].Initialize(ValueType.AutoValue, "B");
            DrawProperties = new[] {"_Type"};
            Type = ValueType.AutoValue;
            NodeWidth = 5;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            bool validate = true;
            switch (_Type)
            {
                case OperationType.Multyply:
                    break;
                case OperationType.Divide:
                    break;
                case OperationType.Add:
                    break;
                case OperationType.Subtract:
                    break;
                default:
                    validate = false;
                    break;
            }
            return validate && base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            switch (_Type)
            {
                case OperationType.Multyply:
                {
                    processor.FloatOut = () => processor.Inputs[0].FloatOut() * processor.Inputs[1].FloatOut();
                    processor.Vector2Out = () => Vector2.Scale(processor.Inputs[0].Vector2Out(), processor.Inputs[1].Vector2Out());
                    processor.Vector3Out = () => Vector3.Scale(processor.Inputs[0].Vector3Out(), processor.Inputs[1].Vector3Out());
                    processor.Vector4Out = () => Vector3.Scale(processor.Inputs[0].Vector4Out(), processor.Inputs[1].Vector4Out());
                    processor.ColorOut = () => processor.Inputs[0].ColorOut() * processor.Inputs[0].ColorOut();
                    processor.IntOut = () => processor.Inputs[0].IntOut() * processor.Inputs[0].IntOut();
                }
                    break;

                case OperationType.Divide:
                {
                    processor.FloatOut = () => processor.Inputs[0].FloatOut() / processor.Inputs[1].FloatOut();
                    processor.Vector2Out = () =>
                    {
                        Vector2 a = processor.Inputs[0].Vector2Out();
                        Vector2 b = processor.Inputs[1].Vector2Out();
                        return new Vector2(a.x / b.x, a.y / b.y);
                    };
                    processor.Vector3Out = () =>
                    {
                        Vector3 a = processor.Inputs[0].Vector3Out();
                        Vector3 b = processor.Inputs[1].Vector3Out();
                        return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
                    };
                    processor.Vector4Out = () =>
                    {
                        Vector4 a = processor.Inputs[0].Vector4Out();
                        Vector4 b = processor.Inputs[1].Vector4Out();
                        return new Vector4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
                    };
                    processor.ColorOut = () =>
                    {
                        Color a = processor.Inputs[0].ColorOut();
                        Color b = processor.Inputs[1].ColorOut();
                        return new Color(a.r / b.r, a.g / b.g, a.b / b.b, a.a / b.a);
                    };
                    processor.IntOut = () => processor.Inputs[0].IntOut() / processor.Inputs[1].IntOut();
                }
                    break;
                case OperationType.Add:
                {
                    processor.FloatOut = () => processor.Inputs[0].FloatOut() + processor.Inputs[1].FloatOut();
                    processor.Vector2Out = () => processor.Inputs[0].Vector2Out() + processor.Inputs[1].Vector2Out();
                    processor.Vector3Out = () => processor.Inputs[0].Vector3Out() + processor.Inputs[1].Vector3Out();
                    processor.Vector4Out = () => processor.Inputs[0].Vector4Out() + processor.Inputs[1].Vector4Out();
                    processor.ColorOut = () => processor.Inputs[0].ColorOut() + processor.Inputs[0].ColorOut();
                    processor.IntOut = () => processor.Inputs[0].IntOut() + processor.Inputs[0].IntOut();
                }
                    break;
                case OperationType.Subtract:
                {
                    processor.FloatOut = () => processor.Inputs[0].FloatOut() - processor.Inputs[1].FloatOut();
                    processor.Vector2Out = () => processor.Inputs[0].Vector2Out() - processor.Inputs[1].Vector2Out();
                    processor.Vector3Out = () => processor.Inputs[0].Vector3Out() - processor.Inputs[1].Vector3Out();
                    processor.Vector4Out = () => processor.Inputs[0].Vector4Out() - processor.Inputs[1].Vector4Out();
                    processor.ColorOut = () => processor.Inputs[0].ColorOut() - processor.Inputs[0].ColorOut();
                    processor.IntOut = () => processor.Inputs[0].IntOut() - processor.Inputs[0].IntOut();
                }
                    break;
            }
        }
        public enum OperationType
        {
            Multyply,
            Divide,
            Add,
            Subtract,

            Log_no,
            Pow_no,
            Repeat_no,

            PerlinNoise_no,

            Max_no,
            Min_no,

            Atan2_no,
        }
    }

    [Serializable]
    public class UnaryOperatorNode : Node
    {
        public OperationType _Type;
        public override string GetPath()
        {
            return "Operations/UnaryOperator";

        }
        public UnaryOperatorNode()
        {
            Name = "UnaryOperator";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.AutoValue, " ");
            DrawProperties = new[] { "_Type" };
            Type = ValueType.AutoValue;
            NodeWidth = 5;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            bool validate = true;
            switch (_Type)
            {
                case OperationType.Abs: break;
                default:
                    validate = false;
                    break;
            }
            return validate && base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            switch (_Type)
            {
                case OperationType.Abs:
                    {
                        processor.FloatOut = () => Mathf.Abs(processor.Inputs[0].FloatOut());
                        processor.Vector2Out = () =>
                        {
                            Vector2 v = processor.Inputs[0].Vector2Out();
                            v.x = Mathf.Abs(v.x);
                            v.y = Mathf.Abs(v.y);
                            return v;
                        };
                        processor.Vector3Out = () =>
                        {
                            Vector3 v = processor.Inputs[0].Vector3Out();
                            v.x = Mathf.Abs(v.x);
                            v.y = Mathf.Abs(v.y);
                            v.z = Mathf.Abs(v.z);
                            return v;
                        };
                        processor.Vector4Out = () =>
                        {
                            Vector4 v = processor.Inputs[0].Vector4Out();
                            v.x = Mathf.Abs(v.x);
                            v.y = Mathf.Abs(v.y);
                            v.z = Mathf.Abs(v.z);
                            v.w = Mathf.Abs(v.w);
                            return v;
                        };
                        processor.ColorOut = () =>
                        {
                            Color v = processor.Inputs[0].ColorOut();
                            v.r = Mathf.Abs(v.r);
                            v.g = Mathf.Abs(v.g);
                            v.b = Mathf.Abs(v.b);
                            v.a = Mathf.Abs(v.a);
                            return v;
                        };
                        processor.IntOut = () => Mathf.Abs(processor.Inputs[0].IntOut());
                    }
                    break;
            }
        }

        public enum OperationType
        {
            Abs,

            Log_no,
            Exp_no,
            Sqrt_no,

            Ceil_no,
            Floor_no,
            Round_no,

            Clamp01_no,
            Sign_no,

            Sin_no,
            Cos_no,
            Tan_no,

            Asin_no,
            Acos_no,
            Atan_no,
        }
    }
    
    [Serializable]
    public class VecFloatOpNode : Node
    {
        public OperationType _Type;
        public override string GetPath()
        {
            return "Operations/VecFloatOp";

        }
        public VecFloatOpNode()
        {
            Name = "VecFloatOp";
            InitLinks(2);
            Inputs[0].Initialize(ValueType.AutoVector, "Vec");
            Inputs[1].Initialize(ValueType.Float, "Float");
            DrawProperties = new[] { "_Type" };
            Type = ValueType.AutoVector;
            NodeWidth = 5;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            bool validate = true;
            switch (_Type)
            {
                case OperationType.Multyply:
                    break;
                case OperationType.Divide:
                    break;
                case OperationType.Add:
                    break;
                case OperationType.Subtract:
                    break;
                default:
                    validate = false;
                    break;
            }
            return validate && base.ValidateOut(proposedType);
        }


        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            switch (_Type)
            {
                case OperationType.Multyply:
                {
                    processor.Vector2Out = () => processor.Inputs[0].Vector2Out() * processor.Inputs[1].FloatOut();
                    processor.Vector3Out = () => processor.Inputs[0].Vector3Out() * processor.Inputs[1].FloatOut();
                    processor.Vector4Out = () => processor.Inputs[0].Vector4Out() * processor.Inputs[1].FloatOut();
                    processor.ColorOut = () => processor.Inputs[0].ColorOut() * processor.Inputs[1].FloatOut();
                }
                    break;

                case OperationType.Divide:
                {

                    processor.Vector2Out = () => processor.Inputs[0].Vector2Out() / processor.Inputs[1].FloatOut();
                    processor.Vector3Out = () => processor.Inputs[0].Vector3Out() / processor.Inputs[1].FloatOut();
                    processor.Vector4Out = () => processor.Inputs[0].Vector4Out() / processor.Inputs[1].FloatOut();
                    processor.ColorOut = () => processor.Inputs[0].ColorOut() / processor.Inputs[1].FloatOut();
                }
                    break;
                case OperationType.Add:
                {
                    processor.Vector2Out = () =>
                    {
                        Vector2 a = processor.Inputs[0].Vector2Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector2(a.x + b, a.y + b);
                    };
                    processor.Vector3Out = () =>
                    {
                        Vector3 a = processor.Inputs[0].Vector3Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector3(a.x + b, a.y + b, a.z + b);
                    };
                    processor.Vector4Out = () =>
                    {
                        Vector4 a = processor.Inputs[0].Vector4Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector4(a.x + b, a.y + b, a.z + b, a.w + b);
                    };
                    processor.ColorOut = () =>
                    {
                        Color a = processor.Inputs[0].ColorOut();
                        float b = processor.Inputs[1].FloatOut();
                        return new Color(a.r + b, a.g + b, a.b + b, a.a + b);
                    };
                }
                    break;
                case OperationType.Subtract:
                {
                    processor.Vector2Out = () =>
                    {
                        Vector2 a = processor.Inputs[0].Vector2Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector2(a.x - b, a.y - b);
                    };
                    processor.Vector3Out = () =>
                    {
                        Vector3 a = processor.Inputs[0].Vector3Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector3(a.x - b, a.y - b, a.z - b);
                    };
                    processor.Vector4Out = () =>
                    {
                        Vector4 a = processor.Inputs[0].Vector4Out();
                        float b = processor.Inputs[1].FloatOut();
                        return new Vector4(a.x - b, a.y - b, a.z - b, a.w - b);
                    };
                    processor.ColorOut = () =>
                    {
                        Color a = processor.Inputs[0].ColorOut();
                        float b = processor.Inputs[1].FloatOut();
                        return new Color(a.r - b, a.g - b, a.b - b, a.a - b);
                    };
                }
                    break;
            }
        }
        public enum OperationType
        {
            Multyply,
            Divide,
            Add,
            Subtract,

            Log_no,
            Pow_no,
            Repeat_no,

            PerlinNoise_no,

            Max_no,
            Min_no,

            Atan2_no,
        }
    }
    
    [Serializable]
    public class ConditionNode : Node
    {
        public OperationType _Type;
        public bool Invert;
        public override string GetPath()
        {
            return "Operations/Condition";
        }
        public ConditionNode()
        {
            Name = "Condition";
            InitLinks(2);
            Inputs[0].Initialize(ValueType.Float, "A");
            Inputs[1].Initialize(ValueType.Float, "B");
            DrawProperties = new[] { "_Type", "Invert" };
            Type = ValueType.Bool;
            NodeWidth = 6;
            CalcNodeHeight();
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            switch (_Type)
            {
                case OperationType.AMoreThanB:
                    processor.BoolOut = () => (processor.Inputs[0].FloatOut() > processor.Inputs[1].FloatOut()) ^ Invert;
                    break;
                case OperationType.AMoreOrEqualB:
                    processor.BoolOut = () => (processor.Inputs[0].FloatOut() >= processor.Inputs[1].FloatOut()) ^ Invert;
                    break;
                case OperationType.AEqualB:
                    processor.BoolOut = () => (processor.Inputs[0].FloatOut() == processor.Inputs[1].FloatOut()) ^ Invert;
                    break;
            }
        }
        public enum OperationType
        {
            AMoreThanB,
            AMoreOrEqualB,
            AEqualB,
        }
    }

    [Serializable]
    public class BoolOperatorNode : Node
    {
        public OperationType _Type;
        bool _lastVal;
        public override string GetPath()
        {
            return "Operations/BoolOperator";
        }
        public BoolOperatorNode()
        {
            Name = "BoolOperator";
            InitLinks(2);
            Inputs[0].Initialize(ValueType.Bool, "A");
            Inputs[1].Initialize(ValueType.Bool, "B", false);
            DrawProperties = new[] { "_Type" };
            Type = ValueType.Bool;
            NodeWidth = 5;
            CalcNodeHeight();
        }
        protected override bool ValidateOut(ValueType proposedType)
        {

            switch (_Type)
            {
                case OperationType.InvertA:
                case OperationType.TriggerA:
                    Inputs[1].ValueRequired = false;
                    break;
                default:
                    Inputs[1].ValueRequired = true;
                    break;
            }
            return base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            switch (_Type)
            {
                case OperationType.InvertA:
                    processor.BoolOut = () => !processor.Inputs[0].BoolOut();
                    break;
                case OperationType.TriggerA:
                    processor.BoolOut = () =>
                    {
                        bool val = processor.Inputs[0].BoolOut();
                        if (_lastVal == val) return false;
                        _lastVal = val;
                        return true;
                    };
                    break;
                case OperationType.Equal:
                    processor.BoolOut = () => processor.Inputs[0].BoolOut() == processor.Inputs[1].BoolOut();
                    break;
                case OperationType.Or:
                    processor.BoolOut = () => processor.Inputs[0].BoolOut() || processor.Inputs[1].BoolOut();
                    break;
                case OperationType.And:
                    processor.BoolOut = () => processor.Inputs[0].BoolOut() && processor.Inputs[1].BoolOut();
                    break;
                case OperationType.Xor:
                    processor.BoolOut = () => processor.Inputs[0].BoolOut() ^ processor.Inputs[1].BoolOut();
                    break;
            }
        }
        public enum OperationType
        {
            InvertA,
            TriggerA,
            Equal,
            Or,
            And,
            Xor
        }
    }

    
}