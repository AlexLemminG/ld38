﻿using System;
using UnityEngine;


public partial class NodeData
{
    [Serializable]
    public class AnimationCurveNode: Node
    {
        public AnimationCurve Curve = AnimationCurve.Linear(0, 0, 1, 1);
        public override string GetPath()
        {
            return "Other/AnimationCurve";
        }
        public AnimationCurveNode()
        {
            Name = "AnimationCurve";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.Float, "In");
            Type = ValueType.Float;
            DrawProperties = new[] { "Curve"};

            CalcNodeHeight();
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.FloatOut = () => Curve.Evaluate(processor.Inputs[0].FloatOut());
        }
    }

    [Serializable]
    public class GradientNode : Node
    {
        public Gradient Gradient = new Gradient();
        public override string GetPath()
        {
            return "Other/Gradient";
        }
        public GradientNode()
        {
            Name = "Gradient";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.Float, "In");
            Type = ValueType.Color;
            DrawProperties = new[] {"Gradient" };

            CalcNodeHeight();
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.ColorOut = () => Gradient.Evaluate(processor.Inputs[0].FloatOut());
        }
    }

    [Serializable]
    public class InputNode: Node
    {
        public Component Component;
        public string Address;
        ReflectedValue _reflectedValue = new ReflectedValue();


        public override string GetPath()
        {
            return "Other/Input";
        }
        public InputNode()
        {
            Name = "Input";
            InitLinks(0);
            Type = ValueType.Float;
            DrawProperties = new[] {"Component", "Address"};
            NodeWidth = 9;
            CalcNodeHeight();
        }

        ValueType GetMember()
        {
            bool found = _reflectedValue.FindMember(Component ? Component : null, Address);
            ValueType valueType;
            if (found && _reflectedValue.ValidateGet())
                valueType = GetValueType(_reflectedValue.Type);
            else
                valueType = ValueType.Error;
            return valueType;
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            CashedType = Type = GetMember();
            return base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            //CashedType = Type = GetMember();

            if (CashedType != ValueType.Error)
            {
                if (CashedType == ValueType.Float)
                    processor.FloatOut = _reflectedValue.CompileGetMethod<float>();
                if (CashedType == ValueType.Vector2)
                    processor.Vector2Out = _reflectedValue.CompileGetMethod<Vector2>();
                if (CashedType == ValueType.Vector3)
                    processor.Vector3Out = _reflectedValue.CompileGetMethod<Vector3>();
                if (CashedType == ValueType.Vector4)
                    processor.Vector4Out = _reflectedValue.CompileGetMethod<Vector4>();
                if (CashedType == ValueType.Color)
                    processor.ColorOut = _reflectedValue.CompileGetMethod<Color>();
                if (CashedType == ValueType.Int)
                    processor.IntOut = _reflectedValue.CompileGetMethod<int>();
                if (CashedType == ValueType.Bool)
                    processor.BoolOut = _reflectedValue.CompileGetMethod<bool>();
                if (CashedType == ValueType.Texture2D)
                    processor.Texture2DOut = _reflectedValue.CompileGetMethod<Texture2D>();
                if (CashedType == ValueType.RenderTexture)
                    processor.RenderTextureOut = _reflectedValue.CompileGetMethod<RenderTexture>();
            }
        }
    }
    
    [Serializable]
    public class OutputNode: Node
    {
        public Component Component;
        public string Address;
        ReflectedValue _reflectedValue = new ReflectedValue();

        public override string GetPath()
        {
            return "Other/Output";
        }
        public OutputNode()
        {
            Name = "Output";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.None, "In", true);
            DrawProperties = new[] {"Component", "Address"};
            Type = ValueType.None;
            NodeWidth = 9;
            CalcNodeHeight();
        }

        ValueType GetMember()
        {
            bool found = _reflectedValue.FindMember(Component ? Component : null, Address);
            ValueType valueType;
            if (found && _reflectedValue.ValidateSet())
                valueType = GetValueType(_reflectedValue.Type);
            else
                valueType = ValueType.Error;
            return valueType;
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            ValueType valueType = GetMember();
            Inputs[0].Type = valueType;
            return base.ValidateOut(proposedType);
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            //ValueType valueType = GetMember();
            //Inputs[0].Type = valueType;
            ValueType valueType = Inputs[0].Type;
            if (valueType != ValueType.Error)
            {
                if (valueType == ValueType.Float)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<float>()(processor.Inputs[0].FloatOut());
                if (valueType == ValueType.Vector2)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<Vector2>()(processor.Inputs[0].Vector2Out());
                if (valueType == ValueType.Vector3)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<Vector3>()(processor.Inputs[0].Vector3Out());
                if (valueType == ValueType.Vector4)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<Vector4>()(processor.Inputs[0].Vector4Out());
                if (valueType == ValueType.Color)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<Color>()(processor.Inputs[0].ColorOut());
                if (valueType == ValueType.Int)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<int>()(processor.Inputs[0].IntOut());
                if (valueType == ValueType.Bool)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<bool>()(processor.Inputs[0].BoolOut());
                if (valueType == ValueType.Texture2D)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<Texture2D>()(processor.Inputs[0].Texture2DOut());
                if (valueType == ValueType.RenderTexture)
                    processor.VoidOut = () => _reflectedValue.CompileSetMethod<RenderTexture>()(processor.Inputs[0].RenderTextureOut());
            }
        }

    }

    [Serializable]
    public class SpringNode: Node
    {
        public float Hardness = 0.1f;
        public float Damping = 0.5f;

        float _damping;

        float _velocity;
        Vector2 _velocity2;
        Vector3 _velocity3;
        Vector4 _velocity4;

        float _current;
        Vector2 _current2;
        Vector3 _current3;
        Vector4 _current4;

        public override string GetPath()
        {
            return "Other/Spring";
        }
        public SpringNode()
        {
            Name = "Spring";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.AutoValue, "Acceleration");
            Type = ValueType.AutoValue;
            DrawProperties = new[] { "Hardness", "Damping" };
            NodeWidth = 7;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            switch (proposedType)
            {
                case ValueType.AutoValue:
                    return true;
                case ValueType.AutoVector:
                    return true;
                case ValueType.Float:
                    return true;
                case ValueType.Vector2:
                    return true;
                case ValueType.Vector3:
                    return true;
                case ValueType.Vector4:
                    return true;
                default:
                    return false;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            Damping = Math.Max(Damping, float.Epsilon);
            _damping = 1f / (1f + Mathf.Pow(Damping, 5));
            processor.FloatOut = () =>
            {
                _velocity += processor.Inputs[0].FloatOut();
                _velocity -= _current * Hardness;
                _velocity *= Mathf.Pow(_damping, Parent.Dt);
                _current += _velocity;
                return _current;
            };
            processor.Vector2Out = () =>
            {
                _velocity2 += processor.Inputs[0].Vector2Out();
                _velocity2 -= _current2 * Hardness;
                _velocity2 *= Mathf.Pow(_damping, Parent.Dt);
                _current2 += _velocity2;
                return _current2;
            };
            processor.Vector3Out = () =>
            {
                _velocity3 += processor.Inputs[0].Vector3Out();
                _velocity3 -= _current3 * Hardness;
                _velocity3 *= Mathf.Pow(_damping, Parent.Dt);
                _current3 += _velocity3;
                return _current3;
            };
            processor.Vector4Out = () =>
            {
                _velocity4 += processor.Inputs[0].Vector4Out();
                _velocity4 -= _current4 * Hardness;
                _velocity4 *= Mathf.Pow(_damping, Parent.Dt);
                _current4 += _velocity4;
                return _current4;
            };
        }
    }
    
    [Serializable]
    public class DeltaNode : Node
    {
        float _lastVal;
        Vector2 _lastVal2;
        Vector3 _lastVal3;
        Vector4 _lastVal4;

        public override string GetPath()
        {
            return "Other/Delta";
        }
        public DeltaNode()
        {
            Name = "Delta";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.AutoValue, "Val");
            Type = ValueType.AutoValue;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            switch (proposedType)
            {
                case ValueType.AutoValue:
                    return true;
                case ValueType.AutoVector:
                    return true;
                case ValueType.Float:
                    return true;
                case ValueType.Vector2:
                    return true;
                case ValueType.Vector3:
                    return true;
                case ValueType.Vector4:
                    return true;
                default:
                    return false;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.FloatOut = () =>
            {
                float val = processor.Inputs[0].FloatOut();
                float delta = val - _lastVal;
                _lastVal = val;
                return delta;
            };
            processor.Vector2Out = () =>
            {
                Vector2 val2 = processor.Inputs[0].Vector2Out();
                Vector2 delta2 = val2 - _lastVal2;
                _lastVal2 = val2;
                return delta2;
            };
            processor.Vector3Out = () =>
            {
                Vector3 val3 = processor.Inputs[0].Vector3Out();
                Vector3 delta3 = val3 - _lastVal3;
                _lastVal3 = val3;
                return delta3;
            };
            processor.Vector4Out = () =>
            {
                Vector4 val4 = processor.Inputs[0].Vector4Out();
                Vector4 delta4 = val4 - _lastVal4;
                _lastVal4 = val4;
                return delta4;
            };
        }
    }


    [Serializable]
    public class DozerNode : Node
    {
        public bool Freeze;
        float _lastVal;
        Vector2 _lastVal2;
        Vector3 _lastVal3;
        Vector4 _lastVal4;

        public override string GetPath()
        {
            return "Other/Dozer";
        }
        public DozerNode()
        {
            Name = "Dozer";
            InitLinks(2);
            Inputs[0].Initialize(ValueType.AutoValue, "Val");
            Inputs[1].Initialize(ValueType.Bool, "Update");
            DrawProperties = new[] { "Freeze" };
            Type = ValueType.AutoValue;
            CalcNodeHeight();
        }

        protected override bool ValidateOut(ValueType proposedType)
        {
            switch (proposedType)
            {
                case ValueType.AutoValue:
                    return true;
                case ValueType.AutoVector:
                    return true;
                case ValueType.Float:
                    return true;
                case ValueType.Vector2:
                    return true;
                case ValueType.Vector3:
                    return true;
                case ValueType.Vector4:
                    return true;
                default:
                    return false;
            }
        }

        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.FloatOut = () =>
            {
                if (processor.Inputs[1].BoolOut())
                    _lastVal = processor.Inputs[0].FloatOut();
                else if (!Freeze)
                    _lastVal = 0;
                return _lastVal;

            };
            processor.Vector2Out = () =>
            {
                if (processor.Inputs[1].BoolOut())
                    _lastVal2 = processor.Inputs[0].Vector2Out();
                else if (!Freeze)
                    _lastVal2 = new Vector2();
                return _lastVal2;
            };
            processor.Vector3Out = () =>
            {
                if (processor.Inputs[1].BoolOut())
                    _lastVal3 = processor.Inputs[0].Vector3Out();
                else if (!Freeze)
                    _lastVal3 = new Vector3();
                return _lastVal3;
            };
            processor.Vector4Out = () =>
            {
                if (processor.Inputs[1].BoolOut())
                    _lastVal4 = processor.Inputs[0].Vector4Out();
                else if (!Freeze)
                    _lastVal4 = new Vector4();
                return _lastVal4;
            };
        }
    }



    [Serializable]
    public class TimerNode : Node
    {
        public LimitType LType;
        public float TimeSpeed = 1f;
        public float Limit = 1f;
        public bool Inverse;
        public bool To01;

        float _time;

        public override string GetPath()
        {
            return "Other/Timer";
        }
        public TimerNode()
        {
            Name = "Timer";
            InitLinks(1);
            Inputs[0].Initialize(ValueType.Bool, "Reset");
            DrawProperties = new[] { "LType", "TimeSpeed", "Limit", "Inverse", "To01" };
            Type = ValueType.Float;
            CalcNodeHeight();
        }



        public override void InitializeNodeProcessor(NodeProcessor processor)
        {
            processor.FloatOut = () =>
            {
                _time += Parent.Dt * TimeSpeed;
                if (processor.Inputs[0].BoolOut())
                    _time = 0f;

                float val = _time;
                if (_time > Limit)
                {
                    switch (LType)
                    {
                        case LimitType.Repeat:
                            val = Mathf.Repeat(_time, Limit);
                            break;
                        case LimitType.StayToStart:
                            val = 0f;
                            break;
                        case LimitType.StayToEnd:
                            val = Limit;
                            break;
                        case LimitType.PingPong:
                            val = Mathf.PingPong(_time, Limit);
                            break;
                        case LimitType.PingPongOnce:
                            val = Mathf.Max(0, Limit - Mathf.Abs(_time - Limit));
                            break;
                        default:
                            val = _time;
                            break;
                    }
                }

                if (Inverse) val = Limit - val;
                if (To01) val /= Limit;
                return val;
            };
        }


        public enum LimitType
        {
            None,
            Repeat,
            StayToStart,
            StayToEnd,
            PingPong,
            PingPongOnce,
        }

    }




}