using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

[Serializable]
public partial class NodeData
{
    public Link[] Outputs;
    NodeProcessor[] _processors;
    [NonSerialized]
    public float Dt;
    float _lastTime;

    public static HashSet<ValueType> AutoValues = new HashSet<ValueType>
    {
        ValueType.AutoValue,
        ValueType.AutoVector,
        ValueType.Float,
        ValueType.Vector2,
        ValueType.Vector3,
        ValueType.Vector4,
        ValueType.Color,
        ValueType.Int
    };
    public static HashSet<ValueType> AutoVectors = new HashSet<ValueType>
    {
        ValueType.AutoVector,
        ValueType.Vector2,
        ValueType.Vector3,
        ValueType.Vector4,
        ValueType.Color
    };

    #region Initialize

    public NodeData()
    {
        Outputs = new Link[0];
        FixNullArrays();
    }

    public void Initialize()
    {
        InitializeNodesIDs();
        //Debug.Log(GenerateCode());
    }

    void InitializeNodesIDs()
    {
        for (int t = 0; t < NodeTypesCount; t++)
        {
            Node[] nodes = GetArray(t);
            if (nodes == null) continue;
            for (int id = 0; id < nodes.Length; id++)
            {
                Node node = nodes[id];
                node.NodeType = t;
                node.NodeId = id;
            }
        }
    }
    #endregion


    #region Public
    public void Prepare()
    {
        _processors = new NodeProcessor[Outputs.Length];
        for (int i = 0; i < Outputs.Length; i++)
            _processors[i] = GetNodeProcessor(GetNode(Outputs[i]));
    }

    public void Process()
    {
        float time = Time.time;
        Dt = time - _lastTime;
        _lastTime = time;

        if (_processors == null) return;
        for (int i = 0; i < _processors.Length; i++)
            _processors[i].VoidOut();
    }



    public Node CteateInput(ValueType valueType, int id, string name)
    {
        return CteateIO(valueType, id, name, IOMode.Input);
    }

    public Node CteateOutput(ValueType valueType, int id, string name)
    {
        return CteateIO(valueType, id, name, IOMode.Output);
    }


    Node CteateIO(ValueType valueType, int id, string name, IOMode mode)
    {
        Node[] nodes;
        switch (valueType)
        {
            case ValueType.Float:
                if (FloatNodes.Length == id)
                    AddElement(ref FloatNodes, new FloatNode {IOMode = mode});
                if (FloatNodes.Length < id)
                    Debug.LogError("Wrong ID");
                FloatNodes[id].ValueName = name;
                return FloatNodes[id];
            case ValueType.Vector2:
                if (Vector2Nodes.Length == id)
                    AddElement(ref Vector2Nodes, new Vector2Node {IOMode = mode});
                if (Vector2Nodes.Length < id)
                    Debug.LogError("Wrong ID");
                Vector2Nodes[id].ValueName = name;
                return Vector2Nodes[id];
            case ValueType.Vector3:
                if (Vector3Nodes.Length == id)
                    AddElement(ref Vector3Nodes, new Vector3Node {IOMode = mode});
                if (Vector3Nodes.Length < id)
                    Debug.LogError("Wrong ID");
                Vector3Nodes[id].ValueName = name;
                return Vector3Nodes[id];
            case ValueType.Vector4:
                if (Vector4Nodes.Length == id)
                    AddElement(ref Vector4Nodes, new Vector4Node {IOMode = mode});
                if (Vector4Nodes.Length < id)
                    Debug.LogError("Wrong ID");
                Vector4Nodes[id].ValueName = name;
                return Vector4Nodes[id];
            case ValueType.Color:
                if (ColorNodes.Length == id)
                    AddElement(ref ColorNodes, new ColorNode {IOMode = mode});
                if (ColorNodes.Length < id)
                    Debug.LogError("Wrong ID");
                ColorNodes[id].ValueName = name;
                return ColorNodes[id];
            default:
                Debug.LogError("Wrong ValueType");
                return null;
        }
    }
    #endregion


    public List<Node> GetAllNodes()
    {
        List<Node> nodeList = new List<Node>();
        for (int t = 0; t < NodeTypesCount; t++)
            foreach (Node node in GetArray(t))
            {
                if (node == null) continue;
                nodeList.Add(node);
            }
        return nodeList;
    }

    public HashSet<Node> GetDeadEnds(List<Node> nodes)
    {
        HashSet<Node> deadEnds = new HashSet<Node>(nodes);
        foreach (Node node in nodes)
            foreach (Link link in node.Inputs)
            {
                Node inNode = GetNode(link);
                if (inNode == null) continue;
                deadEnds.Remove(inNode);
            }
        return deadEnds;
    }

    static void AddElement<T>(ref T[] array, T node) where T: Node
    {
        T[] newArray = new T[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[array.Length] = node;
        array = newArray;
    }
    static void DecreaseArray<T>(ref T[] array) where T: Node
    {
        T[] newArray = new T[array.Length - 1];
        for (int i = 0; i < newArray.Length; i++)
            newArray[i] = array[i];
        array = newArray;
    }

    void FixLinks(int tOld, int iOld, int tNew, int iNew)
    {
        for (int t = 0; t < NodeTypesCount; t++)
            foreach (Node node in GetArray(t))
            {
                if (node == null) continue;
                foreach (Link link in node.Inputs)
                {
                    if (link.NodeType == tOld && link.NodeId == iOld)
                    {
                        link.NodeType = tNew;
                        link.NodeId = iNew;
                    }
                }
            }
    }

    public NodeProcessor GetNodeProcessor(Node node)
    {
        if (node == null) return null;
        if (node.CashedType == ValueType.Error) return null;

        if (node.Processor == null)
        {
            node.Parent = this;
            node.Processor = new NodeProcessor();
            node.Processor.Inputs = new NodeProcessor[node.Inputs.Length];
            node.InitializeNodeProcessor(node.Processor);
        }

        node.Processor.OutputCount++;

        for (int i = 0; i < node.Inputs.Length; i++)
        {
            node.Processor.Inputs[i] = GetNodeProcessor(GetNode(node.Inputs[i]));
            if (node.Inputs[i].ValueRequired && node.Processor.Inputs[i] == null) return null;
        }

        if (node.Processor.OutputCount == 2) node.Processor.SeveralOutputsOptimisation();
        return node.Processor;

    }

    public bool ValidateNodes(Node node)
    {
        if (node == null) return false;
        if (node.CashedType == ValueType.Error)
            return false;

        for (int i = 0; i < node.Inputs.Length; i++)
        {
            if (node.Inputs[i].Type == ValueType.Error)
                return false;
            Node inNode = GetNode(node.Inputs[i]);
            if (node.Inputs[i].ValueRequired &&
                (inNode == null || inNode.CashedType == ValueType.Error)) return false;
        }

        return true;
    }

    public Node GetNode(Link link)
    {
        if (link.NodeType < 0 || link.NodeId < 0 || link.NodeType >= NodeTypesCount) return null;
        Node[] nodes = GetArray(link.NodeType);
        if (nodes == null || nodes.Length == 0 || link.NodeId >= nodes.Length) return null;
        return nodes[link.NodeId];
    }
    public void SetNode(int type, int id, Node node)
    {
        Node[] array = GetArray(type);
        if (array == null || array.Length == 0 || id >= array.Length) return;
        array[id] = node;
    }

    public Node CreateNode(int type)
    {
        //Debug.Log("CreateNode:" + type);
        Node[] nodes = GetArray(type);
        Type typet = NodesTypes[type].Type;

        Node node = (Node)typet.GetConstructor(new Type[0]).Invoke(new object[0]);

        node.NodeType = type;
        for (int i = 0; i < nodes.Length; i++)
        {
            if (nodes[i] != null) continue;
            nodes[i] = node;
            node.NodeId = i;
            break;
        }
        if (node.NodeId < 0)
        {
            node.NodeId = nodes.Length;
            AddElement(type, node);
        }
        return node;
    }


    void RemoveNode(int type, int id)
    {
        Node[] nodes = GetArray(type);
        if (nodes == null) return;
        int lastNode = nodes.Length - 1;
        if (nodes.Length > 1 && id != lastNode)
        {
            nodes[id] = nodes[lastNode];
            FixLinks(type, id, -1, -1);
            FixLinks(type, lastNode, type, id);
            DecreaseArray(type);
            InitializeNodesIDs();
        }
        else if (nodes.Length > 0)
        {
            FixLinks(type, id, -1, -1);
            DecreaseArray(type);
        }
    }
    public void RemoveNode(Node node)
    {
        RemoveNode(node.NodeType, node.NodeId);
    }

    public static bool ValidateTypes(ValueType ourType, ValueType proposedType)
    {
        if (ourType == ValueType.AutoValue)
            return AutoValues.Contains(proposedType);
        if (ourType == ValueType.AutoVector)
            return AutoVectors.Contains(proposedType);
        return ourType == proposedType;
    }

    public static ValueType GetMostAccurateType(ValueType a, ValueType b)
    {
        return GetTypeAccuracy(a) > GetTypeAccuracy(b) ? a : b;
    }
    static int GetTypeAccuracy(ValueType type)
    {
        switch (type)
        {
            case ValueType.AutoValue:
                return 0;
            case ValueType.AutoVector:
                return 1;
            default:
                return 2;
        }
    }

    public static ValueType GetValueType(System.Type type)
    {
        if (type == typeof (float))
            return ValueType.Float;
        if (type == typeof (Vector2))
            return ValueType.Vector2;
        if (type == typeof (Vector3))
            return ValueType.Vector3;
        if (type == typeof (Vector4))
            return ValueType.Vector4;
        if (type == typeof (Color))
            return ValueType.Color;
        if (type == typeof (int))
            return ValueType.Int;
        if (type == typeof (bool))
            return ValueType.Bool;
        if (type == typeof (Texture2D))
            return ValueType.Texture2D;
        if (type == typeof (RenderTexture))
            return ValueType.RenderTexture;
        return ValueType.Error;
    }


    #region Classes
    public abstract class Node
    {
        public Link[] Inputs;
        [NonSerialized]
        public ValueType Type;
        [NonSerialized]
        public ValueType CashedType;
        [NonSerialized]
        public NodeProcessor Processor;
        [NonSerialized]
        public NodeData Parent;

        [NonSerialized]
        public string Name;
        public Vector2 Position;
        [NonSerialized]
        public string[] DrawProperties;
        [NonSerialized]
        public int NodeWidth = 7;
        [NonSerialized]
        public int NodeHeight = DefaultHeight;
        public const int DefaultHeight = 2;

        static int _windowCounter = 1;
        [NonSerialized]
        public int WindowID = _windowCounter++;
        [NonSerialized]
        public int NodeType = -1;
        [NonSerialized]
        public int NodeId = -1;
        [NonSerialized]
        public bool Controllable = true;




        protected void InitLinks(int count)
        {
            if (Inputs != null && Inputs.Length >= count) return;
            Inputs = new Link[count];
            for (int i = 0; i < count; i++)
                Inputs[i] = new Link();
        }

        protected void CalcNodeHeight()
        {
            if (DrawProperties != null)
                NodeHeight += DrawProperties.Length;
            NodeHeight += Inputs.Length;
        }

        protected virtual bool ValidateOut(ValueType proposedType)
        {
            return ValidateTypes(Type, proposedType) || ValidateTypes(proposedType, Type);
        }

        public void UpdateTypes(ValueType type, NodeData nodeData,HashSet<Node> loopProtection )
        {
            CashedType = ValidateOut(type) ? GetMostAccurateType(type, Type) : ValueType.Error;
            if (loopProtection.Contains(this))
            {
                CashedType = ValueType.Error;
                return;
            }
            loopProtection.Add(this);

            foreach (Link input in Inputs)
            {
                Node inNode = nodeData.GetNode(input);
                if (inNode == null)
                {
                    if (input.ValueRequired) CashedType = ValueType.Error;
                    continue;
                }
                if (input.Type == ValueType.AutoValue || input.Type == ValueType.AutoVector)
                    inNode.UpdateTypes(type, nodeData, new HashSet<Node>(loopProtection));
                else
                    inNode.UpdateTypes(input.Type, nodeData, new HashSet<Node>(loopProtection));
            }
        }

        public virtual void InitializeNodeProcessor(NodeProcessor processor) {}

        public abstract string GetPath();
    }

    [Serializable]
    public class Link
    {
        public int NodeType = -1;
        public int NodeId = -1;

        [NonSerialized]
        public ValueType Type;
        [NonSerialized]
        public string Name;
        [NonSerialized]
        public bool ValueRequired;

        public void Initialize(ValueType type, string name, bool valueRequired = true)
        {
            Type = type;
            Name = name;
            ValueRequired = valueRequired;
        }
    }

    public class NodeProcessor
    {
        public NodeProcessor[] Inputs;
        public int OutputCount;

        public Func<float> FloatOut;
        Func<float> _rawFloatOut;
        float _cachedFloat;

        public Func<Vector2> Vector2Out;
        Func<Vector2> _rawVector2Out;
        Vector2 _cachedVector2;

        public Func<Vector3> Vector3Out;
        Func<Vector3> _rawVector3Out;
        Vector3 _cachedVector3;

        public Func<Vector4> Vector4Out;
        Func<Vector4> _rawVector4Out;
        Vector4 _cachedVector4;

        public Func<Color> ColorOut;
        Func<Color> _rawColorOut;
        Color _cachedColor;

        public Func<int> IntOut;
        Func<int> _rawIntOut;
        int _cachedInt;

        public Func<bool> BoolOut;
        Func<bool> _rawBoolOut;
        bool _cachedBool;

        public Func<Texture2D> Texture2DOut;
        Func<Texture2D> _rawTexture2DOut;
        Texture2D _cachedTexture2D;

        public Func<RenderTexture> RenderTextureOut;
        Func<RenderTexture> _rawRenderTextureOut;
        RenderTexture _cachedRenderTexture;

        public Action VoidOut;

        public static int CurrentFrame;
        int _lastNodeFrame;

        public void SeveralOutputsOptimisation()
        {
            //Float
            _rawFloatOut = FloatOut;
            FloatOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedFloat;
                CurrentFrame = _lastNodeFrame;
                return _cachedFloat = _rawFloatOut();
            };

            //Vector2
            _rawVector2Out = Vector2Out;
            Vector2Out = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedVector2;
                CurrentFrame = _lastNodeFrame;
                return _cachedVector2 = _rawVector2Out();
            };

            //Vector3
            _rawVector3Out = Vector3Out;
            Vector3Out = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedVector3;
                CurrentFrame = _lastNodeFrame;
                return _cachedVector3 = _rawVector3Out();
            };

            //Vector4
            _rawVector4Out = Vector4Out;
            Vector4Out = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedVector4;
                CurrentFrame = _lastNodeFrame;
                return _cachedVector4 = _rawVector4Out();
            };

            //Color
            _rawColorOut = ColorOut;
            ColorOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedColor;
                CurrentFrame = _lastNodeFrame;
                return _cachedColor = _rawColorOut();
            };

            //Int
            _rawIntOut = IntOut;
            IntOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedInt;
                CurrentFrame = _lastNodeFrame;
                return _cachedInt = _rawIntOut();
            };

            //Bool
            _rawBoolOut = BoolOut;
            BoolOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedBool;
                CurrentFrame = _lastNodeFrame;
                return _cachedBool = _rawBoolOut();
            };

            //Texture2D
            _rawTexture2DOut = Texture2DOut;
            Texture2DOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedTexture2D;
                CurrentFrame = _lastNodeFrame;
                return _cachedTexture2D = _rawTexture2DOut();
            };

            //RenderTexture
            _rawRenderTextureOut = RenderTextureOut;
            RenderTextureOut = delegate
            {
                if (CurrentFrame != _lastNodeFrame) return _cachedRenderTexture;
                CurrentFrame = _lastNodeFrame;
                return _cachedRenderTexture = _rawRenderTextureOut();
            };
        }
    }

    public enum ValueType
    {
        None,
        Error,
        AutoValue,
        AutoVector,
        Float,
        Vector2,
        Vector3,
        Vector4,
        Color,
        Int,
        Bool,
        FloatMap2D,
        Texture2D,
        RenderTexture,
        Polygon,
        Mesh
    }

    public class NodeType
    {
        public Type Type;
        public int TypeID;
        public string Path;
    }
    #endregion
}