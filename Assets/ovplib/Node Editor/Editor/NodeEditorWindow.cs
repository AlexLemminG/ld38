﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using EditorUtility = UnityEditor.EditorUtility;
using Object = UnityEngine.Object;

public partial class NodeEditorWindow: EditorWindow
{
    const float CellSize = 16f;

    GenericMenu _newNodeMenu;
    GenericMenu _rightClickMenu;
    Vector2 _scrollSide;
    Vector2 _scrollMain;
    Vector2 _mainViewSize = new Vector2(255, 255);
    Vector2 _rightClickMenuPosition;
    NodeData _nodeData;
    Draging _draging = new Draging();
    LinkDraging _linkDraging;
    GraphDraging _graphDraging = new GraphDraging();
    SelectionProcessor _selectionProcessor;
    SquareSelection _squareSelection = new SquareSelection();

    int _activeWindowID;

    List<NodeData.Node> _removeNodes = new List<NodeData.Node>();
    List<NodeData.Node> _nodeList;
    HashSet<NodeData.Node> _deadEnds;


    [MenuItem("Window/Node Editor")]
    static void Init()
    {
        GetWindow<NodeEditorWindow>("Node Editor").Show();
    }

    public void Initialize(NodeData nodeData)
    {
        OnNewDataSelected(nodeData);

        _newNodeMenu = new GenericMenu();
        for (int i = 0; i < NodeData.NodesTypes.Length; i++)
        {
            int i2 = i;
            _newNodeMenu.AddItem(new GUIContent(NodeData.NodesTypes[i].Path), false, data =>
            {
                _nodeData.CreateNode((int)data);
                RebuildGraph();
            }, i2);
        }

        _rightClickMenu = new GenericMenu();
        for (int i = 0; i < NodeData.NodesTypes.Length; i++)
        {
            int i2 = i;
            _rightClickMenu.AddItem(new GUIContent("Create/" + NodeData.NodesTypes[i].Path), false, data =>
            {
                NodeData.Node node = _nodeData.CreateNode((int)data);
                node.Position = _rightClickMenuPosition;
                RebuildGraph();
            }, i2);
        }

        Show();
    }

    public void OnNewDataSelected(NodeData nodeData)
    {
        _nodeData = nodeData;
        if (_nodeData == null) return;
        _nodeData.FixNullArrays();
        _nodeData.Initialize();
        RebuildGraph();
        Repaint();
    }

    void OnEnable()
    {
        EditorApplication.hierarchyWindowChanged += OnHierarchyWindowChanged;

        _linkDraging = new LinkDraging(this);
        _selectionProcessor = new SelectionProcessor(OnNewDataSelected);
    }

    void OnDisable()
    {
        EditorApplication.hierarchyWindowChanged = null;

        _selectionProcessor.Reset();
    }
    void OnHierarchyWindowChanged()
    {
        _selectionProcessor.Reset();
        Repaint();
    }




    void OnGUI()
    {
        _selectionProcessor.Process();
        if (!_selectionProcessor.Host) _nodeData = null;

        Styles.Initialize();
        if (_nodeData == null)
        {
            DrawBigMessage("No Selected Data");
            return;
        }
        if (_newNodeMenu == null) Initialize(_nodeData);

        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
        if (GUILayout.Button("Create Node", EditorStyles.toolbarDropDown))
            _newNodeMenu.DropDown(new Rect(1, EditorStyles.toolbar.fixedHeight - 2, 1, 1));

        _selectionProcessor.DropDown();
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();
        DrawMainViewGUI();
        EditorGUILayout.EndHorizontal();

        if (_removeNodes.Count > 0)
        {
            foreach (NodeData.Node node in _removeNodes)
                _nodeData.RemoveNode(node);
            _removeNodes.Clear();
            Repaint();
        }


    }


    void DrawMainViewGUI()
    {
        //_scrollMain = EditorGUILayout.BeginScrollView(_scrollMain, false, false, GUIStyles.labelLeft, GUIStyles.labelLeft, GUIStyles.background);

        Rect rect = GUILayoutUtility.GetRect(4096, 8192, 4096, 8192, GUIStyle.none, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        GUI.BeginClip(rect, _scrollMain, Vector2.zero, false);

        const int bgSize = 1 << 14;
        int scrollx = -(int)(CellSize * (int)(_scrollMain.x / CellSize));
        int scrolly = -(int)(CellSize * (int)(_scrollMain.y / CellSize));
        Rect bgRect = new Rect(scrollx - CellSize * 4, scrolly - CellSize * 4, bgSize, bgSize);
        Rect bgCoords = bgRect;
        bgCoords.width /= CellSize;
        bgCoords.height /= CellSize;
        GUI.DrawTextureWithTexCoords(bgRect, Styles.BackgroundGrid, bgCoords);
        //GUILayout.Box("", EditorStyles.label, GUILayout.Width(_mainViewSize.x + 1024), GUILayout.Height(_mainViewSize.y + 1024));

        DrawLinks();

        DrawNodes();
        _squareSelection.Process(_nodeList);
        GUI.EndClip();
        //EditorGUILayout.EndScrollView();


        if (Event.current.type == EventType.ContextClick)
        {
            Vector2 mousePos = Event.current.mousePosition;
            _rightClickMenuPosition = mousePos;
            _rightClickMenu.ShowAsContext();
            Event.current.Use();
        }


        if (Event.current.type == EventType.mouseDown)
        {
            _activeWindowID = -1;
            EditorGUI.FocusTextInControl("");
        }

        if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Space)
        {
            Vector2 pos = new Vector2();
            foreach (NodeData.Node node in _nodeList)
                pos += node.Position;
            pos /= _nodeList.Count;
            _scrollMain = new Vector2(512, 512) - pos;
        }

        //_squareSelection.Process(_nodeList);

        _scrollMain = _graphDraging.Drag(_scrollMain);
    }

    static Vector2 GetNodeInputPosition(NodeData.Node node, int input)
    {
        int properties = 0;
        if (node.DrawProperties != null) properties = node.DrawProperties.Length;
        return node.Position + new Vector2(-10, (properties + input + 1) * CellSize);
    }

    static Vector2 GetNodeOutputPosition(NodeData.Node node)
    {
        float nodeWidth = CellSize * node.NodeWidth;
        float nodeHeight = CellSize * node.NodeHeight;

        return node.Position + new Vector2(nodeWidth + 10, nodeHeight * 0.5f);
    }

    void DrawLinks()
    {
        if (Event.current.type == EventType.Repaint)
            foreach (NodeData.Node node in _nodeList)
            {
                if (node == null) continue;
                for (int l = 0; l < node.Inputs.Length; l++)
                {
                    NodeData.Link link = node.Inputs[l];
                    NodeData.Node inNode = _nodeData.GetNode(link);

                    if (inNode != null)
                    {
                        Vector2 p1 = GetNodeInputPosition(node, l);
                        Vector2 p0 = GetNodeOutputPosition(inNode);

                        DrawCurve(p0, p1, Styles.GetColor(inNode.CashedType));

                    }
                }
            }
        _linkDraging.DrawLink();
    }

    void DrawNodes()
    {

        //if (Event.current.type == EventType.MouseDrag || Event.current.type == EventType.MouseMove || Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseUp) Repaint();
        Repaint();
        foreach (NodeData.Node node in _nodeList)
        {
            if (node == null) continue;
            DrawNode(node);
            _mainViewSize = Vector2.Max(_mainViewSize, node.Position);
        }

        _linkDraging.AfterDrawNodes();
    }

    void DrawNode(NodeData.Node node)
    {
        Event currentEvent = Event.current;
        EventType eventType = currentEvent.type;
        EditorGUIUtility.labelWidth = 50;
        float nodeWidth = CellSize * node.NodeWidth;
        const float widthPadding = 2;

        int nodeHeight = node.NodeHeight;
        Rect rect = new Rect(node.Position, new Vector2(nodeWidth, CellSize * nodeHeight));

        bool focused = _activeWindowID == node.WindowID;
        bool selected = _squareSelection.Selection.Contains(node);

        GUI.color = Styles.GetColor(node.Type);
        GUI.Box(rect, "", focused || selected ? Styles.NodeActive : Styles.Node);
        GUI.color = Color.white;

        Rect labelRect = rect;
        labelRect.height = CellSize;
        labelRect.y -= labelRect.height;
        
        GUI.Label(labelRect, node.Name, Styles.Title);


        if ((eventType == EventType.MouseDown || eventType == EventType.Used) && rect.Contains(currentEvent.mousePosition))
        {
            if (_activeWindowID != node.WindowID)
                EditorGUI.FocusTextInControl("");

            _activeWindowID = node.WindowID;
            if (!selected)
                _squareSelection.Selection.Clear();
        }

        EditorGUI.BeginChangeCheck();
        float y = node.Position.y + CellSize / 2;
        float x = node.Position.x + widthPadding;
        if (node.DrawProperties != null && node.DrawProperties.Length > 0)
        {
            Rect propertyRect = new Rect(x, y, nodeWidth - widthPadding * 3, 16);
            for (int i = 0; i < node.DrawProperties.Length; i++)
            {
                DrawProperty(propertyRect, node, node.DrawProperties[i], node.DrawProperties[i]);
                propertyRect.y = y += propertyRect.height;
            }
        }
        if (EditorGUI.EndChangeCheck()) RebuildGraph();

        Rect inputRect = new Rect(x, y, nodeWidth, CellSize);
        Rect inputBoxRect = new Rect(node.Position.x - 10, y + 3, 10, 12);
        Rect bigInputRect = new Rect(node.Position.x - 10, y, inputRect.width + inputBoxRect.width, CellSize);
        Rect smallInputRect = new Rect(inputBoxRect.x, y, inputBoxRect.width, CellSize);
        for (int i = 0; i < node.Inputs.Length; i++)
        {
            NodeData.Link link = node.Inputs[i];
            NodeData.Node inNode = _nodeData.GetNode(link);
            //bool mouseHover = allInputRect.Contains(currentEvent.mousePosition);

            GUI.Label(inputRect, link.Name);
            GUIStyle gs = inNode != null && smallInputRect.Contains(currentEvent.mousePosition) ? Styles.InputBoxActive : Styles.InputBox;
            GUI.color = Styles.GetColor(inNode != null ? inNode.CashedType : link.Type);
            GUI.Box(inputBoxRect, "", gs);
            GUI.color = Color.white;
            _linkDraging.ProcessNodeInput(node, i, bigInputRect, smallInputRect);

            inputRect.y = y += inputRect.height;
            inputBoxRect.y = inputRect.y + 3;
            bigInputRect.y = inputRect.y;
            smallInputRect.y = inputRect.y;
        }

        if (node.Type != NodeData.ValueType.None)
        {
            Rect outputRect = new Rect(rect.x + nodeWidth, rect.y + rect.height * 0.5f - 8, 10, 16);
            GUI.color = Styles.GetColor(node.CashedType);
            GUI.Box(outputRect, "");
            GUI.color = Color.white;
            _linkDraging.ProcessNodeOutput(node, outputRect);
        }

     

        if (focused && eventType == EventType.KeyDown && currentEvent.keyCode == KeyCode.Delete && GUIUtility.keyboardControl == 0)
        {
            _nodeData.RemoveNode(node);
            RebuildGraph();
            currentEvent.Use();
        }

        //rect.position += new Vector2(0, CellSize);
        Rect fullRect = rect;
        fullRect.height += CellSize;
        fullRect.y -= CellSize;

        if (Event.current.type == EventType.ContextClick && fullRect.Contains(currentEvent.mousePosition))
        {
            GenericMenu nodeMenu = new GenericMenu();
            string s = node.GetType().Name + "s[" + node.NodeId + "]";
            nodeMenu.AddItem(new GUIContent("To Clipboard: " + s), false, (st) => { EditorGUIUtility.systemCopyBuffer = (string)st; }, s);
            nodeMenu.ShowAsContext();
            Event.current.Use();
        }

        if (focused && eventType == EventType.ValidateCommand && currentEvent.commandName == "Duplicate")
        {
            NodeData.Node newNode = _nodeData.CreateNode(node.NodeType);
            newNode.Position = node.Position + new Vector2(64, 64);
            RebuildGraph();
            currentEvent.Use();
        }
        Vector2 newPos = _draging.Drag(fullRect, node.Position, node);
        Styles.SnapPosition(ref newPos);
        if (node.Position != newPos)
        {
            Vector2 delta = newPos - node.Position;
            foreach (NodeData.Node n in _squareSelection.Selection)
                n.Position += delta;
            node.Position = newPos;
        }
    }

    static void DrawProperty(Rect rect, NodeData.Node node, string property, string name)
    {
        FieldInfo field = node.GetType().GetField(property);
        if (field == null) return;
        object value = field.GetValue(node);

        if (field.FieldType == typeof (Transform))
            field.SetValue(node, EditorGUI.ObjectField(rect, name, (Transform)value, typeof (Transform), true));
        else if (field.FieldType == typeof (float))
            field.SetValue(node, EditorGUI.FloatField(rect, name, (float)value));
        else if (field.FieldType == typeof (Vector2))
            field.SetValue(node, EditorGUI.Vector2Field(rect, "", (Vector2)value));
        else if (field.FieldType == typeof (Vector3))
            field.SetValue(node, EditorGUI.Vector3Field(rect, "", (Vector3)value));
        else if (field.FieldType == typeof (Vector4))
        {
            rect.position -= new Vector2(0, CellSize);
            field.SetValue(node, EditorGUI.Vector4Field(rect, "", (Vector4)value));
        }
        else if (field.FieldType == typeof (Color))
            field.SetValue(node, EditorGUI.ColorField(rect, "", (Color)value));
        else if (field.FieldType == typeof (int))
            field.SetValue(node, EditorGUI.IntField(rect, name, (int)value));
        else if (field.FieldType == typeof (bool))
            field.SetValue(node, EditorGUI.Toggle(rect, name, (bool)value));
        else if (field.FieldType == typeof (AnimationCurve))
            field.SetValue(node, EditorGUI.CurveField(rect, "", (AnimationCurve)value));
        else if (field.FieldType == typeof (string))
            field.SetValue(node, EditorGUI.TextField(rect, "", (string)value));
        else if (field.FieldType == typeof (Component))
            field.SetValue(node, EditorGUI.ObjectField(rect, "", (Component)value, typeof (Component), true));
        else if (field.FieldType.BaseType == typeof (Enum))
            field.SetValue(node, EditorGUI.EnumPopup(rect, "", (Enum)value));
        else if (field.FieldType == typeof(Gradient))
            field.SetValue(node, GradientField(rect, (Gradient)value));
        else
            GUI.Label(rect, name + "   " + field.FieldType);
    }

    static readonly MethodInfo GradientMethodInfo = typeof(EditorGUI).GetMethod("GradientField", BindingFlags.NonPublic | BindingFlags.Static, null, new Type[] { typeof(Rect), typeof(Gradient) }, null);
    public static object GradientField(Rect rect, Gradient gradient)
    {
        return GradientMethodInfo.Invoke(null, new object[] { rect, gradient });
    }

    static void DrawCurve(Vector2 p0, Vector2 p1, Color color)
    {
        Vector2 dir = p1 - p0;
        //Vector2 normal = new Vector2(10 + dir.magnitude * 0.1f, 0);
        Vector2 normal = new Vector2(Mathf.Sqrt(6f + Mathf.Max(-dir.x, 0f)) * 6f, 0f);

        Vector2 n0 = p0 + normal;
        Vector2 n1 = p1 - normal;
        //float dist = HandleUtility.DistancePointBezier(Event.current.mousePosition, p0, p1, n0, n1);
        Handles.DrawBezier(p0, p1, n0, n1, color, null, 3);
    }

    void DrawBigMessage(string message)
    {
        GUILayout.Label(message, Styles.BigMessageLabel, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
    }



    void RebuildGraph()
    {
        _nodeList = _nodeData.GetAllNodes();
        _deadEnds = _nodeData.GetDeadEnds(_nodeList);

        foreach (NodeData.Node node in _nodeList)
            node.Processor = null;

        List<NodeData.Link> outputs = new List<NodeData.Link>();
        foreach (NodeData.Node node in _deadEnds)
        {
            node.UpdateTypes(node.Type, _nodeData, new HashSet<NodeData.Node>());
            NodeData.NodeProcessor pr = _nodeData.GetNodeProcessor(node);

            if (!_nodeData.ValidateNodes(node)) continue;
            if (pr != null && pr.VoidOut != null && node.Type != NodeData.ValueType.Error)
                outputs.Add(new NodeData.Link {NodeType = node.NodeType, NodeId = node.NodeId});
        }
        _nodeData.Outputs = outputs.ToArray();
        _nodeData.Prepare();

        if (_selectionProcessor.Host != null)
        {
            //Undo.RecordObject(_selectionProcessor.Host, "Qwe");
            EditorUtility.SetDirty(_selectionProcessor.Host);
        }



    }



    class Draging
    {
        public NodeData.Node Node;
        public Vector2 Delta;


        public Vector2 Drag(Rect rect, Vector2 position, NodeData.Node node)
        {
            Vector2 mousePosition = Event.current.mousePosition;
            Vector2 mouseScreenPosition = GUIUtility.GUIToScreenPoint(mousePosition);
            if (Node == null && Event.current.type == EventType.MouseDown && Event.current.button == 0 && rect.Contains(mousePosition))
            {
                Delta = position - mouseScreenPosition;
                Node = node;
                Event.current.Use();

            }
            if (Node == node)
            {
                if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
                {
                    Node = null;
                    Event.current.Use();
                }
                return mouseScreenPosition + Delta;
            }
            return position;
        }
    }

    class LinkDraging
    {
        public NodeEditorWindow Window;
        NodeData.Node _node;
        Vector2 _inputPosition;
        bool _hoverInput;

        public LinkDraging(NodeEditorWindow window)
        {
            Window = window;
        }

        public void DrawLink()
        {
            if (_node != null)
            {
                if (Event.current.type != EventType.Repaint) return;
                Vector2 linkPosition = _hoverInput ? _inputPosition : Event.current.mousePosition;
                _hoverInput = false;
                DrawCurve(GetNodeOutputPosition(_node), linkPosition, new Color(1, 1, 1, 0.5f));
                Window.Repaint();
            }
        }

        public void AfterDrawNodes()
        {
            if (_node != null)
            {
                if (Event.current.type == EventType.mouseUp)
                {
                    _node = null;
                    Window.RebuildGraph();
                    Event.current.Use();
                }
            }
        }

        public void ProcessNodeInput(NodeData.Node node, int inputId, Rect bigRect, Rect smallRect)
        {

            if (_node != null)//draging
            {
                if (!bigRect.Contains(Event.current.mousePosition)) return;
                NodeData.Link link = node.Inputs[inputId];

                bool validate = (NodeData.ValidateTypes(link.Type, _node.Type) || NodeData.ValidateTypes(_node.Type, link.Type));// && link.NodeType == -1;

                if (Event.current.type == EventType.Repaint && validate)
                {
                    _hoverInput = true;
                    _inputPosition = GetNodeInputPosition(node, inputId);
                }

                if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
                {
                    if (validate)
                    {
                        link.NodeType = _node.NodeType;
                        link.NodeId = _node.NodeId;
                    }
                }
            }
            else//
            {
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && smallRect.Contains(Event.current.mousePosition))
                {
                    NodeData.Link link = node.Inputs[inputId];
                    NodeData.Node inNode = Window._nodeData.GetNode(link);
                    if (inNode != null)
                    {
                        _node = inNode;
                        link.NodeType = -1;
                        link.NodeId = -1;
                        _hoverInput = false;
                        Window.RebuildGraph();
                        Event.current.Use();
                    }
                }
            }
        }
        public void ProcessNodeOutput(NodeData.Node node, Rect outputRect)
        {
            if (_node == null && Event.current.type == EventType.MouseDown)//draging
            {
                if (outputRect.Contains(Event.current.mousePosition))
                {
                    _node = node;
                    Event.current.Use();

                }
            }
        }

    }

    class GraphDraging
    {
        Vector2 _delta;
        bool _draging;

        public Vector2 Drag(Vector2 position)
        {
            Vector2 mousePosition = Event.current.mousePosition;
            Vector2 mouseScreenPosition = GUIUtility.GUIToScreenPoint(mousePosition);

            if (Event.current.type == EventType.MouseDown && Event.current.button == 2)
            {
                _delta = position - mouseScreenPosition;
                _draging = true;
                Event.current.Use();
            }
            if (_draging)
            {
                if (Event.current.type == EventType.MouseUp && Event.current.button == 2)
                {
                    _draging = false;
                    Event.current.Use();
                }
                return mouseScreenPosition + _delta;
            }
            return position;
        }
    }

    class SelectionProcessor
    {
        public Action<NodeData> OnNewDataSelected;
        GenericMenu _dataMenu;
        Object _lastObj;
        Item _firstElement = null;
        public Object Host;


        public SelectionProcessor(Action<NodeData> onNewDataSelected)
        {
            OnNewDataSelected = onNewDataSelected;
            _dataMenu = new GenericMenu();
        }

        public void Process()
        {
            Object obj = Selection.activeObject;

            if (_lastObj != obj)
            {
                _lastObj = obj;
                _dataMenu = new GenericMenu();
                _firstElement = null;

                if (obj is GameObject)
                {
                    GameObject go = (GameObject)obj;
                    MonoBehaviour[] monoBehaviours = go.GetComponents<MonoBehaviour>();
                    foreach (MonoBehaviour monoBehaviour in monoBehaviours)
                        SelectionChenged(monoBehaviour);
                }
                else
                    SelectionChenged(obj);

                if (_firstElement != null)
                    Select(_firstElement);
            }
        }

        void SelectionChenged(Object obj)
        {
            if (obj == null) return;
            FieldInfo[] fields = obj.GetType().GetFields();
            foreach (FieldInfo fieldInfo in fields)
                if (fieldInfo.FieldType == typeof (NodeData))
                {
                    NodeData nodeData = (NodeData)fieldInfo.GetValue(obj);
                    Item item = new Item(nodeData, obj);
                    if (_firstElement == null) _firstElement = item;
                    _dataMenu.AddItem(new GUIContent(fieldInfo.Name), false, Select, item);
                }

        }

        void Select(object obj)
        {
            Item item = obj as Item;
            if (item == null) return;
            if (OnNewDataSelected != null)
                OnNewDataSelected(item.Data);
            Host = item.Host;
        }

        public void DropDown()
        {
            if (_dataMenu.GetItemCount() < 2) return;
            if (GUILayout.Button("Select", EditorStyles.toolbarDropDown))
                _dataMenu.DropDown(new Rect(1, EditorStyles.toolbar.fixedHeight - 2, 1, 1));
        }

        public void Reset()
        {
            _lastObj = null;
            Process();
        }

        class Item
        {
            public NodeData Data;
            public Object Host;
            public Item(NodeData data, Object host)
            {
                Data = data;
                Host = host;
            }
        }

    }


    class SquareSelection
    {
        Vector2 _startPos;
        bool _draging;
        bool _phase2;
        public readonly HashSet<NodeData.Node> Selection = new HashSet<NodeData.Node>();

        public void Process(List<NodeData.Node> nodeList)
        {
            Vector2 mousePosition = Event.current.mousePosition;
            Vector2 mouseScreenPosition = GUIUtility.GUIToScreenPoint(mousePosition);
            if (!_draging)
            {
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
                {
                    _startPos = mouseScreenPosition;
                    _draging = true;
                    _phase2 = false;
                    //Event.current.Use();
                }
            }
            else
            {
                if (!_phase2)
                {
                    if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
                    {
                        _draging = false;
                        _phase2 = false;
                        Selection.Clear();
                        Event.current.Use();
                    }
                    if ((_startPos - mouseScreenPosition).magnitude > 5)
                        _phase2 = true;
                }
                else
                {
                    Rect rect = new Rect();
                    Vector2 start = GUIUtility.ScreenToGUIPoint(_startPos);
                    Vector2 end = GUIUtility.ScreenToGUIPoint(mouseScreenPosition);
                    rect.min = Vector2.Min(start, end);
                    rect.max = Vector2.Max(start, end);

                    GUI.Box(rect, "", new GUIStyle("SelectionRect"));
             

                    if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
                    {
                        _draging = false;
                        _phase2 = false;
                        Selection.Clear();
                        foreach (NodeData.Node node in nodeList)
                        {
                            Rect nRect = new Rect(node.Position, new Vector2(CellSize * node.NodeWidth, CellSize * node.NodeHeight));
                            if (rect.Overlaps(nRect))
                                Selection.Add(node);
                        }
                        Event.current.Use();
                    }
                }
            }
        }
    }
}


