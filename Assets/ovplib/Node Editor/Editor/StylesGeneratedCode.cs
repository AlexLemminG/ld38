using UnityEngine;
public partial class NodeEditorWindow{
static partial class Styles{
static void InitializeColors(){
TypeColors = new Color[16];
TypeColors[(int)NodeData.ValueType.None] = new Color(1.0f,1.0f,1.0f,1.0f);
TypeColors[(int)NodeData.ValueType.Error] = new Color(.584f,.098f,.584f,1.0f);
TypeColors[(int)NodeData.ValueType.AutoValue] = new Color(.837f,.881f,.89f,1.0f);
TypeColors[(int)NodeData.ValueType.AutoVector] = new Color(.739f,.777f,.784f,1.0f);
TypeColors[(int)NodeData.ValueType.Float] = new Color(.745f,.953f,.996f,1.0f);
TypeColors[(int)NodeData.ValueType.Vector2] = new Color(.592f,.827f,.949f,1.0f);
TypeColors[(int)NodeData.ValueType.Vector3] = new Color(.482f,.714f,.957f,1.0f);
TypeColors[(int)NodeData.ValueType.Vector4] = new Color(.314f,.514f,.933f,1.0f);
TypeColors[(int)NodeData.ValueType.Color] = new Color(1.0f,.949f,.686f,1.0f);
TypeColors[(int)NodeData.ValueType.Int] = new Color(.984f,.663f,.6f,1.0f);
TypeColors[(int)NodeData.ValueType.Bool] = new Color(.518f,.408f,.388f,1.0f);
TypeColors[(int)NodeData.ValueType.FloatMap2D] = new Color(.165f,.898f,.114f,1.0f);
TypeColors[(int)NodeData.ValueType.Texture2D] = new Color(.165f,.898f,.114f,1.0f);
TypeColors[(int)NodeData.ValueType.RenderTexture] = new Color(.165f,.898f,.114f,1.0f);
TypeColors[(int)NodeData.ValueType.Polygon] = new Color(1.0f,1.0f,1.0f,1.0f);
TypeColors[(int)NodeData.ValueType.Mesh] = new Color(1.0f,1.0f,1.0f,1.0f);
}}}