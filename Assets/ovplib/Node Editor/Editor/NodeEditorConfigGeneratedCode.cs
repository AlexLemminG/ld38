
using UnityEngine;
public partial class NodeEditorConfig{
[System.Serializable]
public class StyleColors{
public Color NoneColor;
public Color ErrorColor;
public Color AutoValueColor;
public Color AutoVectorColor;
public Color FloatColor;
public Color Vector2Color;
public Color Vector3Color;
public Color Vector4Color;
public Color ColorColor;
public Color IntColor;
public Color BoolColor;
public Color FloatMap2DColor;
public Color Texture2DColor;
public Color RenderTextureColor;
public Color PolygonColor;
public Color MeshColor;
public Color[] GetColors(){ 
return new Color[]{
NoneColor,
ErrorColor,
AutoValueColor,
AutoVectorColor,
FloatColor,
Vector2Color,
Vector3Color,
Vector4Color,
ColorColor,
IntColor,
BoolColor,
FloatMap2DColor,
Texture2DColor,
RenderTextureColor,
PolygonColor,
MeshColor,
};}
}}