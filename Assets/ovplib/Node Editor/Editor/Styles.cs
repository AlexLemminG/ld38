using UnityEngine;

public partial class NodeEditorWindow
{
    static partial class Styles
    {
        static bool _initializd;
        public static GUIStyle Background;
        public static GUIStyle Node;
        public static GUIStyle NodeActive;
        public static GUIStyle Title;
        public static GUIStyle InputBox;
        public static GUIStyle InputBoxActive;
        public static GUIStyle BigMessageLabel;

        public static Color[] TypeColors;

        public static Texture2D BackgroundGrid;


        public static void Initialize()
        {
            if (BackgroundGrid == null)
            {
                const float bg = 0.35f;
                const float lines = 0.3f;
                BackgroundGrid = GetGridTexture((int)CellSize, new Color(bg, bg, bg, 1f), new Color(lines, lines, lines, 1f));
 
            }
            if (Background == null)
            {

                InitializeColors();
                Background = new GUIStyle("flow background");

                Node = new GUIStyle("flow node 0");//TE NodeBox
                NodeActive = new GUIStyle("flow node 0 on");

                Title = new GUIStyle("ShurikenLabel");
                Title.alignment = TextAnchor.LowerRight;
                Title.fontStyle = FontStyle.Bold;
                Title.normal.textColor = Gray(1f, 0.5f);
                Title.normal.background = GetRectTexture(Gray(0, 0.2f), Gray(0.0f, 0.4f));
                Title.border = new RectOffset(1, 1, 1, 1);

                InputBox = new GUIStyle("box");//flow overlay box, HelpBox
                InputBox.normal.background = GetRectTexture(Gray(0.8f, 1f), Gray(0.4f, 1f));
                InputBox.border = new RectOffset(1, 1, 1, 1);
                InputBoxActive = new GUIStyle(InputBox);
                InputBoxActive.normal.background = GetRectTexture(Gray(1, 1f), Gray(0.6f, 1f));
               

                BigMessageLabel = new GUIStyle("HeaderLabel");
                BigMessageLabel.fontSize = 32;
                BigMessageLabel.alignment = TextAnchor.MiddleCenter;
            }


        }
        
        public static Color GetColor(NodeData.ValueType type)
        {
            return TypeColors[(int)type];
        }

        static Texture2D GetGridTexture(int size,Color background,Color lines)
        {
            Color[] colors = new Color[size * size];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = background;

            for (int i = 0; i < size; i++)
                colors[i] = lines;

            for (int i = 0; i < colors.Length; i += size)
                colors[i] = lines;


            Texture2D tex = new Texture2D(size, size);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Repeat;
            tex.SetPixels(colors);
            tex.Apply(false);
            return tex;
        }

        static Texture2D GetRectTexture(Color background, Color lines)
        {
            Color[] colors = new Color[]
            {
                lines, lines, lines,
                lines, background, lines,
                lines, lines, lines,
            };
            Texture2D tex = new Texture2D(3, 3);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Repeat;
            tex.SetPixels(colors);
            tex.Apply(false);
            return tex;
        }

        static Color Gray(float light, float alpha)
        {
            return new Color(light, light, light, alpha);
        }

        static Texture2D GetOnePixelTex(Color color)
        {
            Texture2D tex = new Texture2D(1, 1);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Repeat;
            tex.SetPixels(new[] {color});
            tex.Apply(false);
            return tex;
        }

        public static void SnapPosition(ref Vector2 pos)
        {
            pos.x = ((int)(pos.x / CellSize)) * CellSize;
            pos.y = ((int)(pos.y / CellSize)) * CellSize;
        }
    }
}