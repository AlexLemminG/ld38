using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public partial class NodeEditorConfig: ScriptableObject
{
    public TextAsset NodeDataFile;
    public TextAsset WindowStylesFile;
    public TextAsset NodeEditorConfigFile;
    public bool Generate;
    [Space(32)]
    public StyleColors Colors;


    void OnValidate()
    {
        if (Generate)
        {
            Generate = false;
            GenerateFile(NodeDataFile, GenerateNodeDataCode());
            GenerateFile(NodeEditorConfigFile, GenerateNodeEditorConfigCode());
            GenerateFile(WindowStylesFile, GenerateWindowStyles());
            //Debug.Log(GenerateWindowStyles());
        }
    }
    static void GenerateFile(TextAsset file,string text)
    {
        string path = AssetDatabase.GetAssetPath(file);
        File.WriteAllText(path, text);
        AssetDatabase.ImportAsset(path);
    }

    string GenerateWindowStyles()
    {
        string[] valueTypes = Enum.GetNames(typeof (NodeData.ValueType));

        string s = "";
        Color[] colors = Colors.GetColors();

        s += "using UnityEngine;\npublic partial class NodeEditorWindow{\nstatic partial class Styles{";

        s += "\nstatic void InitializeColors(){";
        s += "\nTypeColors = new Color[" + valueTypes.Length + "];";
        for (int i = 0; i < valueTypes.Length; i++)
            s += "\nTypeColors[(int)NodeData.ValueType." + valueTypes[i] + "] = new Color(" + 
                colors[i].r.ToString("#.0##") + "f," + colors[i].g.ToString("#.0##") + "f," + colors[i].b.ToString("#.0##") + "f," + colors[i].a.ToString("#.0##") + "f);";
        s += "\n}";


        s += "}}";
        return s;
    }


    static string GenerateNodeEditorConfigCode()
    {
        string s = "\nusing UnityEngine;\npublic partial class NodeEditorConfig{";


        s += "\n[System.Serializable]\npublic class StyleColors{";
        string[] valueTypes = Enum.GetNames(typeof (NodeData.ValueType));
        for (int i = 0; i < valueTypes.Length; i++)
            s += "\npublic Color " + valueTypes[i] + "Color;";

        s += "\npublic Color[] GetColors(){ \nreturn new Color[]{";
        for (int i = 0; i < valueTypes.Length; i++)
            s +="\n"+ valueTypes[i] + "Color,";
        s += "\n};}";


        s += "\n}}";
        return s;
    }


    static string GenerateNodeDataCode()
    {
        List<Type> typeList = typeof(NodeData).GetNestedTypes().Where(type => type.IsSubclassOf(typeof(NodeData.Node))).ToList();
        SortedDictionary<string, Type> typesDict = new SortedDictionary<string, Type>();
        foreach (Type type in typeList)
            typesDict.Add(type.Name, type);
        string[] typeName = new string[typesDict.Count];
        typesDict.Keys.CopyTo(typeName, 0);

        string[] pathArray = new string[typeList.Count];
        for (int i = 0; i < typeName.Length; i++)
        {
            Type type = typesDict[typeName[i]];
            NodeData.Node node = (NodeData.Node)type.GetConstructor(new Type[0]).Invoke(new object[0]);
            pathArray[i] = node.GetPath();
        }


        string s = "public partial class NodeData{";


        s += "\npublic const int NodeTypesCount = " + typeName.Length + ";";

        for (int i = 0; i < typeName.Length; i++)
            s += "\npublic " + typeName[i] + "[] " + typeName[i] + "s;";

        s += "\npublic static NodeType[] NodesTypes = {";
        for (int i = 0; i < typeName.Length; i++)
            s += "\nnew NodeType {Type = typeof (" + typeName[i] + "), TypeID = " + i + ", Path = \"" + pathArray[i] + "\"},";
        s += "\n};";

        s += "\npublic Node[] GetArray(int type){switch (type){";
        for (int i = 0; i < typeName.Length; i++)
            s += "\ncase " + i + ": return " + typeName[i] + "s;";
        s += "\ndefault:return null;}}";

        s += "\npublic void SetArray(int type, Node[] nodes){switch (type){";
        for (int i = 0; i < typeName.Length; i++)
            s += "\ncase " + i + ":" + typeName[i] + "s = (" + typeName[i] + "[])nodes;break;";
        s += "\n}}";

        s += "\npublic void AddElement(int type, Node node){switch (type){";
        for (int i = 0; i < typeName.Length; i++)
            s += "\ncase " + i + ": AddElement(ref " + typeName[i] + "s , (" + typeName[i] + ")node);break;";
        s += "\n}}";

        s += "\npublic void DecreaseArray(int type){switch (type){";
        for (int i = 0; i < typeName.Length; i++)
            s += "\ncase " + i + ": DecreaseArray(ref " + typeName[i] + "s );break;";
        s += "\n}}";

        s += "\npublic void FixNullArrays(){";
        for (int i = 0; i < typeName.Length; i++)
            s += "\nif(" + typeName[i] + "s == null) " + typeName[i] + "s = new " + typeName[i] + "[0];";
        s += "\n}";

        /*
        s += "public Node CreateNode(int type){switch (type){";
        for (int i = 0; i < nodesTypes.Length; i++)
            s += "case " + i + ":return new " + nodesTypes[i].Type.Name + "();";
        s += "default: return null;}}";
        //*/

        s += "\n}";
        return s;
    }
    

    [MenuItem("Assets/Create/ScriptableObject")]
    public static void CreateAsset()
    {
        ScriptableObject asset = CreateInstance<ScriptableObject>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
            path = "Assets";
        else if (Path.GetExtension(path) != "")
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New ScriptableObject" + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}