using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof (NodeData))]
public class NodeDataDrawer: PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //position.width *= 0.8f;
        //position.x += position.width;

        if (GUI.Button(position, "Open " + label.text + " Graph"))
        {
            NodeEditorWindow mw = EditorWindow.GetWindow<NodeEditorWindow>("Node Editor");
            mw.Initialize((NodeData)GetObject(property));
        }
    }
    static object GetObject(SerializedProperty property)
    {
        object obj = property.serializedObject.targetObject;
        return obj.GetType().GetField(property.name).GetValue(obj);
    }

}