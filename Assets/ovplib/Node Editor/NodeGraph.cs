﻿using UnityEngine;

[ExecuteInEditMode]
public class NodeGraph: MonoBehaviour
{
    public NodeData Data = new NodeData();

    void Awake()
    {
        if (Data != null)
        {
            Data.CteateOutput(NodeData.ValueType.Vector3, 0, "Transform Pos");
            Data.CteateInput(NodeData.ValueType.Vector3, 1, "Mouse Pos");
            Data.Prepare();
        }
    }

    void Update()
    {
        if (Data == null) return;
        Data.Process();
        Data.Vector3Nodes[1].Value = Input.mousePosition;
        transform.position = Data.Vector3Nodes[0].Value;

    }
}
