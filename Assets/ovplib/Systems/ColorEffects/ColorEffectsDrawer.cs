using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof (ColorEffects))]
public class ColorEffectsDrawer: PropertyDrawer
{
    static GUIStyle _backgroundStyle;
    string _propertyPathWichDropdownOpened;
    Dictionary<string, ReorderableList> _lists = new Dictionary<string, ReorderableList>();

    ReorderableList Init(SerializedProperty property)
    {
        ReorderableList list = new ReorderableList(property.serializedObject, property.FindPropertyRelative("EffectArray"), true, true, true, true);
        list.elementHeightCallback = (int index) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            ColorEffects.Type type = (ColorEffects.Type)element.FindPropertyRelative("Type").enumValueIndex;
            return GetElementHeight(type);
        };
        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            DrawElementCallback(rect, element);
        };

        list.drawHeaderCallback = (Rect rect) => { };
        list.drawElementBackgroundCallback = (rect, index, active, focused) =>
        {
            if (_backgroundStyle == null)_backgroundStyle = new GUIStyle("RL Element");
            if (Event.current.type == EventType.Repaint && active)
            {
                Color savedColor = GUI.backgroundColor;
                GUI.backgroundColor = new Color(1f, 1f, 1f, 0.2f);
                
                SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty typeProperty = element.FindPropertyRelative("Type");
                if (typeProperty == null) return;
                ColorEffects.Type type = (ColorEffects.Type)typeProperty.enumValueIndex;
                rect.height = GetElementHeight(type);
                _backgroundStyle.Draw(rect, focused, false, active, false);
                GUI.backgroundColor = savedColor;
            }
        };


        list.onAddDropdownCallback = (Rect buttonRect, ReorderableList l) =>
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Levels"), false, AddItem, ColorEffects.Type.Levels);
            menu.AddItem(new GUIContent("Values"), false, AddItem, ColorEffects.Type.Values);
            menu.AddItem(new GUIContent("Gradient"), false, AddItem, ColorEffects.Type.Gradient);
            menu.ShowAsContext();

            string path = l.serializedProperty.propertyPath;
            path = path.Substring(0, path.Length - ".EffectArray".Length);
            _propertyPathWichDropdownOpened = path;
        };
        return list;
    }

    float GetElementHeight(ColorEffects.Type type)
    {
        int lines = 1;
        switch (type)
        {
            case ColorEffects.Type.Levels:
                lines = 4;
                break;
            case ColorEffects.Type.Values:
                lines = 5;
                break;
            case ColorEffects.Type.Gradient:
                lines = 3;
                break;
        }
        return EditorGUIUtility.singleLineHeight * lines + 8;
    }

    static void DrawElementCallback(Rect rect, SerializedProperty element)
    {
        ColorEffects.Type type = (ColorEffects.Type)element.FindPropertyRelative("Type").enumValueIndex;
        float lineHeight = EditorGUIUtility.singleLineHeight;

        {
            Rect boxRect = rect;
            boxRect.height = 2;
            boxRect.y -= 2;
            GUI.Box(boxRect, GUIContent.none);
        }

        Rect nameRect = new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight);
        Rect valRect = new Rect(rect.x + 60, rect.y, rect.width - 60, lineHeight);
        bool guiEnabled = GUI.enabled;

        {
            var enabled = element.FindPropertyRelative("Enabled");
            enabled.boolValue = GUI.Toggle(nameRect, enabled.boolValue, type.ToString());
            if (guiEnabled) GUI.enabled = enabled.boolValue;

            int offset = 24;
            nameRect.x += offset;
            nameRect.width -= offset - 30;
        }

        SerializedProperty ints = element.FindPropertyRelative("Ints");
        SerializedProperty floats = element.FindPropertyRelative("Floats");

        switch (type)
        {
            case ColorEffects.Type.Levels:
            {
                if (ints.arraySize < 1 || floats.arraySize < 7) break;
                float min = floats.GetArrayElementAtIndex(0).floatValue;
                float max = floats.GetArrayElementAtIndex(1).floatValue - 1;

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                ColorEffects.Channel channel = (ColorEffects.Channel)ints.GetArrayElementAtIndex(0).intValue;
                ints.GetArrayElementAtIndex(0).intValue = (int)(ColorEffects.Channel)EditorGUI.EnumPopup(valRect, channel);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "min");
                min = EditorGUI.Slider(valRect, min, -1f, 1f);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "max");
                max = EditorGUI.Slider(valRect, max, -1f, 1f);


                floats.GetArrayElementAtIndex(0).floatValue = min;
                floats.GetArrayElementAtIndex(1).floatValue = max + 1;
                break;
            }
            case ColorEffects.Type.Values:
            {
                float brightness = floats.GetArrayElementAtIndex(0).floatValue;
                float contrast = floats.GetArrayElementAtIndex(1).floatValue;
                float saturation = floats.GetArrayElementAtIndex(2).floatValue;
                float gamma = floats.GetArrayElementAtIndex(3).floatValue;

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "brightness");
                brightness = EditorGUI.Slider(valRect, brightness, -1f, 1f);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "contrast");
                contrast = EditorGUI.Slider(valRect, contrast, 0f, 2f);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "saturation");
                saturation = EditorGUI.Slider(valRect, saturation, 0f, 2f);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "gamma");
                gamma = EditorGUI.Slider(valRect, gamma, 0f, 2f);


                floats.GetArrayElementAtIndex(0).floatValue = brightness;
                floats.GetArrayElementAtIndex(1).floatValue = contrast;
                floats.GetArrayElementAtIndex(2).floatValue = saturation;
                floats.GetArrayElementAtIndex(3).floatValue = gamma;
                break;
            }
            case ColorEffects.Type.Gradient:
            {
                float opacity = floats.GetArrayElementAtIndex(6).floatValue;
                Color white = new Color(
                    floats.GetArrayElementAtIndex(0).floatValue,
                    floats.GetArrayElementAtIndex(1).floatValue,
                    floats.GetArrayElementAtIndex(2).floatValue, 1f);
                Color black = new Color(
                    floats.GetArrayElementAtIndex(3).floatValue,
                    floats.GetArrayElementAtIndex(4).floatValue,
                    floats.GetArrayElementAtIndex(5).floatValue, 1f);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                Rect colorRect = valRect;
                colorRect.width *= 0.5f;
                black = EditorGUI.ColorField(colorRect, black);
                colorRect.x += colorRect.width;
                white = EditorGUI.ColorField(colorRect, white);

                valRect.y += lineHeight;
                nameRect.y += lineHeight;
                GUI.Label(nameRect, "opacity");
                opacity = EditorGUI.Slider(valRect, opacity, 0f, 1f);

                floats.GetArrayElementAtIndex(0).floatValue = white.r;
                floats.GetArrayElementAtIndex(1).floatValue = white.g;
                floats.GetArrayElementAtIndex(2).floatValue = white.b;

                floats.GetArrayElementAtIndex(3).floatValue = black.r;
                floats.GetArrayElementAtIndex(4).floatValue = black.g;
                floats.GetArrayElementAtIndex(5).floatValue = black.b;
                floats.GetArrayElementAtIndex(6).floatValue = opacity;
                break;
            }
        }

        if (guiEnabled) GUI.enabled = true;
    }

    ReorderableList GetList(SerializedProperty property)
    {
        ReorderableList list;
        if (!_lists.TryGetValue(property.propertyPath, out list))
            _lists.Add(property.propertyPath, list = Init(property));
        return list;
    }
    ReorderableList GetList(string propertyPath)
    {
        ReorderableList list;
        return !_lists.TryGetValue(propertyPath, out list) ? null : list;
    }

    void AddItem(object data)
    {
        ReorderableList list = GetList(_propertyPathWichDropdownOpened);
        if (list == null) return;
        int index = list.serializedProperty.arraySize;
        list.serializedProperty.arraySize++;
        list.index = index;
        list.serializedProperty.serializedObject.ApplyModifiedProperties();

        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
        ColorEffects.Type type = (ColorEffects.Type)data;

        element.FindPropertyRelative("Enabled").boolValue = true;
        element.FindPropertyRelative("Type").enumValueIndex = (int)type;
        SerializedProperty ints = element.FindPropertyRelative("Ints");
        SerializedProperty floats = element.FindPropertyRelative("Floats");
        ints.arraySize = 1;
        floats.arraySize = 7;


        if (type == ColorEffects.Type.Levels)
        {
            ints.GetArrayElementAtIndex(0).intValue = (int)ColorEffects.Channel.RGB;
            floats.GetArrayElementAtIndex(0).floatValue = 0f;
            floats.GetArrayElementAtIndex(1).floatValue = 1f;
        }
        else if (type == ColorEffects.Type.Values)
        {
            floats.GetArrayElementAtIndex(0).floatValue = 0f;
            floats.GetArrayElementAtIndex(1).floatValue = 1f;
            floats.GetArrayElementAtIndex(2).floatValue = 1f;
            floats.GetArrayElementAtIndex(3).floatValue = 1f;
        }
        else if (type == ColorEffects.Type.Gradient)
        {
            floats.GetArrayElementAtIndex(0).floatValue = 1f;
            floats.GetArrayElementAtIndex(1).floatValue = 1f;
            floats.GetArrayElementAtIndex(2).floatValue = 1f;
            floats.GetArrayElementAtIndex(3).floatValue = 0f;
            floats.GetArrayElementAtIndex(4).floatValue = 0f;
            floats.GetArrayElementAtIndex(5).floatValue = 0f;
            floats.GetArrayElementAtIndex(6).floatValue = 0f;
        }


        list.serializedProperty.serializedObject.ApplyModifiedProperties();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (!property.FindPropertyRelative("Show").boolValue) return EditorGUIUtility.singleLineHeight;
        ReorderableList list = GetList(property);
        return list.GetHeight();
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var show = property.FindPropertyRelative("Show");
        var enabled = property.FindPropertyRelative("Enabled");
        if (show.boolValue)
        {
            GUI.enabled = enabled.boolValue;
            ReorderableList list = GetList(property);
            list.DoList(position);
            GUI.enabled = true;
        }
        else
        {
            GUI.Label(position, GUIContent.none, "RL Header");
        }
        Rect position2 = position;
        position2.width = 46;
        position2.x = position.width - position2.width;
        enabled.boolValue = EditorGUI.Toggle(position2, enabled.boolValue);
        show.boolValue = EditorGUI.Foldout(position, show.boolValue, property.name + " (" + property.FindPropertyRelative("EffectArray").arraySize + ")");

    }

}