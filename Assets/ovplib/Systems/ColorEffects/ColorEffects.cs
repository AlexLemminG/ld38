﻿
using System;
using UnityEngine;

[System.Serializable]
public class ColorEffects
{
    public bool Show;
    public bool Enabled = true;
    public Effect[] EffectArray;

    public void Evaluate(Color[] input)
    {
        if (!Enabled || EffectArray == null || EffectArray.Length < 1) return;
        foreach (Effect effect in EffectArray)
            effect.Evaluate(input);
    }

    [System.Serializable]
    public class Effect
    {
        public bool Enabled = true;
        public Type Type;
        public int[] Ints = new int[1];
        public float[] Floats = new float[7];

        public void FixData()
        {
            if (Ints == null || Ints.Length < 1)
                Ints = new int[1];
            if (Floats == null || Floats.Length < 7)
                Floats = new float[7];
        }

        public void Evaluate(Color[] input)
        {
            if (!Enabled) return;
            switch (Type)
            {
                case Type.Levels:
                {
                    EvaluateLevels(input);
                    break;
                }
                case Type.Values:
                {
                    EvaluateValues(input);
                    break;
                }
                case Type.Gradient:
                {
                    EvaluateGradient(input);
                    break;
                }
            }
        }

        public void EvaluateLevels(Color[] input)
        {
            float min = Floats[0];
            float max = Floats[1];
            float range = max - min;

            const float tolerance = 0.0000001f;
            if (Math.Abs(min) > tolerance || Math.Abs(max - 1) > tolerance)
            {
                if (Ints[0] == 3)//RGB
                {
                    for (int i = 0; i < input.Length; i++)
                    {
                        Color c = input[i];
                        c.r = c.r * range + min;
                        c.g = c.g * range + min;
                        c.b = c.b * range + min;
                        input[i] = c;
                    }
                }
                else
                {
                    if (Ints[0] == 0)
                        for (int i = 0; i < input.Length; i++)
                            input[i].r = input[i].r * range + min;
                    else if (Ints[0] == 1)
                        for (int i = 0; i < input.Length; i++)
                            input[i].g = input[i].g * range + min;
                    else if (Ints[0] == 2)
                        for (int i = 0; i < input.Length; i++)
                            input[i].b = input[i].b * range + min;
                }
            }
        }
        public void EvaluateValues(Color[] input)
        {
            float brightness = Floats[0];
            float contrast = Floats[1];
            float saturation = Floats[2];
            float gamma = Floats[3];

            const float tolerance = 0.0000001f;

            if (Math.Abs(brightness) > tolerance || Math.Abs(contrast - 1f) > tolerance)
            {
                //a + (b-a)*t
                //0.5 + b*t - 0.5*t
                float bcAdd = brightness + 0.5f - 0.5f * contrast;
                for (int i = 0; i < input.Length; i++)
                {
                    Color c = input[i];
                    c.r = c.r * contrast + bcAdd;
                    c.g = c.g * contrast + bcAdd;
                    c.b = c.b * contrast + bcAdd;
                    input[i] = c;
                }
            }

            if (Math.Abs(saturation - 1f) > tolerance)
            {
                //l + (b-l)*t
                //l - l*t + b*t
                float lMul = (1f - saturation) / 3f;
                for (int i = 0; i < input.Length; i++)
                {
                    Color c = input[i];
                    float l = (c.r + c.g + c.b) * lMul;
                    c.r = l + c.r * saturation;
                    c.g = l + c.g * saturation;
                    c.b = l + c.b * saturation;
                    input[i] = c;
                }
            }

            if (Math.Abs(gamma - 1f) > tolerance)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    Color c = input[i];
                    c.r = Mathf.Pow(c.r, gamma);
                    c.g = Mathf.Pow(c.g, gamma);
                    c.b = Mathf.Pow(c.b, gamma);
                    input[i] = c;
                }
            }
        }
        public void EvaluateGradient(Color[] input)
        {
            float whiteR = Floats[0];
            float whiteG = Floats[1];
            float whiteB = Floats[2];
            float blackR = Floats[3];
            float blackG = Floats[4];
            float blackB = Floats[5];
            float opacity = Floats[6];


            //a + (b-a)*t

            //b + (w-b)*l
            //c + ((b + (w-b)*l) - c)*o //4+ 2*
            //c + b*o + (w-b)*o*l - c*o
            //c*(1-o) + add + cl*l //2+ 2*

            const float tolerance = 0.0000001f;
            if (Math.Abs(opacity) > tolerance)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    Color c = input[i];
                    float l = (c.r + c.g + c.b) * 0.33333333f;
                    c.r = Lerp(c.r, Lerp(blackR, whiteR, l), opacity);
                    c.g = Lerp(c.g, Lerp(blackG, whiteG, l), opacity);
                    c.b = Lerp(c.b, Lerp(blackB, whiteB, l), opacity);
                    input[i] = c;
                }
            }



        }

        float Lerp(float a, float b, float t)
        {
            return a + (b - a) * t;
        }
    }

    public enum Type
    {
        Levels,//Channel, MinMax
        Values,//brightness, contrast, saturation, gamma
        Gradient,//ColorBlack, ColorWhite, Opacity
    }

    public enum Channel
    {
        Red = 0,
        Green = 1,
        Blue = 2,
        RGB = 3
    }
}