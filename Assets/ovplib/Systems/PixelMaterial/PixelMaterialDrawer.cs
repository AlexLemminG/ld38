using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(PixelMaterial))]
public class PixelMaterialDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        //return EditorGUIUtility.singleLineHeight;
        var colors = property.FindPropertyRelative("Colors");
        if (colors == null) return EditorGUIUtility.singleLineHeight;
        var fold = property.FindPropertyRelative("Fold");
        if (!fold.boolValue) return EditorGUIUtility.singleLineHeight;
        return property.FindPropertyRelative("Colors").arraySize * EditorGUIUtility.singleLineHeight * 3 + EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var colors = property.FindPropertyRelative("Colors");
        var fold = property.FindPropertyRelative("Fold");

        Rect itemRect = position;
        float singleLineHeight = EditorGUIUtility.singleLineHeight;
        itemRect.height = singleLineHeight * 3;
        Rect lineRect = position;
        lineRect.height = singleLineHeight;

        EditorGUI.BeginChangeCheck();
        fold.boolValue = EditorGUI.Foldout(lineRect, fold.boolValue, property.name);
        lineRect.y += singleLineHeight;
        lineRect.x += 16;
        lineRect.width -= 16;

        if (fold.boolValue)
            if (colors != null)
            {
                int length = colors.arraySize;

                for (int i = 0; i < length; i++)
                {
                    var colorItem = colors.GetArrayElementAtIndex(i);
                    var color = colorItem.FindPropertyRelative("OriginalColor");
                    var newColor = colorItem.FindPropertyRelative("NewColor");
                    var roughness = colorItem.FindPropertyRelative("Roughness");
                    var gloss = colorItem.FindPropertyRelative("Gloss");

                    Rect boxRect = lineRect;
                    boxRect.height = singleLineHeight * 3;
                    boxRect.x += 16;
                    GUI.Box(boxRect, GUIContent.none);


                    Rect rect = lineRect;
                    rect.width = 80;
                    EditorGUI.ColorField(rect, GUIContent.none, color.colorValue, false, false, false, null);
                    rect.x += rect.width;
                    rect.width = 24;
                    if (GUI.Button(rect, "R")) newColor.colorValue = color.colorValue;
                    rect.x += rect.width;
                    rect.width = lineRect.width - rect.x;
                    newColor.colorValue = EditorGUI.ColorField(rect, GUIContent.none, newColor.colorValue, true, false, false, null);

                    //EditorGUI.PropertyField(lineRect, color);
                    lineRect.y += singleLineHeight;
                    EditorGUI.PropertyField(lineRect, roughness);
                    lineRect.y += singleLineHeight;
                    EditorGUI.PropertyField(lineRect, gloss);
                    lineRect.y += singleLineHeight;
                }
            }
        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();
    }

}