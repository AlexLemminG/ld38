﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PixelMaterial
{
    public Texture2D Texture;
    public ColorItem[] Colors = new ColorItem[0];
    public byte[] Indices;
    public bool Fold;
    
    public void EvaluateValues(Color[] input)
    {
        if (Indices.Length == 0) return;
        float[] roughness = new float[Colors.Length];
        float[] gloss = new float[Colors.Length];
        for (int i = 0; i < Colors.Length; i++)
        {
            roughness[i] = Colors[i].Roughness / 256f;
            gloss[i] = Colors[i].Gloss / 256f;
        }
        for (int i = 0; i < input.Length; i++)
        {
            int id = Indices[i];
            input[i].g = roughness[id];
            input[i].b = gloss[id];
        }
    }

    public void EvaluateColors(Color[] input)
    {
        for (int i = 0; i < input.Length; i++)
            input[i] = Colors[Indices[i]].NewColor;
    }

    public void LoadTexture(Texture2D texture)
    {
        if (Texture == texture) return;
        if (texture == null)
        {
            Colors = new ColorItem[0];
            Indices = new byte[0];
            return;
        }
        Texture = texture;
        Color32[] colors = texture.GetPixels32();
        Dictionary<int, ColorItem> dict = new Dictionary<int, ColorItem>();

        for (int i = 0; i < colors.Length; i++)
        {
            int index = ColorToIndex(colors[i]);
            ColorItem ci;
            if (!dict.TryGetValue(index, out ci))
                dict.Add(index, ci = new ColorItem());
            ci.OriginalColor = ci.NewColor = colors[i];
        }
        if (dict.Count > 255)
        {
            Colors = new ColorItem[0];
            Indices = new byte[colors.Length];
            return;
        }
        List<ColorItem> list = new List<ColorItem>(dict.Values);

        list.Sort((ci0, ci1) =>
        {
            int i0 = ci0.OriginalColor.r + ci0.OriginalColor.g + ci0.OriginalColor.b;
            int i1 = ci1.OriginalColor.r + ci1.OriginalColor.g + ci1.OriginalColor.b;
            if (i0 == i1) return 0;
            return i0 > i1 ? -1 : 1;
        });


        Colors = list.ToArray();
        for (byte i = 0; i < Colors.Length; i++)
            Colors[i].Index = i;

        Indices = new byte[colors.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            int index = ColorToIndex(colors[i]);
            ColorItem ci = dict[index];
            Indices[i] = ci.Index;
        }
    }
    
    static int ColorToIndex(Color32 color)
    {
        return color.r | (color.g << 8) | (color.b << 16);
    }

    [System.Serializable]
    public class ColorItem
    {
        public Color32 OriginalColor;
        public Color32 NewColor;
        [Range(0, 255)]
        public byte Roughness;
        [Range(0, 255)]
        public byte Gloss;
        public byte Index;


    }
}