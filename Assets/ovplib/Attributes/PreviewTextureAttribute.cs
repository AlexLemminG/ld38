﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PreviewTextureAttribute : PropertyAttribute
{
    public float GuiSize = 1f;
    public float MaxTexScale = 0f;
    public bool ExactGuiSize = false;

    public PreviewTextureAttribute(float guiScale, float maxTexScale)
    {
        GuiSize = guiScale;
        MaxTexScale = maxTexScale;
        ExactGuiSize = false;
    }
    public PreviewTextureAttribute(float sizeInPixels)
    {
        GuiSize = sizeInPixels;
        ExactGuiSize = true;
    }
    public PreviewTextureAttribute()
    {

    }
}


#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(PreviewTextureAttribute))]
public class PreviewTextureDrawer : PropertyDrawer
{  
    GUIStyle _style;
    Material _material;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = 16;
        if (property.propertyType == SerializedPropertyType.ObjectReference)
            DrawTexture(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + GetTexHeigth(property);
    }


    void DrawTexture(Rect position, SerializedProperty property, GUIContent label)
    {
        Texture2D texture = (Texture2D)EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Texture2D), true);
        property.objectReferenceValue = texture;
        if (texture == null) return;

        PreviewTextureAttribute texAttribute = (PreviewTextureAttribute)attribute;

        float posWidth = EditorGUIUtility.currentViewWidth - 19;

        if (texAttribute.ExactGuiSize)
        {
            float scale = texAttribute.GuiSize/texture.height;
            position = new Rect(0, position.y + 16, texture.width * scale, texAttribute.GuiSize);
            position.x = posWidth - position.width;
        }
        else
        {
            float xOffset = (1f - texAttribute.GuiSize);
            float width = posWidth*texAttribute.GuiSize;
            float scale = width/texture.width;
            if (texAttribute.MaxTexScale != 0f)
                scale = Mathf.Min(scale, texAttribute.MaxTexScale);

            position = new Rect(posWidth * xOffset, position.y + 16, texture.width * scale, texture.height * scale);
        }

        if (_style == null) _style = new GUIStyle { imagePosition = ImagePosition.ImageOnly };
        _style.normal.background = texture;
        GUI.Label(position, "", _style);
    }

    public float GetTexHeigth(SerializedProperty property)
    {
        PreviewTextureAttribute texAttribute = (PreviewTextureAttribute) attribute;
        if (texAttribute.ExactGuiSize)
            return texAttribute.GuiSize;

        Texture2D texture = (Texture2D) property.objectReferenceValue;
        if (texture == null) return 0f;

        float posWidth = EditorGUIUtility.currentViewWidth - 19;

        float width = posWidth*texAttribute.GuiSize;
        float scale = width/texture.width;
        if (texAttribute.MaxTexScale != 0f)
            scale = Mathf.Min(scale, texAttribute.MaxTexScale);
        return texture.height*scale;
    }

    Material GetMaterial()
    {
        if (_material == null)
        {
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            _material = new Material(shader);
            _material.hideFlags = HideFlags.HideAndDontSave;
        }

        _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

        _material.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        _material.SetInt("_ZWrite", 0);
        _material.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
        return _material;
    }

}

#endif