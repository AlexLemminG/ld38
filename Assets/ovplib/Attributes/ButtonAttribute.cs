﻿using System;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
public class ButtonAttribute : PropertyAttribute
{
    public string[] ButtonNames;
    public string[] FuncNames;
    public float Inline;

    /// <param name="names">buttonName(0), funcName(0) ... buttonName(n), funcName(n)</param>
    public ButtonAttribute(params string[] names)
    {
        int length = names.Length/2;
        ButtonNames = new string[length];
        FuncNames = new string[length];
        if (names.Length%2 == 1) Debug.LogError("ObserveAttribute(params count must be even)");
        for (int i = 0; i < names.Length; i++)
            (i%2 == 0 ? ButtonNames : FuncNames)[i/2] = names[i];
    }

    /// <param name="inline">buttons line width: positive:right, negative:left, zero: off</param>
    /// <param name="names">buttonName(0), funcName(0) ... buttonName(n), funcName(n)</param>
    public ButtonAttribute(float inline = 0f, params string[] names)
        : this(names)
    {
        Inline = inline;
    }
}


#if UNITY_EDITOR
[CustomPropertyDrawer(typeof (ButtonAttribute))]
public class ButtonDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect propertyPosition = position;
        Rect buttonsPosition = position;
        if (IsInline)
        {
            bool right = Attribute.Inline > 0f;
            if (right)
            {
                buttonsPosition.width = Attribute.Inline;
                buttonsPosition.x = position.width - buttonsPosition.width;
                propertyPosition.width = buttonsPosition.x;
                buttonsPosition.x += 16;
            }
            else
            {
                buttonsPosition.width = -Attribute.Inline;
                propertyPosition.x = buttonsPosition.width;
                propertyPosition.width = position.width - buttonsPosition.width;
                propertyPosition.x += 16;
            }
            buttonsPosition.width -= 8;
            propertyPosition.width -= 8;
        }
        else
        {
            buttonsPosition.height = 16;
            propertyPosition.height = 16;
            buttonsPosition.y += 16;
        }

        EditorGUI.PropertyField(propertyPosition, property, label);

        buttonsPosition.width /= Attribute.ButtonNames.Length;
        for (int i = 0; i < Attribute.ButtonNames.Length; i++)
        {
            if (Attribute.ButtonNames[i] != null)
                if (GUI.Button(buttonsPosition, Attribute.ButtonNames[i]))
                    Invoke(property.serializedObject.targetObject, Attribute.FuncNames[i]);
            buttonsPosition.x += buttonsPosition.width;
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + (IsInline ? 0f : 16f);
    }

    private static void Invoke(object obj, string name)
    {
        const BindingFlags flags =
            BindingFlags.NonPublic |
            BindingFlags.Instance |
            BindingFlags.Public |
            BindingFlags.Static |
            BindingFlags.IgnoreCase |
            BindingFlags.IgnoreReturn;
        MethodInfo method = obj.GetType().GetMethod(name, flags);
        if (method != null)
            method.Invoke(obj, new object[] {});
    }

    private bool IsInline
    {
        get { return Attribute.Inline != 0f; }
    }

    private ButtonAttribute Attribute
    {
        get { return (ButtonAttribute) attribute; }
    }
}
#endif