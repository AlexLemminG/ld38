﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Text.RegularExpressions;
#endif


/// <summary>
/// UseCurve 50 sw 230 Curve; Curve 180; Value 80
/// UseGradient 50 sw 120 Gradient !Color; Gradient 260; Color 190
/// </summary>
public class UberDrawAttribute : PropertyAttribute
{

    public readonly string Data;
    public readonly bool WithLable;
    public readonly int Lines;
    public readonly float LableWidth;


    /// <summary>(varName) (size); sw - swith, sl - slider, \n - newLine</summary>
    /// <param name="data">sw - (varName) (size) sw (offSize) [!]var(0) ... [!]var(n);      sl - (varName) (size) sl [min=0] [max=1]; </param>
    public UberDrawAttribute(string data, bool withLable = true, float lableWidth = -1f)
    {
        Data = data;
        WithLable = withLable;
        Lines = data.Split('\n').Length;
        LableWidth = lableWidth;
    }
}


#if UNITY_EDITOR
//[UberDraw("UseCurve 50 sw 110 Curve !Value.2 Value; Curve 180; Value 80; Value.2 200 Sl", true, 80)]
//[UberDraw("UseGradient 50 sw 120 Gradient !Color; Gradient 260; Color 190", true, 80)]

[CustomPropertyDrawer(typeof(UberDrawAttribute))]
public class UberDraw : PropertyDrawer
{
    static Dictionary<string, Val[]> _valsesDict = new Dictionary<string, Val[]>();
    const int Height = 16;
    static readonly GUIStyle ErrorStyle = GetStyle(Color.yellow, new Color(0.7f, 0, 0, 1), new Color(0, 0, 0, 0.5f));

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        UberDrawAttribute ueAttribute = (UberDrawAttribute)attribute;
        return ueAttribute.Lines * Height;
    }

    public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
    {
        UberDrawAttribute ueAttribute = (UberDrawAttribute)attribute;
        EditorGUI.BeginProperty(pos, label, property);
        if (ueAttribute.WithLable)
        {
            pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), label);
            if (ueAttribute.LableWidth > 0) pos.x = ueAttribute.LableWidth;
        }

        Val[] vals = GetValues(ueAttribute);
        int x = (int)pos.x;
        pos.height = Height;

        foreach (Val val in vals)
        {
            if (val.NewLine)
            {
                x = (int)pos.x;
                pos.y += Height;
            }
            if (!val.Enabled) continue;

            SerializedProperty valProperty = property.FindPropertyRelative(val.Name);
            if (valProperty == null)
            {
                EditorGUI.LabelField(GetRect(pos, ref x, val.Width), "Error", ErrorStyle);
                continue;
            }
            switch (val.Type)
            {
                case Val.ValType.Normal:
                    EditorGUI.PropertyField(GetRect(pos, ref x, val.Width), valProperty, GUIContent.none);
                    break;
                case Val.ValType.Slider:
                    Slider(GetRect(pos, ref x, val.Width), valProperty, val.DataF[0], val.DataF[1]);
                    break;
                case Val.ValType.Switch:
                    Switch(pos, ref x, valProperty, val);
                    break;
            }



        }

        EditorGUI.EndProperty();
    }

    static void Slider(Rect rect, SerializedProperty property, float min, float max)
    {
        switch (property.propertyType)
        {
            case SerializedPropertyType.Float:
                {
                    EditorGUI.Slider(rect, property, min, max, GUIContent.none);
                }
                break;
            case SerializedPropertyType.Integer:
                {
                    EditorGUI.IntSlider(rect, property, (int)min, (int)max, GUIContent.none);
                }
                break;
            case SerializedPropertyType.Vector2:
                {
                    Vector2 value = property.vector2Value;
                    EditorGUI.MinMaxSlider(rect, ref value.x, ref value.y, min, max);
                    property.vector2Value = value;
                }
                break;
        }
    }

    static void Switch(Rect source, ref int x, SerializedProperty property, Val val)
    {
        bool on = property.boolValue;
        int width = on ? val.Width : (int)val.DataF[0];

        for (int i = 0; i < val.Controlled.Length; i++)
        {
            Val cval = val.Controlled[i];
            cval.Enabled = on ^ val.ControlledInvert[i];
        }
        if (on) EditorGUI.PropertyField(GetRect(source, ref x, width), property, GUIContent.none);
        else property.boolValue = EditorGUI.ToggleLeft(GetRect(source, ref x, width), property.name, on);
    }
    static Rect GetRect(Rect source, ref int x, int width)
    {
        source.xMin = x;
        source.width = width;
        x += width;
        return source;
    }
    static Val[] GetValues(UberDrawAttribute ueAttribute)
    {
        string data = ueAttribute.Data;

        Val[] values;
        if (_valsesDict.TryGetValue(data, out values))
            return values;
        values = Parse(data);
        _valsesDict.Add(data, values);
        return values;
    }
    static Val[] Parse(string data)
    {
        Regex regex = new Regex(@"\s+");

        Dictionary<string, Val> valDict = new Dictionary<string, Val>();
        string[] lines = data.Split('\n');
        bool newLine = false;
        foreach (string l in lines)
        {
            string[] columns = l.Split(';');
            foreach (string c in columns)
            {
                string[] vals = regex.Replace(c, " ").Trim(' ').Split(' ');
                if (vals.Length == 1) break;
                int width;
                Val val;
                if (!int.TryParse(vals[1], out width)) val = new Val(vals[0], 80, newLine);
                else if (vals.Length == 2) val = new Val(vals[0], width, newLine);
                else val = new Val(vals[0], width, vals, newLine);

                valDict.Add(vals[0], val);
                newLine = false;
            }
            newLine = true;
        }
        Val[] valArray = new Val[valDict.Count];
        valDict.Values.CopyTo(valArray, 0);

        foreach (Val val in valArray)
            if (val.Type == Val.ValType.Switch)
                val.InitializeSwitch(valDict);

        return valArray;
    }
    static GUIStyle GetStyle(Color foreGround, Color backGround, Color backGround2)
    {
        GUIStyle style = new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Scene).label);
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = foreGround;
        {
            Texture2D tex = new Texture2D(3, 3, TextureFormat.ARGB32, false);
            tex.filterMode = FilterMode.Point;
            tex.SetPixels(new Color[]
                {
                    backGround2, backGround2, backGround2,
                    backGround2, backGround, backGround2,
                    backGround2, backGround2, backGround2
                });
            tex.Apply();
            style.normal.background = tex;
        }
        style.border = new RectOffset(1, 1, 1, 1);
        return style;
    }



    class Val
    {
        public string Name;
        public int Width;
        public bool NewLine;
        public ValType Type;
        public float[] DataF;
        public string[] DataS;
        public bool Enabled = true;
        public Val[] Controlled;
        public bool[] ControlledInvert;

        public Val(string name, int width, string[] vals, bool newLine = false)
        {
            Name = GetName(name);
            Width = width;
            NewLine = newLine;

            string type = vals[2];
            switch (type)
            {
                case "Sl":
                case "sl":
                    Type = ValType.Slider;
                    DataF = vals.Length == 5 ? new[] { float.Parse(vals[3]), float.Parse(vals[4]) } : new[] { 0f, 1f };
                    break;
                case "Sw":
                case "sw":
                    Type = ValType.Switch;
                    if (vals.Length > 4)
                    {
                        DataS = vals;
                        DataF = new[] { float.Parse(vals[3]) };
                    }
                    else Type = ValType.Normal;
                    break;

                default:
                    Type = ValType.Normal;
                    break;
            }
        }
        public Val(string name, int width, bool newLine = false)
        {
            Name = GetName(name);
            Width = width;
            NewLine = newLine;
            Type = ValType.Normal;
        }

        public string GetName(string val)
        {
            return val.Split('.')[0];
        }

        public void InitializeSwitch(Dictionary<string, Val> valDict)
        {
            int start = 4;
            Controlled = new Val[DataS.Length - start];
            ControlledInvert = new bool[Controlled.Length];

            for (int i = start; i < DataS.Length; i++)
            {
                int i2 = i - start;
                string s = DataS[i];
                bool invert = s[0] == '!';
                ControlledInvert[i2] = invert;
                if (invert) s = s.Substring(1);
                Controlled[i2] = valDict[s];
            }
        }

        public enum ValType
        {
            Broken,
            Normal,
            Slider,
            Switch
        }
    }
}


#endif
