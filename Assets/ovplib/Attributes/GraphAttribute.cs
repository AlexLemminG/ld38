﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
#endif

[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
public class GraphAttribute : PropertyAttribute
{
    public int Height;
    public string FuncName;
    public Color Graph = Color.white;
    public Color Background = Color.black;
    public int LineWidth;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="funcName"></param>
    /// <param name="height"></param>
    /// <param name="graphHex">#rrggbbaa</param>
    /// <param name="backgroundHex">#rrggbbaa</param>
    public GraphAttribute(string funcName, int height, string graphHex = "#ffffffff", string backgroundHex = "#000000ff",int lineWidth = 1)
    {
        FuncName = funcName;
        Height = height;
        Color color;
        if (ColorUtility.TryParseHtmlString(graphHex, out color))
            Graph = color;
        if (ColorUtility.TryParseHtmlString(backgroundHex, out color))
            Background = color;
        LineWidth = Math.Max(lineWidth, 1);
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof (GraphAttribute))]
public class GraphDrawer : PropertyDrawer
{
    private Texture2D _tex;
    private GUIStyle _style;
    private Color32[] _colors;
    private MethodInfo _method;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = 16;
        DrawTexture(position, property, label);
    }

    private void DrawTexture(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, label);

        int width = (int) EditorGUIUtility.currentViewWidth - 19;
        int height = Attribute.Height;
        if (_tex == null || _tex.width != width || _tex.height != height)
        {
            _tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
            _tex.filterMode = FilterMode.Point;
            _colors = new Color32[width*height];
        }

        object obj = property.serializedObject.targetObject;
        MethodInfo method = GetMethod(obj, Attribute.FuncName);
        if (method == null || method.GetParameters().Length != 1) return;
        MapUtils.GetGraph(width, height, _colors, Attribute.Graph, Attribute.Background, Attribute.LineWidth,
                          x => (float) method.Invoke(obj, new object[] {x}));
        _tex.SetPixels32(_colors);
        _tex.Apply(false);


        position.y += base.GetPropertyHeight(property, label);
        position.height = height;
        if (_style == null) _style = new GUIStyle {imagePosition = ImagePosition.ImageOnly};
        _style.normal.background = _tex;
        GUI.Label(position, "", _style);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + ((GraphAttribute) attribute).Height;
    }

    private static MethodInfo GetMethod(object obj, string name)
    {
        const BindingFlags flags =
            BindingFlags.NonPublic |
            BindingFlags.Instance |
            BindingFlags.Public |
            BindingFlags.Static |
            BindingFlags.IgnoreCase |
            BindingFlags.IgnoreReturn;
        return obj.GetType().GetMethod(name, flags);
    }

    private GraphAttribute Attribute
    {
        get { return (GraphAttribute) attribute; }
    }
}
#endif