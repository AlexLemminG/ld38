﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MinMaxVec2Attribute : PropertyAttribute
{
    public readonly float Min;
    public readonly float Max;

    public readonly float Round;
    public readonly float InvRound;
    public readonly bool UseRound;

    public MinMaxVec2Attribute(float min, float max, float round = -1)
    {
        Min = min;
        Max = max;

        UseRound = round > 0;
        Round = 1 / round;
        InvRound = round;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(MinMaxVec2Attribute))]
public class MinMaxVec2Drawer : PropertyDrawer
{
    static GUIStyle _fieldStyle;
    public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
    {
        const int fieldW = 60;
        const int sliderSubstrW = fieldW * 2;
        if (_fieldStyle == null) _fieldStyle = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label;

        MinMaxVec2Attribute minMaxVec2 = (MinMaxVec2Attribute)attribute;

        EditorGUI.BeginProperty(pos, label, property);
        pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), label);

        int x = (int)pos.x;
        float xVal = property.FindPropertyRelative("x").floatValue;
        float yVal = property.FindPropertyRelative("y").floatValue;
        EditorGUI.MinMaxSlider(GetRect(pos, ref x, (int)pos.width - sliderSubstrW), ref xVal, ref yVal, minMaxVec2.Min, minMaxVec2.Max);

        xVal = Mathf.Clamp(xVal, minMaxVec2.Min, minMaxVec2.Max);
        yVal = Mathf.Clamp(yVal, minMaxVec2.Min, minMaxVec2.Max);

        if (minMaxVec2.UseRound)
        {
            xVal = Mathf.Round(xVal * minMaxVec2.Round) * minMaxVec2.InvRound;
            yVal = Mathf.Round(yVal * minMaxVec2.Round) * minMaxVec2.InvRound;
        }

        xVal = EditorGUI.FloatField(GetRect(pos, ref x, fieldW), xVal, _fieldStyle);
        yVal = EditorGUI.FloatField(GetRect(pos, ref x, fieldW), yVal, _fieldStyle);

        property.FindPropertyRelative("x").floatValue = xVal;
        property.FindPropertyRelative("y").floatValue = yVal;

        EditorGUI.EndProperty();
    }


    static Rect GetRect(Rect source, ref int x, int width)
    {
        source.xMin = x;
        source.width = width;
        x += width;
        return source;
    }

}
#endif