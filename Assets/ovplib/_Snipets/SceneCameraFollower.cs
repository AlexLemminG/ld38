using UnityEngine;

public class SceneCameraFollower : MonoBehaviour
{
    public bool CopyPosition = true;
    public bool CopyRotation = true;
    public bool CopyFov;

    void Update()
    {
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (!enabled) return;
        //if (GetComponent<Camera>() == null) return;
        if (UnityEditor.SceneView.currentDrawingSceneView == null || UnityEditor.SceneView.currentDrawingSceneView.camera == null) return;
        Camera targetCamera = UnityEditor.SceneView.currentDrawingSceneView.camera;
        Transform targetTransform = targetCamera.transform;

        if (CopyPosition) transform.position = targetTransform.position;
        if (CopyRotation) transform.rotation = targetTransform.rotation;
        if (GetComponent<Camera>() == null) CopyFov = false;
        if (CopyFov) GetComponent<Camera>().fieldOfView = targetCamera.fieldOfView;
    }
#endif



}