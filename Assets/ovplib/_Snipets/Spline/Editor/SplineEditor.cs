﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Spline))]
public class SplineEditor : Editor
{
     Spline _spline;
    static Spline _selected;
    State _state = State.Normal;


    void OnEnable()
    {
        _spline = (Spline)target;

    }

    public override void OnInspectorGUI()
    {
        EditorGUIUtility.labelWidth = 50;
        EditorGUIUtility.fieldWidth = 30;
        EditorGUILayout.BeginHorizontal();

        if (_selected == _spline) GUI.enabled = false;
        if (GUILayout.Button("Select"))
        {
            _selected = _spline;
            SceneView.RepaintAll();
            Repaint();
        }
        GUI.enabled = true;

        _spline.Parts = Mathf.Max(EditorGUILayout.IntField("Parts", _spline.Parts, GUILayout.Width(85), GUILayout.ExpandWidth(false)), 1);
        _spline.LineColor = EditorGUILayout.ColorField(_spline.LineColor, GUILayout.Width(50), GUILayout.ExpandWidth(false));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        _spline.UseOptimization = EditorGUILayout.Toggle("Optimize", _spline.UseOptimization, GUILayout.Width(75), GUILayout.ExpandWidth(false));
        _spline.Error = EditorGUILayout.Slider("Error", _spline.Error, 0.01f, 0.3f);
        EditorGUILayout.EndHorizontal();
 
        EditorGUIUtility.labelWidth = 120;
        EditorGUIUtility.fieldWidth = 30;

        if (GUI.changed) Generate();
    }

    void AddPoint(Spline spline, int i)
    {
        int i0 = i - 1;
        Vector3 position;
        Vector3 normal;
        if (i < 1)
        {
            if (i < 0)
            {
                position = spline.Positins[0] - spline.Normals[0] * 0.2f;
                normal = spline.Normals[0];
                i = 0;
            }
            else
            {
                int temp = spline.Positins.Length - 1;
                position = spline.Positins[temp] + spline.Normals[temp] * 0.2f;
                normal = spline.Normals[temp];
                i = spline.Positins.Length;
            }
        }
        else
        {
            position = Spline.Hermite(0.5f, spline.Positins[i0], spline.Normals[i0], spline.Normals[i], spline.Positins[i]);
            Vector3 posA = Spline.Hermite(0.4f, spline.Positins[i0], spline.Normals[i0], spline.Normals[i], spline.Positins[i]);
            Vector3 posB = Spline.Hermite(0.6f, spline.Positins[i0], spline.Normals[i0], spline.Normals[i], spline.Positins[i]);
            normal = (posB - posA) * 4f;
        }

        ArrayUtility.Insert(ref spline.Positins, i, position);
        ArrayUtility.Insert(ref spline.Normals, i, normal);
        Generate();
    }
    void DeletePoint(Spline spline, int i)
    {
        ArrayUtility.RemoveAt(ref spline.Positins, i);
        ArrayUtility.RemoveAt(ref spline.Normals, i);
        Generate();
    }

    public void OnSceneGUI()
    {
        if (Event.current.control)
        {
            if (Event.current.shift)
                _state = State.DeletePoint;
            else
                _state = State.AddPoint;
        }
        else if (Event.current.alt)
            _state = State.MoveSpline;
        else
            _state = State.Normal;

        if (_selected == _spline)
        {
            if (_state == State.Normal)
            {
                Handles.color = Color.white;
                for (int j = 0; j < _spline.Positins.Length; j++)
                {
                    DrawPoint(ref _spline.Positins[j], ref _spline.Normals[j]);
                }
                for (int j = 1; j < _spline.Positins.Length; j++)
                {
                    Handles.color = _spline.LineColor;
                    int j0 = j - 1;
                    DrawLine(_spline.Positins[j0], _spline.Positins[j], _spline.Normals[j0], _spline.Normals[j],
                             _spline.Parts);
                }
            }
            else if (_state == State.AddPoint)
            {
                for (int j = 1; j < _spline.Positins.Length; j++)
                {
                    Handles.color = _spline.LineColor;
                    int j0 = j - 1;
                    DrawLine(_spline.Positins[j0], _spline.Positins[j], _spline.Normals[j0], _spline.Normals[j],
                             _spline.Parts);
                }
                Handles.color = Color.black;
                for (int j = 0; j < _spline.Positins.Length; j++)
                {
                    Handles.DotCap(0, _spline.Positins[j], Quaternion.identity, 0.2f);
                }

                for (int j = -1; j < _spline.Positins.Length; j++)
                {
                    Handles.color = _spline.LineColor;
                    int j0 = j - 1;
                    Vector3 pos;
                    if (j < 1)
                    {
                        if (j < 0)
                        {
                            pos = _spline.Positins[0] - _spline.Normals[0]*0.2f;
                            Handles.DrawLine(_spline.Positins[0], pos);
                        }
                        else
                        {
                            int temp = _spline.Positins.Length - 1;
                            pos = _spline.Positins[temp] + _spline.Normals[temp]*0.2f;
                            Handles.DrawLine(_spline.Positins[temp], pos);
                        }
                    }
                    else
                        pos = Spline.Hermite(0.5f, _spline.Positins[j0], _spline.Normals[j0], _spline.Normals[j],
                                             _spline.Positins[j]);
                    Handles.color = new Color(0.5f, 1f, 0.7f, 1f);
                    if (Handles.Button(pos, Quaternion.identity, 0.3f, 0.3f, Handles.DotCap))
                    {
                        AddPoint(_spline, j);
                        _state = State.Normal;
                    }
                }

            }
            else if (_state == State.DeletePoint)
            {
                for (int j = 1; j < _spline.Positins.Length; j++)
                {
                    Handles.color = _spline.LineColor;
                    int j0 = j - 1;
                    DrawLine(_spline.Positins[j0], _spline.Positins[j], _spline.Normals[j0], _spline.Normals[j],
                             _spline.Parts);
                }
                Handles.color = new Color(1f, 0.6f, 0.5f, 1f);
                for (int j = 0; j < _spline.Positins.Length; j++)
                {
                    if (Handles.Button(_spline.Positins[j], Quaternion.identity, 0.3f, 0.3f, Handles.DotCap))
                    {
                        DeletePoint(_spline, j);
                        _state = State.Normal;
                    }
                }
            }
            else if (_state == State.MoveSpline)
            {
                Handles.color = Color.white;

                Vector3 pos = _spline.Positins[0];
                Vector3 newPos = Handles.PositionHandle(pos, Quaternion.identity);
                if (pos != newPos)
                {
                    Vector3 delta = newPos - pos;
                    for (int i = 0; i < _spline.Positins.Length; i++)
                    {
                        _spline.Positins[i] += delta;
                    }
                }

                for (int j = 1; j < _spline.Positins.Length; j++)
                {
                    Handles.color = _spline.LineColor;
                    int j0 = j - 1;
                    DrawLine(_spline.Positins[j0], _spline.Positins[j], _spline.Normals[j0], _spline.Normals[j],
                             _spline.Parts);
                }
            }
        }
        else
        {
            for (int j = 1; j < _spline.Positins.Length; j++)
            {
                Handles.color = _spline.LineColor;
                int j0 = j - 1;
                DrawLine(_spline.Positins[j0], _spline.Positins[j], _spline.Normals[j0], _spline.Normals[j],
                         _spline.Parts);
            }
        }
       
        const float width = 220;
        const float height = 80;

        Handles.BeginGUI();
        Color savedColor = GUI.backgroundColor;
        Color backgroundColor = _selected == null ? Color.white : _selected.LineColor;
        GUI.backgroundColor = Color.Lerp(backgroundColor, new Color(1, 1, 1, 0), 0.8f);
        GUILayout.BeginArea(new Rect(Screen.width - width - 20, Screen.height - height - 60, width, height), GUI.skin.box);

        if (_state == State.Normal)
            GUILayout.Label("Ctrl\t\tAdd Point \nCtrl + Shift\tRemove Point \nAlt\t\tMove Spline\n");
        else if (_state == State.AddPoint)
            GUILayout.Label("Add Point\n\n\n");
        else if (_state == State.DeletePoint)
            GUILayout.Label("Remove Point\n\n\n");
        else if (_state == State.MoveSpline)
            GUILayout.Label("Move Spline\n\n\n");

        GUILayout.Button("asdasd");

        GUILayout.EndArea();
        GUI.backgroundColor = savedColor;
        Handles.EndGUI();

        if (GUI.changed) Generate();
    }
    
    public static void Select(params Spline[] splines)
    {
        if (Event.current.type != EventType.mouseDown || Event.current.button != 0) return;

        const int maxParts = 16;
        float minDist = float.MaxValue;
        Spline selected = null;

        for (int i = 0; i < splines.Length; i++)
        {
            Spline spline = splines[i];
            for (int j = 1; j < spline.Positins.Length; j++)
            {
                int j0 = j - 1;
                Vector3[] points = Spline.GetPositions(spline.Positins[j0], spline.Positins[j], spline.Normals[j0],
                                                    spline.Normals[j],
                                                    spline.Parts > maxParts ? maxParts : spline.Parts);
                float dist = HandleUtility.DistanceToPolyLine(points);
                if (dist < minDist)
                {
                    minDist = dist;
                    selected = spline;
                }
            }
        }
        if (selected != null) _selected = selected;

        Event.current.Use();
    }

    public bool DrawPoint(ref Vector3 position, ref Vector3 normal)
    {
        const float mult = 7f;
        const float invMult = 1f / mult;
        Handles.color = new Color(1f, 1f, 1f, 1f);

        Vector3 newPosition = Handles.PositionHandle(position, Quaternion.LookRotation(normal));
        //Vector3 newPosition = Handles.FreeMoveHandle(position, Quaternion.identity, 0.3f, Vector3.zero, Handles.DotCap);
        bool changed = newPosition != position;
        position = newPosition;

        Vector3 newNorm = normal * invMult;
        Vector3 posA = position + newNorm;
        Vector3 posB = position - newNorm;
        Handles.color = new Color(0.2f, 1f, 0.2f, 0.5f);
        Handles.DrawLine(posA, posB);
        Vector3 posA2 = Handles.FreeMoveHandle(posA, Quaternion.identity, 0.2f, Vector3.zero, Handles.DotCap);
        Vector3 posB2 = Handles.FreeMoveHandle(posB, Quaternion.identity, 0.2f, Vector3.zero, Handles.DotCap);

        if (posA != posA2)
        {
            normal = (posA2 - position) * mult;
            changed = true;
        }
        else if (posB != posB2)
        {
            changed = true;
            normal = (position - posB2) * mult;
        }
        return changed;
    }
    public void DrawLine(Vector3 p0, Vector3 p1, Vector3 n0, Vector3 n1, int count)
    {
        Handles.DrawPolyLine(Spline.GetPositions(p0, p1, n0, n1, count));
    }

    void Generate()
    {
        _spline.Apply();

    }
    
    enum State
    {
        Normal,
        AddPoint,
        DeletePoint,
        MoveSpline
    }
}