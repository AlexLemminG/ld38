using System.Collections.Generic;
using UnityEngine;



public class Spline  : MonoBehaviour
{
    public event System.Action OnChange;
    public Vector3[] Positins = new [] {new Vector3(-1, 0, 0), new Vector3(1, 0, 0)};
    public Vector3[] Normals = new []{new Vector3(1, 0, 0), new Vector3(1, 0, 0)};
#if UNITY_EDITOR
    public Color LineColor = new Color(0, 1, 0, 1);
#endif
    public int Parts = 10;
    public bool UseOptimization;
    public float Error = 0.1f;

    public void Apply()
    {
        if (OnChange != null) OnChange();
    }

    public Vector3[] GetPositionsRaw()
    {
        int pointsCount = Parts + 1;
        int count = pointsCount*(Positins.Length - 1);
        count -= Positins.Length - 2; // remove connections
        Vector3[] points = new Vector3[count];
        float step = 1f/Parts;

        Vector3[] positins = Positins;
        Vector3[] normals = Normals;

        for (int p = 1, i = 0; p < positins.Length; p++)
        {
            Vector3 p0 = positins[p - 1];
            Vector3 p1 = positins[p];
            Vector3 n0 = normals[p - 1];
            Vector3 n1 = normals[p];
            float t = 0;
            for (int j = 0; j < pointsCount; j++, i++)
            {
                points[i] = Hermite(t, p0, n0, n1, p1);
                t += step;
            }
            i--; // remove connections
        }
        return points;
    }

    public Vector3[] GetPositions()
    {
        Vector3[] points = GetPositionsRaw();
        if (UseOptimization) points = Optimize(points, Error);
        return points;
    }

    public Point[] GetPoints()
    {
        return GeneratePoints(GetPositions());
    }

    public Vector3 Evaluate(float t)
    {
        t = Mathf.Clamp(t, 0.0f, 1.0f);
        int length = Positins.Length - 1;
        float t2 = t * length;

        int i0 = (int)t2;
        if (i0 >= length) i0--;
        int i1 = i0 + 1;

        Vector3 p0 = Positins[i0];
        Vector3 p1 = Positins[i1];
        Vector3 n0 = Normals[i0];
        Vector3 n1 = Normals[i1];

        float t3 = t2 - i0;
        return Hermite(t3, p0, n0, n1, p1);
    }

    public static Point[] GeneratePoints(Vector3[] positions)
    {
        int vecCount = positions.Length;
        int parts = vecCount - 1;
        Point[] points = new Point[vecCount];
        Vector3[] forwards = new Vector3[vecCount];
        Vector3[] upwards = new Vector3[vecCount];
        Vector3[] rights = new Vector3[vecCount];

        forwards[0] = (positions[1] - positions[0]).normalized;
        forwards[parts] = (positions[parts] - positions[parts - 1]).normalized;
        for (int v = 1; v < parts; v++)
            forwards[v] = (positions[v + 1] - positions[v - 1]).normalized;



        if (forwards[0].y > 0.9f || forwards[0].y < -0.9f)
            upwards[0] = new Vector3(1, 0, 0);
        else
            upwards[0] = new Vector3(0, 1, 0);
        rights[0] = Vector3.Cross(forwards[0], upwards[0]).normalized;
        upwards[0] = Vector3.Cross(forwards[0], rights[0]).normalized;



        for (int v = 1; v < vecCount; v++)
        {
            rights[v] = Vector3.Cross(forwards[v], -upwards[v - 1]).normalized;
            upwards[v] = Vector3.Cross(forwards[v], rights[v]).normalized;
        }

        for (int v = 0; v < vecCount; v++)
            points[v] = new Point {Position = positions[v], Right = rights[v], Upward = upwards[v]};

        return points;
    }

    public static Vector3[] GetPositions(Vector3 p0, Vector3 p1, Vector3 n0, Vector3 n1, int parts)
    {
        int pointsCount = parts + 1;
        float step = 1f / parts;

        Vector3[] points = new Vector3[pointsCount];
        float t = 0;
        for (int i = 0; i < pointsCount; i++)
        {
            points[i] = Hermite(t, p0, n0, n1, p1);
            t += step;
        }
        return points;
    }
    
    static Vector3[] Optimize(Vector3[] points, float error)
    {
        LinkedList<Vector3> pList = new LinkedList<Vector3>(points);

        for (int pc = 0; pc != pList.Count; error *= 0.5f)
        {
            pc = pList.Count;
            Optimize(pList, error);
        }

        points = new Vector3[pList.Count];
        pList.CopyTo(points, 0);
        return points;
    }
    static void Optimize(LinkedList<Vector3> points, float error)
    {
        for (LinkedListNode<Vector3> node = points.First; node.Next != null && node.Next.Next != null; node = node.Next)
        {
            Vector3 a = node.Value;
            Vector3 b = node.Next.Next.Value;
            Vector3 p = node.Next.Value;

            Vector3 delta = b - a;
            Vector3 point = p - a;

            Vector3 project = Vector3.Project(point, delta);
            float currentError = (point - project).magnitude / delta.magnitude;

            if (currentError < error) points.Remove(node.Next);
        }
    }
    
    public static Vector3 Hermite(float t, Vector3 p0, Vector3 m0, Vector3 m1, Vector3 p1)
    {
        float t2 = t * t;
        float t3 = t2 * t;
        float t32 = 3f * t2;
        float t23 = 2f * t3;
        return (t23 - t32 + 1f) * p0 + (t3 - 2f * t2 + t) * m0 + (t32 - t23) * p1 + (t3 - t2) * m1;
    }
    
    public struct Point
    {
        public Vector3 Position;
        public Vector3 Upward;
        public Vector3 Right;
    }
}