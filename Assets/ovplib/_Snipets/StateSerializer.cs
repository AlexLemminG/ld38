using UnityEngine;

public class StateSerializer
{
    static void Serialize(bool write, ref int state, ref int position, ref bool value)
    {
        if (write)
        {
            if (value) state |= 1 << position;
        }
        else
        {
            value = (state & (1 << position)) != 0;
        }
        position++;
    }
    static void Serialize(bool write, ref int state, ref int position, ref int value, int length)
    {
        int mask = (1 << length) - 1;
        if (write)
        {
            state |= ((value) & mask) << position;
        }
        else
        {
            int temp = state >> position;
            value = temp & mask;
        }
        position += length;
    }
}
