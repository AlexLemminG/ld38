using UnityEngine;
using System;

public delegate void SetPixelDelegate(int x, int y);

public class BaseVectorDraw
{
    static SetPixelDelegate _setPixel;
    public static SetPixelDelegate SetPixelMethod
    {
        set { _setPixel = value; }
    }
    static SetPixelDelegate _setPixelSmooth;
    public static SetPixelDelegate SetPixelSmoothMethod
    {
        set { _setPixel = value; }
    }


    static void Swap<T>(ref T x1, ref T x2)
    {
        T temp = x1;
        x1 = x2;
        x2 = temp;
    }

    static void Line(int x0, int y0, int x1, int y1)
    {
        bool steep = (Math.Abs(y1 - y0) > Math.Abs(x1 - x0));
        if (steep)
        {
            Swap(ref x0, ref y0);
            Swap(ref x1, ref y1);
        }

        if (x0 > x1)
        {
            Swap(ref x0, ref x1);
            Swap(ref y0, ref y1);
        }

        int deltax = x1 - x0;
        int deltay = Math.Abs(y1 - y0);
        int error = -(deltax + 1) / 2;
        int ystep;
        int y = y0;
        if (y0 < y1)
        {
            ystep = 1;
        }
        else
        {
            ystep = -1;
        }

        for (int x = x0; x <= x1; x++)
        {
            if (steep)
            {
                _setPixel(y, x);
            }
            else
            {
                _setPixel(x, y);
            }
            error += deltay;
            if (error >= 0)
            {
                y += ystep;
                error -= deltax;
            }
        }
    }
    static void Line(float x0, float y0, float x1, float y1)
    {
        Line((int)x0, (int)y0, (int)x1, (int)y1);
    }
    static void Line(Vector2 p1, Vector2 p2)
    {
        Line(p1.x, p1.y, p2.x, p2.y);
    }



    public static void DrawWuLine(int x0, int y0, int x1, int y1)
    {
        //���������� ��������� ���������
        int dx = (x1 > x0) ? (x1 - x0) : (x0 - x1);
        int dy = (y1 > y0) ? (y1 - y0) : (y0 - y1);
        //���� ����� ����������� ����� �� ����, ������ ������� ����� - ��������� ��� ������� � ���
        if (dx == 0 || dy == 0)
        {
            //g.DrawLine(new Pen(clr), x0, y0, x1, y1);
            //return;
        }

        //��� �-����� (����������� ������� < 1)
        if (dy < dx)
        {
            //������ ����� ������ ����� ������� ���������� �
            if (x1 < x0)
            {
                x1 += x0;
                x0 = x1 - x0;
                x1 -= x0;
                y1 += y0;
                y0 = y1 - y0;
                y1 -= y0;
            }
            //������������� ��������� ���������� Y
            float grad = (float)dy / dx;
            //������������� ���������� ��� Y
            float intery = y0 + grad;
            //������ �����
            PutPixel(x0, y0, 255);

            for (int x = x0 + 1; x < x1; x++)
            {
                //������� �����
                PutPixel(x, (int)intery, (int)(255 - FPart(intery) * 255));
                //������ �����
                PutPixel(x, (int)intery + 1, (int)(FPart(intery) * 255));
                //��������� ���������� Y
                intery += grad;
            }
            //��������� �����
            PutPixel(x1, y1, 255);
        }
            //��� Y-����� (����������� ������� > 1)
        else
        {
            //������ ����� ������ ����� ������� ���������� Y
            if (y1 < y0)
            {
                x1 += x0;
                x0 = x1 - x0;
                x1 -= x0;
                y1 += y0;
                y0 = y1 - y0;
                y1 -= y0;
            }
            //������������� ��������� ���������� X
            float grad = (float)dx / dy;
            //������������� ���������� ��� X
            float interx = x0 + grad;
            //������ �����
            PutPixel(x0, y0, 255);
            for (int y = y0 + 1; y < y1; y++)
            {
                //������� �����
                PutPixel((int)interx, y, 255 - (int)(FPart(interx) * 255));
                //������ �����
                PutPixel((int)interx + 1, y, (int)(FPart(interx) * 255));
                //��������� ���������� X
                interx += grad;
            }
            //��������� �����
            PutPixel(x1, y1, 255);
        }
    }


    private static void PutPixel(int x, int y, int alpha)
    {
       // g.FillRectangle(new SolidBrush(Color.FromArgb(alpha, col)), x, y, 1, 1);
    }



    private static float FPart(float x)
    {
        return x - ((int)x);
    }

    public static void DrawWuCircle(Graphics g, Color clr, int _x, int _y, int radius)
    {
        //��������� ��������, ������� �� ���� ������� ��������� � ������� � ������
        PutPixel(_x + radius, _y, 255);
        PutPixel(_x, _y + radius, 255);
        PutPixel(_x - radius + 1, _y, 255);
        PutPixel(_x, _y - radius + 1, 255);

        float iy = 0;
        for (int x = 0; x <= radius * Math.Cos(Math.PI / 4); x++)
        {
            //���������� ������� �������� ���������� Y 
            iy = (float)Math.Sqrt(radius * radius - x * x);

            //IV ��������, Y
            PutPixel(_x - x, _y + (int)iy, 255 - (int)(FPart(iy) * 255));
            PutPixel(_x - x, _y + (int)(iy) + 1, (int)(FPart(iy) * 255));
            //I ��������, Y
            PutPixel(_x + x, _y + (int)(iy), 255 - (int)(FPart(iy) * 255));
            PutPixel(_x + x, _y + (int)(iy) + 1, (int)(FPart(iy) * 255));
            //I ��������, X
            PutPixel(_x + (int)(iy), _y + x, 255 - (int)(FPart(iy) * 255));
            PutPixel(_x + (int)(iy) + 1, _y + x, (int)(FPart(iy) * 255));
            //II ��������, X
            PutPixel(_x + (int)(iy), _y - x, 255 - (int)(FPart(iy) * 255));
            PutPixel(_x + (int)(iy) + 1, _y - x, (int)(FPart(iy) * 255));

            //� ������� ���������� ����������� ������ �������� �� 1 ������
            x++;
            //II ��������, Y
            PutPixel(_x + x, _y - (int)(iy), (int)(FPart(iy) * 255));
            PutPixel(_x + x, _y - (int)(iy) + 1, 255 - (int)(FPart(iy) * 255));
            //III ��������, Y
            PutPixel(_x - x, _y - (int)(iy), (int)(FPart(iy) * 255));
            PutPixel(_x - x, _y - (int)(iy) + 1, 255 - (int)(FPart(iy) * 255));
            //III ��������, X
            PutPixel(_x - (int)(iy), _y - x, (int)(FPart(iy) * 255));
            PutPixel(_x - (int)(iy) + 1, _y - x, 255 - (int)(FPart(iy) * 255));
            //IV ��������, X
            PutPixel(_x - (int)(iy), _y + x, (int)(FPart(iy) * 255));
            PutPixel(_x - (int)(iy) + 1, _y + x, 255 - (int)(FPart(iy) * 255));
            //������� ��������
            x--;
        }
    }

    public static void Rect(int x0, int y0, int x1, int y1)
    {
        Line(x0, y0, x0, y1);
        Line(x1, y0, x1, y1);
        Line(x0, y0, x1, y0);
        Line(x0, y1, x1, y1);
    }
    public static void Circle(int x0, int y0, float radius)
    {
        float chuvi = 2f * Mathf.PI * radius;
        int _delta = (int)(chuvi / 2f);
        if (_delta > 50)
            _delta = 50;
        float _angle = (2 * Mathf.PI) / (float)_delta;

        float tx, ty, temp;
        tx = x0;
        ty = radius + y0;


        Vector2 fPoint = new Vector2(tx, ty);
        Vector2 lastPoint = fPoint;
        for (int i = 1; i <= _delta; i++)
        {
            temp = i * _angle;
            tx = radius * (float)Math.Sin(temp) + x0;
            ty = radius * (float)Math.Cos(temp) + y0;
            Vector2 tPoint = new Vector2(tx, ty);
            Line(lastPoint, tPoint);
            lastPoint = tPoint;
        }
        Line(lastPoint, fPoint);
    }
    public static void Ellipse(int cx, int cy, int rx, int ry, float angle)
    {
        float chuvi = 2f * Mathf.PI * (float)Math.Sqrt(rx * rx + ry * ry);
        int steps = (int)(chuvi / 3);
        if (steps > 50)
            steps = 50;
        float beta = angle * Mathf.Deg2Rad;
        float sinbeta = Mathf.Sin(beta);
        float cosbeta = Mathf.Cos(beta);

        steps = 360 / steps;

        int i = 0;
        float alpha = i * Mathf.Deg2Rad;
        float sinalpha = Mathf.Sin(alpha);
        float cosalpha = Mathf.Cos(alpha);

        float _x = cx + (rx * cosalpha * cosbeta - ry * sinalpha * sinbeta);
        float _y = cy + (rx * cosalpha * sinbeta + ry * sinalpha * cosbeta);

        float _fPointx = _x;
        float _fPointy = _y;

        float lastX = _x;
        float lastY = _y;

        for (i = 1; i < 360; i += steps)
        {
            alpha = i * Mathf.Deg2Rad;
            sinalpha = Mathf.Sin(alpha);
            cosalpha = Mathf.Cos(alpha);

            _x = cx + (rx * cosalpha * cosbeta - ry * sinalpha * sinbeta);
            _y = cy + (rx * cosalpha * sinbeta + ry * sinalpha * cosbeta);

            Line(lastX, lastY, _x, _y);
            lastX = _x;
            lastY = _y;
        }
        Line(lastX, lastY, _fPointx, _fPointy);
    }
    public static void Arc(Vector2 p1, float rx, float ry, float angle, bool largeArcFlag, bool sweepFlag, Vector2 p2)
    {
        float tx, ty;
        double trx2, try2, tx2, ty2;
        float temp1, temp2;
        float _radian = (angle * Mathf.PI / 180.0f);
        float _CosRadian = (float)Math.Cos(_radian);
        float _SinRadian = (float)Math.Sin(_radian);
        temp1 = (p1.x - p2.x) / 2.0f;
        temp2 = (p1.y - p2.y) / 2.0f;
        tx = (_CosRadian * temp1) + (_SinRadian * temp2);
        ty = (-_SinRadian * temp1) + (_CosRadian * temp2);

        trx2 = rx * rx;
        try2 = ry * ry;
        tx2 = tx * tx;
        ty2 = ty * ty;


        double radiiCheck = tx2 / trx2 + ty2 / try2;
        if (radiiCheck > 1)
        {
            rx = (float)(float)Math.Sqrt((float)radiiCheck) * rx;
            ry = (float)Math.Sqrt((float)radiiCheck) * ry;
            trx2 = rx * rx;
            try2 = ry * ry;
        }

        double tm1;
        tm1 = (trx2 * try2 - trx2 * ty2 - try2 * tx2) / (trx2 * ty2 + try2 * tx2);
        tm1 = (tm1 < 0) ? 0 : tm1;

        float tm2;
        tm2 = (largeArcFlag == sweepFlag) ? -(float)Math.Sqrt((float)tm1) : (float)Math.Sqrt((float)tm1);


        float tcx, tcy;
        tcx = tm2 * ((rx * ty) / ry);
        tcy = tm2 * (-(ry * tx) / rx);

        float cx, cy;
        cx = _CosRadian * tcx - _SinRadian * tcy + ((p1.x + p2.x) / 2.0f);
        cy = _SinRadian * tcx + _CosRadian * tcy + ((p1.y + p2.y) / 2.0f);

        float ux = (tx - tcx) / rx;
        float uy = (ty - tcy) / ry;
        float vx = (-tx - tcx) / rx;
        float vy = (-ty - tcy) / ry;
        float _angle, _delta;

        float p, n, t;
        n = (float)Math.Sqrt((ux * ux) + (uy * uy));
        p = ux;
        _angle = (uy < 0) ? -(float)Math.Acos(p / n) : (float)Math.Acos(p / n);
        _angle = _angle * 180.0f / Mathf.PI;
        _angle %= 360f;

        n = (float)Math.Sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy));
        p = ux * vx + uy * vy;
        t = p / n;
        if ((Math.Abs(t) >= 0.99999f) && (Math.Abs(t) < 1.000009f))
        {
            if (t > 0)
                t = 1f;
            else
                t = -1f;
        }
        _delta = (ux * vy - uy * vx < 0) ? -(float)Math.Acos(t) : (float)Math.Acos(t);

        _delta = _delta * 180.0f / Mathf.PI;

        if (!sweepFlag && _delta > 0)
        {
            _delta -= 360f;
        }
        else if (sweepFlag && _delta < 0)
            _delta += 360f;

        _delta %= 360f;

        int number = 100;
        float deltaT = _delta / number;

        Vector2 _point = new Vector2(0, 0);
        Vector2 lastPoint = _point;

        float t_angle;
        for (int i = 0; i <= number; i++)
        {
            t_angle = (deltaT * i + _angle) * Mathf.PI / 180.0f;
            _point.x = _CosRadian * rx * (float)Math.Cos(t_angle) - _SinRadian * ry * (float)Math.Sin(t_angle) + cx;
            _point.y = _SinRadian * rx * (float)Math.Cos(t_angle) + _CosRadian * ry * (float)Math.Sin(t_angle) + cy;
            Line(lastPoint, _point);
            lastPoint = _point;
        }
    }
}

