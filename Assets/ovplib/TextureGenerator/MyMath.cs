﻿using UnityEngine;

public static class MyMath
{
    public static float Noise2D(int x, int y)
    {
        //diapazone[-1: 1]
        x = x + y * 57;
        x = (x << 13) ^ x;
        return (1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
    }
    static int SmpleRandom(int seed, int a, int b)
    {
        int iran = seed ^ (seed << 7);
        return (iran % a) - b;
    }
    public static int[,] DiamondSquare(int count, int[,] levels, int C1, int C2, int C3, int C4)
    {
        int size = 1 << count;
        int[,] heightMap = new int[size + 1,size + 1];

        heightMap[0, 0] = C1;
        heightMap[0, size] = C2;
        heightMap[size, 0] = C3;
        heightMap[size, size] = C4;

        int tempSize = size;
        int tempSize2;

        int a;
        int b;

        int c1;
        int c2;
        int c3;
        int c4;
        int ce;

        for (int i = 0; i < count; i++)
        {
            tempSize = size >> i;
            tempSize2 = tempSize >> 1;


            a = levels[i, 0];
            b = levels[i, 1];

            //central
            for (int x = 0; x < size; x += tempSize)
            {
                for (int y = 0; y < size; y += tempSize)
                {

                    c1 = heightMap[x, y];
                    c2 = heightMap[x, y + tempSize];
                    c3 = heightMap[x + tempSize, y];
                    c4 = heightMap[x + tempSize, y + tempSize];
                    ce = c1 + c2 + c3 + c4;
                    heightMap[x + tempSize2, y + tempSize2] = (ce >> 2) + SmpleRandom(ce, a, b);
                }
            }
            //horizontal
            for (int y = 0; y < size; y += tempSize)
            {
                if (y == 0)
                {
                    for (int x = tempSize2; x < size; x += tempSize)
                    {
                        c3 = heightMap[x + tempSize2, y];
                        c4 = heightMap[x - tempSize2, y];
                        ce = c3 + c4;
                        heightMap[x, y] = (ce >> 1) + SmpleRandom(ce, a, b);
                    }
                }
                else
                {
                    for (int x = tempSize2; x < size; x += tempSize)
                    {
                        c1 = heightMap[x, y + tempSize2];
                        c2 = heightMap[x, y - tempSize2];
                        c3 = heightMap[x + tempSize2, y];
                        c4 = heightMap[x - tempSize2, y];
                        ce = c1 + c2 + c3 + c4;
                        heightMap[x, y] = (ce >> 2) + SmpleRandom(ce, a, b);
                    }
                }
            }
            for (int x = tempSize2; x < size; x += tempSize)
            {
                c3 = heightMap[x + tempSize2, size];
                c4 = heightMap[x - tempSize2, size];
                ce = c3 + c4;
                heightMap[x, size] = (ce >> 1) + SmpleRandom(ce, a, b);
            }

            //vertical
            for (int x = 0; x < size; x += tempSize)
            {
                if (x == 0)
                {
                    for (int y = tempSize2; y < size; y += tempSize)
                    {
                        c1 = heightMap[x, y + tempSize2];
                        c2 = heightMap[x, y - tempSize2];
                        ce = c1 + c2;
                        heightMap[x, y] = (ce >> 1) + SmpleRandom(ce, a, b);
                    }
                }
                else
                {
                    for (int y = tempSize2; y < size; y += tempSize)
                    {
                        c1 = heightMap[x, y + tempSize2];
                        c2 = heightMap[x, y - tempSize2];
                        c3 = heightMap[x + tempSize2, y];
                        c4 = heightMap[x - tempSize2, y];
                        ce = c1 + c2 + c3 + c4;
                        heightMap[x, y] = (ce >> 2) + SmpleRandom(ce, a, b);
                    }
                }
            }
            for (int y = tempSize2; y < size; y += tempSize)
            {
                c1 = heightMap[size, y + tempSize2];
                c2 = heightMap[size, y - tempSize2];
                ce = c1 + c2;
                heightMap[size, y] = (ce >> 1) + SmpleRandom(ce, a, b);
            }
        }

        return heightMap;
    }



    public static bool IntersectRaySphere(Vector3 pos, Vector3 dir, Vector3 center, out Vector3 point, float radius)
    {
        Vector3 relPos = pos - center;
        float a = 2f * Vector3.Dot(relPos, dir);
        float b = Vector3.Dot(relPos, relPos) - radius * radius;
        float discr = a * a - 4f * b;

        point = Vector3.zero;
        if (discr < 0) return false;

        float c = Mathf.Sqrt(discr);
        float t1 = -a - c;
        float t2 = -a + c;

        float t = t1 < t2 ? t1 : t2;
        if (t < 0)
        {
            t = t1 > t2 ? t1 : t2;
            if (t < 0) return false;
        }
        point = pos + dir * t * 0.5f;
        return true;
    }

}