﻿using System;
using UnityEngine;
using System.Collections;

public static class TextureUtils
{
    public static void FillTexture(Texture2D tex, Func<float, float, Color> func)
    {
        Color[] colors = GetColors(tex.width, tex.height, func);
        tex.SetPixels(colors);
        tex.Apply();
    }

    public static Color[] GetColors(int width, int height , Func<float, float, Color> func)
    {
        Color[] colors = new Color[width * height];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                float fx = x / (float)width;
                float fy = y / (float)height;
                colors[x + y * width] = func(fx, fy);
            }
        return colors;
    }
}

