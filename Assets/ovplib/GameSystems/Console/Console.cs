﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Console : MonoBehaviour
{
    static Dictionary<string, Command> _commands = new Dictionary<string, Command>();
    static string _consoleString = "";
    static Console()
    {
        AddCommand("help", delegate(string[] strings)
            {

                int maxLength = 0;
                foreach (KeyValuePair<string, Command> pair in _commands)
                    if (pair.Key.Length > maxLength) maxLength = pair.Key.Length;
                maxLength += 3;

                string s = "";
                foreach (KeyValuePair<string, Command> pair in _commands)
                    s += pair.Key + new string(' ', maxLength - pair.Key.Length) + pair.Value.Description + "\n";

                Write(s);
            }, "show console commands");
        AddCommand("clear", delegate(string[] strings) { _consoleString = ""; }, "clear console");
    }
    public static void AddCommand(string commandName, Action<string[]> action, string description = null)
    {
        if (commandName == null)
        {
            Debug.LogError("COMMAND NAME IS NULL!!!");
            return;
        }
        if (action == null)
        {
            Debug.LogError("ACTION IS NULL!!!");
            return;
        }
        commandName = commandName.ToLower();
        if (_commands.ContainsKey(commandName))
        {
            Debug.LogError(commandName + " already exist");
            return;
        }
        if (description == null) description = "";
        _commands.Add(commandName, new Command(description, action));
    }
    public static void Do(string line)
    {
        line = line.Trim().ToLower();
        string[] parts = line.Split(' ');
        if (parts.Length == 0) return;

        Command command;
        if (!_commands.TryGetValue(parts[0], out command))
        {
            Write("Unknown command \"" + line + "\"");
            return;
        }
        /*
        try
        {
            if (parts.Length == 1) command.Action(null);
            else
            {
                string[] parameters = new string[parts.Length - 1];
                parts.CopyTo(parameters, 1);
                command.Action(parameters);
            }
        }
        catch (Exception ex)
        {
            Write("[Exception]" + ex.Message);
        }
        /*/
        if (parts.Length == 1) command.Action(null);
        else
        {
            string[] parameters = new string[parts.Length - 1];
            for (int i = 0; i < parameters.Length; i++) parameters[i] = parts[i + 1];
            command.Action(parameters);
        }
        //*/
    }
    public static void Write(string line)
    {
        _consoleString += line + "\n";
    }




    void Awake()
    {
        //Application.RegisterLogCallbackThreaded(UnityLogCallback);
        Application.logMessageReceivedThreaded += UnityLogCallback;
        ConsoleView.Initialize();
    }
    static void UnityLogCallback(string condition, string stackTrace, LogType type)
    {
        string color = "<color=#ffff>";
        switch (type)
        {
            case LogType.Log:
                color = "<color=#ffff>[LOG] ";
                break;
            case LogType.Error:
                color = "<color=#f88f>[ERROR] ";
                break;
            case LogType.Warning:
                color = "<color=#fb8f>[WARNING] ";
                break;
            case LogType.Exception:
                color = "<color=#fb8f>[EXCEPTION] ";
                break;
            case LogType.Assert:
                color = "<color=#fb8f>[ASSERT] ";
                break;
        }

        Write(color + condition + "</color>\n<color=#fff8>" + stackTrace + "</color>");
    }
    class Command
    {
        public string Description;
        public Action<string[]> Action;
        public Command(string description, Action<string[]> action)
        {
            Description = description;
            Action = action;
        }
    }








    void OnGUI()
    {
        ConsoleView.OnGUI();
    }

    public View ConsoleView;
    [Serializable]
    public class View
    {
        Texture2D _backGround;
        public Font Font;
        public float ConsoleHeight = 0.5f;
        GUIStyle _textStyle;
        GUIStyle _textFieldStyle;


        Rect _consoleRect;
        Rect _backGroundTexCoords;
        Rect _consoleTextRect;
        Rect _textFieldRect;

        string _enterText = "";
        Vector2 _scrollPosition;
        public void Initialize()
        {
            _backGround = GetTex(1, 2, new Color(0, 0, 0, 0.6f), new Color(0, 0, 0, 0.9f));
            _textStyle = new GUIStyle
                {
                    richText = true,
                    font = Font,
                    normal = {textColor = Color.white},
                    wordWrap = true
                };

            _textFieldStyle = new GUIStyle();
            _textFieldStyle.richText = false;
            _textFieldStyle.font = Font;
            _textFieldStyle.normal.background = GetTex(1, 1, new Color(0, 0, 0, 0.6f));
            _textFieldStyle.normal.textColor = Color.white;
            _textFieldStyle.alignment = TextAnchor.MiddleLeft;
            _textFieldStyle.contentOffset = new Vector2(SideOffset, 0);


        }
        Texture2D GetTex(int width, int height, params Color32[] colors)
        {
            Texture2D tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
            tex.SetPixels32(colors);
            tex.Apply();
            return tex;
        }

        const int MenuHeight = 24;
        const int TextFieldHeight = 24;
        const int SideOffset = 8;
        void CulcPositions()
        {
            _consoleRect = new Rect(0, 0, Screen.width, Screen.height * ConsoleHeight);
            _backGroundTexCoords = new Rect(0, 0, _consoleRect.width, _consoleRect.height * 0.5f);
            _consoleTextRect = new Rect(SideOffset, MenuHeight, Screen.width - (SideOffset << 1), _consoleRect.height - (MenuHeight + TextFieldHeight));
            _textFieldRect = new Rect(0, _consoleTextRect.yMax, Screen.width, TextFieldHeight);
        }

        public void OnGUI()
        {
            CulcPositions();
            GUI.skin.settings.selectionColor = new Color(1, 1, 1, 0.35f);

            if (Event.current.isKey && Event.current.modifiers == 0 && GUI.GetNameOfFocusedControl() != "textField")
            {
                GUI.FocusControl("textField");
            }




            GUI.DrawTextureWithTexCoords(_consoleRect, _backGround, _backGroundTexCoords);

            GUI.SetNextControlName("textField");
            _enterText = GUI.TextField(_textFieldRect, _enterText, _textFieldStyle);

            GUILayout.BeginArea(_consoleTextRect);
            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, false, true);

            Color savedCursorColor = GUI.skin.settings.cursorColor;
            GUI.skin.settings.cursorColor = new Color(0, 0, 0, 0);
            GUILayout.TextArea(_consoleString, _textStyle);
            GUI.skin.settings.cursorColor = savedCursorColor;

            GUILayout.EndScrollView();
            GUILayout.EndArea();


            if (Event.current.type == EventType.layout && Event.current.keyCode == KeyCode.Return && _enterText != "")
            {
                Console.Do(_enterText);
                _enterText = "";
                Event.current.Use();
            }
        }

        public static void SelectableLabel(string text,GUIStyle style)
        {

            Color color = GUI.skin.settings.cursorColor;
            GUI.skin.settings.cursorColor = new Color(0f, 0f, 0f, 0f);
            GUILayout.TextArea(text, style);
            GUI.skin.settings.cursorColor = color;
        }
    }
}