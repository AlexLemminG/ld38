using System;
using System.Collections.Generic;


public class Characteristics
{
    Characteristic[] _characteristics;
    Dictionary<object, ParentObject> _parentObjects = new Dictionary<object, ParentObject>();
    HashSet<Characteristic> _changed = new HashSet<Characteristic>();


    public Characteristics(Type enumType)
    {
        int length = Enum.GetValues(enumType).Length;
        _characteristics = new Characteristic[length];
        for (int i = 0; i < length; i++)
            _characteristics[i] = new Characteristic(this);
    }

    public void Update()
    {
        if (_changed.Count == 0) return;
        foreach (Characteristic stat in _changed)
            stat.Update();
        _changed.Clear();
    }

    public float Get(Enum characteristic)
    {
        return _characteristics[Convert.ToInt32(characteristic)].Value;
    }
    public void SetBase(Enum characteristic, float value)
    {
        int i = Convert.ToInt32(characteristic);
        _characteristics[i].Base = value;
        _characteristics[i].Changed();
    }
    public void Clear(object obj)
    {
        ParentObject parentObject;
        if (!_parentObjects.TryGetValue(obj, out parentObject)) return;

        LinkedList<Modificator> values = parentObject.Values;
        foreach (Modificator value in values)
        {
            Characteristic characteristic = _characteristics[value.StatId];
            HashSet<Modificator> set = value.IsAdd ? characteristic.Add : characteristic.Multiply;
            set.Remove(value);
            characteristic.Changed();
        }
        foreach (Characteristic stat in _characteristics) stat.Update();
    }
    public Modificator Add(object obj, Enum characteristic, float value)
    {
        return AddValue(obj, characteristic, value, true);
    }
    public Modificator Multiply(object obj, Enum characteristic, float value)
    {
        return AddValue(obj, characteristic, value, false);
    }
    Modificator AddValue(object obj, Enum charId, float value, bool add)
    {
        int i = Convert.ToInt32(charId);
        Modificator modificator = new Modificator(add, i, value, this);
        Characteristic characteristic = _characteristics[i];
        HashSet<Modificator> hashSet = add ? characteristic.Add : characteristic.Multiply;
        hashSet.Add(modificator);
        characteristic.Changed();

        ParentObject obje;
        if (!_parentObjects.TryGetValue(obj, out obje))
        {
            obje = new ParentObject();
            _parentObjects.Add(obj, obje);
        }
        obje.Values.AddLast(modificator);
        return modificator;
    }


    class Characteristic
    {
        Characteristics _parent;
        public float Base;
        public HashSet<Modificator> Multiply = new HashSet<Modificator>();
        public HashSet<Modificator> Add = new HashSet<Modificator>();
        public float Value;
        public Characteristic(Characteristics parent)
        {
            _parent = parent;
        }
        public void Update()
        {
            Value = Base;
            foreach (Modificator v in Multiply) Value *= v.Value;
            foreach (Modificator v in Add) Value += v.Value;
        }
        public void Changed()
        {
            _parent._changed.Add(this);
        }
    }

    class ParentObject
    {
        public LinkedList<Modificator> Values = new LinkedList<Modificator>();
    }

    public class Modificator
    {
        Characteristic _parent;
        public bool IsAdd { get; private set; }
        public int StatId { get; private set; }
        public float Value { get; private set; }
        public Modificator(bool isAdd, int statId, float val, Characteristics parent)
        {
            IsAdd = isAdd;
            StatId = statId;
            Value = val;
            _parent = parent._characteristics[statId];
        }
        public void Set(float value)
        {
            Value = value;
            _parent.Changed();
        }
    }
}