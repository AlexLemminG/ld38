using System;
using System.Collections.Generic;
using UnityEngine;


public class StateMachine
{
    State[] _states;
    public event Action<Enum, float> OnStartState = (e, f) => { };

    public Enum CurrentState { get { return _current.My; } }

    #region cacheVaribles
    State _current;
    State _next;
    State _nextFromEvent;

    float _cashedNextTime;
    #endregion


    public StateMachine(Type stateEnumType, Enum defaultEvent)
    {
        int length = Enum.GetValues(stateEnumType).Length;
        _states = new State[length];
        for (int i = 0; i < length; i++) _states[i] = new State();
        _current =  _states[Convert.ToInt32(defaultEvent)];
    }

    public void Update()
    {
        bool firstFrame = _next != null;
        if (_current == null) return;

        bool start = false;
        bool end = false;
        if (firstFrame)
        {
            start = _current.Duration > 0 || _current != _next;
            if (start)
            {
                _current = _next;
                _next = null;
                _nextFromEvent = null;
                StartAnimation(_current.My, _current.Duration);
            }
        }
        if (TimePassed())
        {
            //Debug.Log("PROCESS end: " + _states[_current.OnTimeEndState].My + "   frEvent: " + (_nextFromEvent == null ? "null" : _nextFromEvent.My.ToString()));
            _next = _nextFromEvent ?? _states[_current.OnTimeEndState];
            _nextFromEvent = null;
            end = true;
            //Debug.Log("result: " + _next.My);
        }
        if (_current.Action != null)
            _current.Action(start, end);
    }


    public void DoEvent(Enum @event)
    {
        int i = Convert.ToInt32(@event);
        Event eEvent;


        if (_current.Events.TryGetValue(i, out eEvent))
        {
            int nextState = eEvent.GetNextState();
            //Debug.Log("DoEvent: " + @event + "   \nnextState: " + ((WeaponInHands.States)nextState));
            if (nextState != -1) _nextFromEvent = _states[nextState];
        }
    }
    /// <summary>
    /// ��������� ��������� ����� ��������
    /// </summary>
    public void SetNextState(Enum state)
    {
        _next = _states[Convert.ToInt32(state)];
    }
    /// <summary>
    /// ��������� ���������� �����, ���� � �������� ���������� ��������
    /// </summary>
    public void SetStateImmediate(Enum state)
    {
        _next = _states[Convert.ToInt32(state)];
        _nextFromEvent = null;
        _cashedNextTime = 0f;
    }
    /// <summary>
    /// �������� ���������, onTimeEndState - ��������� ���������, ���������� ����
    /// </summary>
    public void AddState(Enum state, float duration, Enum onTimeEndState, ActionDel action = null)
    {
        _states[Convert.ToInt32(state)].Initialize(state, action, duration, Convert.ToInt32(onTimeEndState));
    }
    /// <summary>
    /// �������� ���������� ���������, ��� ����������� �� ��� ��� ���� �� �������� ��������
    /// </summary>
    public void AddState(Enum state, ActionDel action = null)
    {
        int i = Convert.ToInt32(state);
        _states[i].Initialize(state, action, -1, i);
    }

    /// <summary>
    /// �������� ������� �� �������
    /// </summary>
    public void AddTransition(Enum fromState, Enum @event, Func<Enum> action)
    {
        Dictionary<int, Event> events = _states[Convert.ToInt32(fromState)].Events;
        int key = Convert.ToInt32(@event);
        Event cevent;
        if (!events.TryGetValue(key, out cevent))
            events.Add(key, cevent = new Event());

        cevent.SetFunc(action);
    }
    /// <summary>
    /// �������� ������� �� �������
    /// </summary>
    public void AddTransition(Enum fromState, Enum @event, Func<bool> action, Enum toState)
    {
        AddTransition(fromState, @event, toState, action);
    }
    /// <summary>
    /// �������� ������� �� �������
    /// </summary>
    public void AddTransition(Enum fromState, Enum @event, Enum toState)
    {
        AddTransition(fromState, @event, toState, null);
    }
    void AddTransition(Enum fromState, Enum @event, Enum toState, Func<bool> action)
    {
        Dictionary<int, Event> events = _states[Convert.ToInt32(fromState)].Events;
        int key = Convert.ToInt32(@event);
        Event cevent;
        if (!events.TryGetValue(key, out cevent))
            events.Add(key, cevent = new Event());

        if (action == null)
            cevent.DefaultState = Convert.ToInt32(toState);
        else
            cevent.AddCase(action, Convert.ToInt32(toState));
    }


    void StartAnimation(Enum state, float duration)
    {
        float time = (Time.time < _cashedNextTime) ? _cashedNextTime : Time.time;
        _cashedNextTime = time + duration;
        OnStartState(state, time);
    }
    public void FixCurrentStateDuration(float newDuration)
    {
        _cashedNextTime = _cashedNextTime - _current.Duration + newDuration;
    }
    public void SetStateDuration(Enum state,float newDuration)
    {
        _states[Convert.ToInt32(state)].Duration = newDuration;
    }


    bool TimePassed()
    {
        return (Time.time + (1f / 16f)) >= _cashedNextTime;
    }


    class State
    {
        public Enum My;
        public ActionDel Action;
        public float Duration;
        public Dictionary<int, Event> Events = new Dictionary<int, Event>();
        public int OnTimeEndState;
        static readonly ActionDel None =( s,e) => { };

        public void Initialize(Enum my, ActionDel action, float duration, int onTimeEndState)
        {
            Duration = duration;
            OnTimeEndState = onTimeEndState;
            My = my;
            Action = action ?? None;
        }
    }

    class Event
    {
        List<Case> _cases = new List<Case>();
        Func<Enum> _func;
        public int DefaultState = -1;

        public int GetNextState()
        {
            if (_func != null) return Convert.ToInt32(_func());
            foreach (Case @case in _cases)
                if (@case._action()) return @case._state;
            return DefaultState;
        }
        public void AddCase(Func<bool> action, int state)
        {
            _cases.Add(new Case { _action = action, _state = state });
        }
        public void SetFunc(Func<Enum> func)
        {
            _func = func;
        }

        class Case
        {
            public Func<bool> _action;
            public int _state;
        }
    }

    public delegate void ActionDel(bool start, bool end);
}