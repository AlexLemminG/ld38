using System.Collections.Generic;
using UnityEngine;

class AudioQueue
{
    AudioSource[] _sources;
    int _lastSource;
    LinkedList<Sample> _samples = new LinkedList<Sample>();
    double _nextCheckTime;
    public int Count { get { return _samples.Count; } }

    public AudioQueue(AudioSource source0, AudioSource source1)
    {
        _sources = new AudioSource[] { source0, source1 };
    }
    public void AddClipFTime(AudioClip clip, float start, float end = 0)
    {
        float time = Time.time;
        double dspTime = AudioSettings.dspTime;
        if (end == 0)
            AddClip(clip, (start - time) + dspTime);
        else
            AddClip(clip, (start - time) + dspTime, (end - time) + dspTime);
    }

    public void AddClip(AudioClip clip, double start, double end = 0)
    {

        if (end != 0 && end <= start)
        {
            Debug.LogError("AudioQueue: clip end time is to small, clip wasn't added to queue =(\nstart time:" + start + "  end time:" + end);
            return;
        }
        _samples.AddLast(new Sample(clip, start, end));
        if (_nextCheckTime > start) _nextCheckTime = 0;
    }
    public void Update()
    {
        double time = AudioSettings.dspTime;

        if (_nextCheckTime > time) return;
        if(_samples.First == null) return;

        Sample sample = _samples.First.Value;
        _samples.RemoveFirst();

        if (!_sources[_lastSource].isPlaying) _lastSource = _lastSource == 0 ? 1 : 0;

        AudioSource lastSource = _sources[_lastSource];
        _lastSource = _lastSource == 0 ? 1 : 0;
        AudioSource nextSource = _sources[_lastSource];
        nextSource.loop = false;


        lastSource.SetScheduledEndTime(sample.Start);
        nextSource.clip = sample.Clip;
        nextSource.PlayScheduled(sample.Start);

        if (sample.End != 0)
            nextSource.SetScheduledEndTime(sample.End);
        else if (_samples.First != null)
            nextSource.SetScheduledEndTime(_samples.First.Value.Start);
        

        _nextCheckTime = sample.Start;
    }



    class Sample
    {
        public AudioClip Clip;
        public double Start;
        public double End;
        public Sample(AudioClip clip, double start, double end)
        {
            Clip = clip;
            Start = start;
            End = end;
        }
    }
}