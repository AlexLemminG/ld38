using System.Collections.Generic;
using UnityEngine;

class AnimationQueue
{
    Animation _animation;
    LinkedList<Sample> _samples = new LinkedList<Sample>();
    Sample _currentSample;
    AnimationState _currentState;
    float _endTime;
    public bool WillChange
    {
        get
        {
            if (_currentState == null) return true;
            return _currentState.time >= _endTime;
        }
    }
    public float Delta
    {
        get
        {
            if (_currentState == null) return 0f;
            return _currentState.time - _endTime;
        }
    }


    public AnimationQueue(Animation animation)
    {
        _animation = animation;
    }
    public void Add(string name)
    {
        _animation[name].wrapMode = WrapMode.ClampForever;
        _samples.AddLast(new Sample(name));
    }
    public void Reset()
    {
        if (_currentState != null)
        {
            _animation.Rewind(_currentState.name);
            _currentState = null;
        }
    }
    public void SinchronizeWith(AnimationQueue queue)
    {
        if (_currentState == null || queue._currentState == null) return;

        float delta = queue._currentState.length - queue._currentState.time;
        _currentState.time = _currentState.length - delta;
    }

    public void Update()
    {
        if (_samples.First == null) return;
        float delta = (_currentState != null && _currentState.enabled) ? _currentState.time - _endTime : 0f;
        if (delta >= 0)
        {
            
            _currentSample = _samples.First.Value;
            _samples.RemoveFirst();
            //Debug.Log(_currentSample.Name + "\n" + delta);
            _currentState = _animation[_currentSample.Name];
            _animation[_currentSample.Name].time = delta;
            _animation.Play(_currentSample.Name);
            _endTime = _currentState.length;
        }
    }

    class Sample
    {
        public string Name;
        public Sample(string name)
        {
            Name = name;
        }
    }

}