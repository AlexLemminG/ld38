using UnityEngine;


[System.Serializable]
public class Spring
{
    public Vector3 Zero;

    public float ReturnSpeed = 0.1f;
    public float Damping = 0.5f;

    public Vector3 Min = -Vector3.one * 180;
    public Vector3 Max = Vector3.one * 180;


    Vector3 _velocity;
    Vector3 _current;

    SumArrayV3 _softAccelerations = new SumArrayV3(4);
    Vector3 _softAccelerationsAccumulator;
    Vector3 _forceAccumulator;
    int _framesLeft = 0;

    public void FixedUpdate()
    {
        _velocity -= _current * ReturnSpeed;

        if (_framesLeft > 0)
        {
            _softAccelerations.AddValue(_softAccelerationsAccumulator);
            _softAccelerationsAccumulator = Vector3.zero;
            _velocity += _softAccelerations.Average;
            _framesLeft--;
        }

        _velocity *= Damping;
        _current += _velocity;
        _current = Vector3.Min(Vector3.Max(_current, Min), Max); // clamp min max
    }
    public Vector3 Get()
    {
        return _current + Zero;
    }



    public void AddAcceleration(int comp, float val)
    {
        _velocity[comp] += val;
    }
    public void AddAcceleration(Vector3 acceleration)
    {
        _velocity += acceleration;
        _framesLeft = _softAccelerations.Length;
    }
    public void AddSoftAcceleration(Vector3 acceleration)
    {
        _softAccelerationsAccumulator += acceleration;
    }
}