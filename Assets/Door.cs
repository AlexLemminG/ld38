﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
	public ObjectCounter counter;

	bool opened = false;

	void Update(){
//		if (!opened && counter.amountInside == 0 && Time.time > 5) {
//			enabled = false;
//			DG.Tweening.DOVirtual.DelayedCall(0.5f, Open);
//		}
	}

	public void Open(){
		if (opened)
			return;
		opened = true;
		gameObject.SetActive (false);
	}

	public void Close(){
		if (!opened)
			return;
		opened = false;
		gameObject.SetActive (true);
	}
}
