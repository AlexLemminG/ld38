﻿using UnityEngine;
using System.Collections;

public class PlayerMovementController : MonoBehaviour {
	Vector3 up = Vector3.forward;
	Vector3 right = Vector3.right;
	float speed = 5f;
	CharacterController cc;
	void Awake(){
		cc = GetComponent<CharacterController> ();
		GetComponent<Health> ().onDeath += () => {
			gameObject.SetActive(false);
		};
	}

	void Update () {
		bool kLeft = Input.GetKey (KeyCode.A);
		bool kRight = Input.GetKey (KeyCode.D);
		bool kUp = Input.GetKey (KeyCode.W);
		bool kDown = Input.GetKey (KeyCode.S);

		Vector3 velocity = Vector3.zero;

		if (kLeft)
			velocity -= right;
		if (kRight)
			velocity += right;
		if (kUp)
			velocity += up;
		if (kDown)
			velocity -= up;

		cc.SimpleMove (velocity * speed);

		var pos = transform.position;
		if(pos.y > 0.5f)
			pos.y = 0f;
		transform.position = pos;
	}
}
