﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DumbEnemyController : MonoBehaviour {
	GameObject player;
	NavMeshAgent agent;

	void Start(){
		player = GameObject.FindWithTag ("Player");
		agent = GetComponent<NavMeshAgent> ();
		GetComponent<Health> ().onDeath += ()=>transform.position = new Vector3(0f, 10000f, 0f);
	}

	void Update(){
		agent.destination = player.transform.position;
	}
}
