﻿using UnityEngine;
using System.Collections;

public class AntAttractionPoint : MonoBehaviour {
	public static AntAttractionPoint instance;

	public Color move = Color.blue;
	public Color attack = Color.red;

	void Awake(){
		instance = this;
	}
	void Update(){
		bool leftMouse  = Input.GetMouseButton(0);
		bool rightMouse = Input.GetMouseButtonDown(1);

		if (leftMouse) {
			MoveToMouse ();
		}

		if (rightMouse) {
			AttackToMouse ();
		}
	}

	void MoveToMouse(){
		SetColor (move);
		SetMousePosition ();
	}

	void AttackToMouse(){
		SetColor (attack);
		SetMousePosition ();
	}

	void SetMousePosition(){
		var mainCamera = Camera.main;
		var ray = mainCamera.ScreenPointToRay (Input.mousePosition);
		RaycastHit rayHit;
		if (Physics.Raycast (ray, out rayHit)) {
			transform.position = rayHit.point;
		}
	}

	void SetColor(Color color){
		GetComponent<Renderer> ().material.color = color;
	}
}
