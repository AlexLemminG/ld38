﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	public CharType charType;
    public Effect DamageEffect;
    public Effect DeathEffect;
    [SerializeField]
	private int _amount;
	public int amount {
		get{
			return _amount;
		}
		set{
            if (value < _amount)
                if (DamageEffect != null)
                    DamageEffect.Emit(transform.position, Vector3.up);
            _amount = value;
			if (!dead && _amount <= 0) {
				Die ();
			}
		}
	}

	private void Die(){
        if (DeathEffect != null) DeathEffect.Emit(transform.position, Vector3.up);
		dead = true;
		foreach (var beh in GetComponentsInChildren<Behaviour>()) {
			beh.enabled = false;
		}
		foreach (var col in GetComponentsInChildren<Collider>()) {
			//col.enabled = false;
		}
		if(onDeath != null)
			onDeath ();
        
    }

	bool dead = false;
	public System.Action onDeath;
}

[System.Flags]
public enum CharType{
	player,
	ant,
	enemy,
	antFood
}