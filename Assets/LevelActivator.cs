﻿using UnityEngine;
using System.Collections;

public class LevelActivator : MonoBehaviour {
	public LevelActivator nextLevel;
	public GameObject[] objectsToActivate;
	public MonoBehaviour[] behavioursToActivate;

	public ObjectCounter playerCounter;
	public ObjectCounter antsCounter;
	public ObjectCounter enemiesCounter;

	public Door inputDoor;

	bool levelStarted = false;
	float timeStarted = 0f;
	void StartLevel(){
		if (levelStarted)
			return;
		levelStarted = true;
		timeStarted = Time.time;
		foreach (var go in objectsToActivate) {
			go.SetActive (!go.activeSelf);
		}
		foreach (var be in behavioursToActivate) {
			be.enabled = !be.enabled;
		}
		if(inputDoor)
			inputDoor.Close();
	}

	bool CheckToStart(){
		if(playerCounter && antsCounter)
			return !levelStarted && Time.time > timeStarted + 1f && playerCounter.amountInside == 1 && antsCounter.amountInside == AntController.totalAlive;
		return false;
	}

	void Update(){
		if (CheckToStart ()) {
			StartLevel ();
		}
		if (CheckToComplete ()) {
			complete = true;
			if(nextLevel)
				nextLevel.Open ();
		}
	}
	bool complete = false;
	bool CheckToComplete(){
		return !complete && (levelStarted || playerCounter == null) && Time.time > timeStarted + 1f && enemiesCounter.amountInside == 0;
	}

	public void Open(){
		inputDoor.Open ();
	}
}
