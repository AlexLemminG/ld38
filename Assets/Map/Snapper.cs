using UnityEngine;

public class Snapper : MonoBehaviour
{
    public float CellWidth = 1;
    public float CellHeight = 1;

    void OnDrawGizmos()
    {
        if (!enabled) return;

        Vector3 pos = transform.position;

        pos.x = Mathf.Round(pos.x/CellWidth)*CellWidth;
        pos.z = Mathf.Round(pos.z/CellHeight)*CellHeight;

        transform.position = pos;
    }
}