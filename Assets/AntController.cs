﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AntController : MonoBehaviour {
	NavMeshAgent agent;
	public static int totalAlive = 0;

	void OnEnable(){
		totalAlive++;
	}
	void OnDisable(){
		totalAlive--;
	}

	void Start(){
		agent = GetComponent<NavMeshAgent> ();
		GetComponent<Health> ().onDeath += Die;
	}

	// Update is called once per frame
	void Update () {
		agent.SetDestination (AntAttractionPoint.instance.transform.position);
	}

	void Die(){
		foreach (var beh in GetComponentsInChildren<Behaviour>()) {
			beh.enabled = false;
		}
	}

	void OnTriggerStay(Collider collider) {
		TryToEat (collider.gameObject);
	}
	float nextEatTime;
	void TryToEat(GameObject obj){
		if (Time.time < nextEatTime || !enabled) {
			return;
		}
		var hp = obj.GetComponent<Health> ();
		if (hp == null || !hp.enabled || hp.amount <= 0)
			return;
		if (hp.charType == CharType.antFood) {
			hp.amount -= 1;
			nextEatTime = Time.time + 0.5f;
			Vector3 scale = transform.localScale * 2f;
			transform.DOKill ();
			transform.DOScale (scale, 0.25f).SetEase (Ease.Flash, 1f, 2f).SetTarget(transform);

			var newAntPosition = transform.position + Random.insideUnitSphere*0.01f;
			Instantiate (gameObject, newAntPosition, Quaternion.identity);
		}
	}
}
