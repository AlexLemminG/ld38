﻿using UnityEngine;
using System.Collections;

public class ObjectCounter : MonoBehaviour {
	public CharType charType;
	public int amountInside;

	void Awake(){
		amountInside = 0;
	}
	void OnTriggerEnter(Collider col){
		if (RightType (col)) {
			amountInside++;
		}
	}

	void OnTriggerExit(Collider col){
		if (RightType (col)) {
			amountInside--;
		}
	}

	bool RightType(Collider col){
		var health = col.GetComponent<Health> ();
		return health != null && health.charType == charType;
	}
}
