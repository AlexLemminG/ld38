﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Attacker : MonoBehaviour
{
	public List<CharType> attackable;
	float nextDamageTime = 0f;
	public float attackRate = 1f;
	public int damagePerAttack = 1;
	public float scaleMultiplier = 1.5f;
    public Effect AttackEffect;

	void OnTriggerStay(Collider collider) {
		TryToAttack (collider.gameObject);
	}

	void TryToAttack(GameObject obj){
		if (Time.time < nextDamageTime || !enabled) {
			return;
		}
		var hp = obj.GetComponent<Health> ();
		if (hp == null || !hp.enabled)
			return;
		if (attackable.Contains(hp.charType)) {
			hp.amount -= damagePerAttack;
			nextDamageTime = Time.time + attackRate;
			Vector3 scale = transform.localScale * scaleMultiplier;
			transform.DOKill ();
			transform.DOScale (scale, 0.25f).SetEase (Ease.Flash, 1f, 2f).SetTarget(transform);
		    if (AttackEffect != null)
		        AttackEffect.Emit(transform.position, Vector3.Normalize(obj.transform.position - transform.position));
		}
	}
}
