﻿using UnityEngine;
using System.Collections;

public class ObjectsSpawner : MonoBehaviour {
	public float radius;
	public int amount;
	public GameObject prefab;

	void Start(){
		while (amount > 0) {
			amount--;
			var pointOnCircle = Random.insideUnitCircle;
			var position = new Vector3(pointOnCircle.x, 0f, pointOnCircle.y) * radius + transform.position;
			var go = Instantiate (prefab, position, Quaternion.identity);
		}
	}

	void OnDrawGizmos(){
		Gizmos.DrawWireSphere (transform.position, radius);
	}
}
