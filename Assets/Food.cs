﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	int initHealth;
	Vector3 initScale;
	float minScale = 0.25f;
	Health health;
	// Use this for initialization
	void Start () {
		health = GetComponent<Health> ();
		health.onDeath += () => Destroy (gameObject);
		initHealth = health.amount;
		initScale = transform.localScale;
	}

	void Update(){
		float scale = 1f * health.amount / initHealth;
		scale = Mathf.Max (scale, minScale);
		transform.localScale = initScale * scale;
	}
}
